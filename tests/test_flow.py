import numpy as np
import pytest
from lobster.lightcurve import Lightcurve


@pytest.fixture
def lightcurve():
    # Create a sample Lightcurve object with some data
    lc = Lightcurve()
    time = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 17, 18, 19, 20], dtype=float)
    flux = np.array([16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1], dtype=float)
    sector = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2], dtype=int)
    lc.init_from_data(time, flux, sector=sector)
    return lc


def test_resample(lightcurve):
    # Alter data
    lightcurve.resample(dt=2, begin_at_dt_ratio=0.0)

    # Expected result: time and flux are correctly removed
    expected_time = np.array([1, 3, 5, 7, 9, 11, 13, 15, 17, 19], dtype=float)
    expected_flux = np.array(
        [15.5, 13.5, 11.5, 9.5, 7.5, np.nan, np.nan, 5.5, 3.5, 1.5], dtype=float
    )
    expected_sector = np.array([1, 1, 1, 1, 1, 2, 2, 2, 2, 2], dtype=int)

    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)
    np.testing.assert_array_equal(lightcurve.sector, expected_sector)


def test_remove_data(lightcurve):
    # Alter data
    lightcurve.remove_data((lightcurve.time > 7) & (lightcurve.time <= 15))

    # Expected result: time and flux are correctly removed
    expected_time = np.array([1, 2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20], dtype=float)
    expected_flux = np.array([16, 15, 14, 13, 12, 11, 10, 5, 4, 3, 2, 1], dtype=float)
    expected_sector = np.array([1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2], dtype=int)

    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)
    np.testing.assert_array_equal(lightcurve.sector, expected_sector)


def test_remove_data_resample(lightcurve):
    # Alter data
    lightcurve.remove_data((lightcurve.time > 7) & (lightcurve.time <= 15))
    lightcurve.resample(dt=2, begin_at_dt_ratio=0.0)

    # Expected result: time and flux are correctly removed
    expected_time = np.array([1, 3, 5, 7, 9, 11, 13, 15, 17, 19], dtype=float)
    expected_flux = np.array(
        [15.5, 13.5, 11.5, 10, np.nan, np.nan, np.nan, 5, 3.5, 1.5], dtype=float
    )
    expected_sector = np.array([1, 1, 1, 1, 2, 2, 2, 2, 2, 2], dtype=int)

    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)
    np.testing.assert_array_equal(lightcurve.sector, expected_sector)


def test_remove_large_jumps(lightcurve):
    # Alter data
    lightcurve.remove_large_jumps(min_jump_days=3, dt=0.5)

    # Expected result: time and flux are correctly removed
    expected_time = np.array(
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9.5, 10.5, 11.5, 12.5, 13.5, 14.5], dtype=float
    )
    expected_flux = np.array([16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1], dtype=float)
    expected_sector = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2], dtype=int)

    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)
    np.testing.assert_array_equal(lightcurve.sector, expected_sector)


def test_remove_large_jumps_resample(lightcurve):
    # Alter data
    lightcurve.remove_large_jumps(min_jump_days=3, dt=0.5)
    lightcurve.resample(dt=2, begin_at_dt_ratio=0.0)

    # Expected result: time and flux are correctly removed
    expected_time = np.array([0, 2, 4, 6, 8, 10, 12, 14], dtype=float)
    expected_flux = np.array([15.5, 13.5, 11.5, 9.5, 7, 4.5, 2.5, 1], dtype=float)
    expected_sector = np.array([1, 1, 1, 1, 1, 2, 2, 2], dtype=int)

    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)
    np.testing.assert_array_equal(lightcurve.sector, expected_sector)
