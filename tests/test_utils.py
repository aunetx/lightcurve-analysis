import numpy as np
import pytest
from lobster.utils import searchsorted_with_nan_handling, is_all_invalid


def test_searchsorted_with_nan_handling():
    b = np.array([0.0, 1.0, np.nan, 3.0, 4.0])

    # Test with single values
    assert searchsorted_with_nan_handling(b, 1) == 1  # Should match np.searchsorted(a, 2)
    assert searchsorted_with_nan_handling(b, 3) == 3  # Adjusted for NaN position
    assert searchsorted_with_nan_handling(b, 4) == 4  # Last position

    # Test with array of values
    values = np.array([1, 3, 4])
    expected = np.array([1, 3, 4])  # Adjusted positions
    np.testing.assert_array_equal(searchsorted_with_nan_handling(b, values), expected)

    # Test with numbers before the first element
    assert searchsorted_with_nan_handling(b, -1) == 0  # Insert at start

    # Test with numbers greater than all elements
    assert searchsorted_with_nan_handling(b, 10) == 5  # Insert at end

    # Test when the array consists entirely of NaNs
    nan_only = np.array([np.nan, np.nan, np.nan])
    assert searchsorted_with_nan_handling(nan_only, 2) == 0  # Like for an ampty array

    # Test empty array
    empty_arr = np.array([])
    assert searchsorted_with_nan_handling(empty_arr, 2) == 0  # Insert at position 0


def test_is_all_invalid():
    # Test only NaNs
    array = np.array([np.nan, np.nan, np.nan])
    assert is_all_invalid(array) == True

    # Test mixed values
    array = np.array([1, 2, np.nan, 4, 5])
    assert is_all_invalid(array) == False

    # Test empty array
    array = np.array([])
    assert is_all_invalid(array) == True
