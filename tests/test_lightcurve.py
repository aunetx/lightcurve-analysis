import numpy as np
import pytest
from lobster.lightcurve import Lightcurve


@pytest.fixture
def lightcurve():
    # Create a sample Lightcurve object with some data
    lc = Lightcurve()
    time = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 17, 18, 19, 20], dtype=float)
    flux = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=float)
    lc.init_from_data(time, flux)
    return lc


def test_remove_isolated_points(lightcurve):
    # Create test data
    lightcurve.time = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 21, 22, 23, 24], dtype=float)
    lightcurve.flux = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=float)

    # Call the method to remove isolated points
    lightcurve.remove_isolated_points(
        isolated_of_more_than_days=2, min_n_points=3, removal_mode="NaN"
    )

    # Expected result: isolated points should be replaced with NaN
    expected_time = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 21, 22, 23, 24], dtype=float)
    expected_flux = np.array(
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, np.nan, np.nan, 1, 1, 1, 1], dtype=float
    )

    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)


def test_remove_large_jumps(lightcurve):
    # Create test data
    lightcurve.time = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 16, 17, 18, 19, 20], dtype=float)
    lightcurve.flux = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=float)
    lightcurve.jumps = {4: {}, 9: {}, 10: {}, 13: {}}

    # Call the method to remove large jumps
    lightcurve.remove_large_jumps(min_jump_days=5, dt=0.5, remove_jumps_metadata=True)

    # Expected result: large jumps should be removed
    expected_jumps = {4: {}, 9: {}, 13: {}}
    expected_time = np.array(
        [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 9.5, 10.5, 11.5, 12.5, 13.5, 14.5],
        dtype=float,
    )
    expected_flux = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=float)

    assert lightcurve.jumps == expected_jumps
    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)


def test_zero_pad(lightcurve):
    # Create test data
    lightcurve.time = np.array([1, 2, 3, 4, 5, 6], dtype=float)
    lightcurve.flux = np.array([1, 1, 1, 1, 1, 1], dtype=float)

    # Zero-pad the lightcurve with n=2 and dt="median"
    lightcurve.zero_pad(n=2, dt="median")

    # Expected result
    expected_time = np.arange(1, 19, 1)
    expected_flux = np.array([1, 1, 1, 1, 1, 1] + [0] * 12)

    # Assert that the time and flux arrays are as expected
    np.testing.assert_array_equal(lightcurve.time, expected_time)
    np.testing.assert_array_equal(lightcurve.flux, expected_flux)
