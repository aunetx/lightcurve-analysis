Lightcurve module
=================

The main module of Lobster. It contains `Lightcurve`, which is the class used
to describe a given star's lightcurve, and then correct, modify, analyze and
plot it at will.

.. autoclass:: lobster.Lightcurve

Private methods and attributes
------------------------------

The private methods and attributes are documented here. They should not be used in normal code,
but they can be useful to understand and to debug some code.

.. autoattribute:: lobster.Lightcurve._debug
.. autoattribute:: lobster.Lightcurve._operations
.. autoattribute:: lobster.Lightcurve._original_time
.. autoattribute:: lobster.Lightcurve._corresponding_time
.. autoattribute:: lobster.Lightcurve._original_quality
.. autoattribute:: lobster.Lightcurve._original_sector
.. automethod:: lobster.Lightcurve._init_from_quarters_data
.. automethod:: lobster.Lightcurve._duration_to_odd_size
.. automethod:: lobster.Lightcurve._apply_filter
.. automethod:: lobster.Lightcurve._divide_flux_between_jumps
.. automethod:: lobster.Lightcurve._add_operation

Filtering constants
-------------------

See notebooks/cutting_periods.ipynb for the computation of these constants.

.. autodata:: lobster.lightcurve.SMOOTH_BOXCAR_FILTER_DIVISER
.. autodata:: lobster.lightcurve.SMOOTH_TRIANGULAR_FILTER_DIVISER
.. autodata:: lobster.lightcurve.SMOOTH_SIN4_FILTER_DIVISER
.. autodata:: lobster.lightcurve.NORMALIZE_BOXCAR_FILTER_DIVISER
.. autodata:: lobster.lightcurve.NORMALIZE_TRIANGULAR_FILTER_DIVISER
.. autodata:: lobster.lightcurve.NORMALIZE_SIN4_FILTER_DIVISER
.. autodata:: lobster.lightcurve.NORMALIZE_SIN4_3_TIMES_FILTER_DIVISER