Lobster
=======

Lobster is a python module used to correct stellar lightcurves before rotation or asteroseismic analysis.

It provides a `Lightcurve` class to describe the lightcurve of a star.
This class has numerous methods to:

- load lightcurves from FITS file with different input format, or from `numpy` arrays
- remove bad quality points, invalids, outliers, large jumps between sections...
- find jumps in-between sectors and TESS sub-sectors, find gaps in time, jumps in flux...
- stitch the lightcurve to correct the jumps, with automatic method comparison to handle edge cases
- apply low-pass convolutive filters, and high-pass convolutive and adaptative filters
- resample the data, pad it with zeros, ensure its positivity...
- plot the lightcurve, its PSD and ACF in the correct way
- save the lightcurve as a FITS file, along with the summary of operations that have been done on it

This is the main way to use Lobster. In order to understand what is being done when you stitch or filter
the lightcurve for example, you can look at the respective modules in the API section, which provide a
lot of insight on the computations being done. Only `Lightcurve` is normally useful for the end-user,
but everything is still entirely detailled along with unit tests and examples.

.. toctree::
   :maxdepth: 2
   :caption: Detailed API

   api/lightcurve
   api/filtering
   api/stitching
   api/plotting
   api/maths
   api/fits_management
   api/utils
