project = "Lobster"
copyright = "2025, Aurélien Hamy"
author = "Aurélien Hamy"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.mathjax",
    "sphinx_book_theme",
    "matplotlib.sphinxext.plot_directive",
]

autodoc_default_options = {
    "members": True,
    "undoc-members": False,
    "show-inheritance": True,
}
autodoc_member_order = "bysource"

napoleon_numpy_docstring = True

html_theme = "sphinx_book_theme"
html_title = "Lobster"
html_logo = "logo.png"
html_theme_options = {
    "path_to_docs": "docs",
    "repository_url": "https://gitlab.com/aunetx/lobster/",
    "use_repository_button": True,
    "use_download_button": True,
    "show_toc_level": 2,
}
html_sidebars = {
    "**": [
        "navbar-logo.html",
        "search-field.html",
        "sbt-sidebar-nav.html",
    ]
}
