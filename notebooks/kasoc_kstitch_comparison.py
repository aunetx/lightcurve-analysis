# %%
import numpy as np
import matplotlib.pyplot as plt
import sys
import ipad
import pandas as pd
from ipad.catalog import Catalog

# this is needed when importing from the notebooks folder
sys.path.append("../")
from lobster import Lightcurve
from lobster.filtering import sin4_filter

# %%
# RUN ONLY TO RECREATE THE TIC LIST

cat = ipad.catalog.Catalog()
ohio_data = cat.load_catalog("ohio_stars")
tic_list = list(ohio_data.TIC.unique())
ipad.downloads.qlp.download_stars(tic_list)
qlp_target_list = cat.load_catalog("QLP_target_list")
tic_ohio_qlp = qlp_target_list.query("TIC == @tic_list").TIC.unique()
del qlp_target_list
del ohio_data
tic_ohio_qlp = tic_ohio_qlp.astype(int)
tic_list_df = pd.DataFrame(tic_ohio_qlp)
tic_list_df.to_csv("./ohio_stars_QLP_only.csv", header=False, index=False)

# %%

# cat = ipad.catalog.Catalog()
tic_list_df = pd.read_csv("./ohio_stars_QLP_only.csv", header=None)
# tic_list_df = pd.read_parquet("./Stars_not_working.parquet")
data_location = "/home/aunetx/Documents/fits_data/TESS"
# data_location = cat.load_data_location("local_dl_tess").file_location

# %%
# KASOC k-stitch function

import numpy as np
import logging
from copy import deepcopy
from bottleneck import nanmedian, allnan, nansum, move_mean
from statsmodels.nonparametric.smoothers_lowess import lowess


# ------------------------------------------------------------------------------
# from # https://github.com/tasoc/corrections/blob/devel/corrections/kasoc_filter/kasoc_filter.py
# with Mikkel N. Lund modifications


@np.errstate(invalid="ignore")
def _slope(x_1, x_2, y_1, y_2):
    return (1 - 2 * (x_1 > x_2)) * ((y_2 - y_1) / np.abs((x_2 - x_1)))


def theil_sen(x, y, n_samples=100000, seed=291988):
    """
    Computes the Theil-Sen estimator for 2D data.
    This complexity is O(n**2), which can be poor for large n. We will perform a sampling
    of data points to get an unbiased, but larger variance estimator.
    The sampling will be done by picking two points at random, and computing the slope,
    up to n_samples times.
    Parameters:
            x (ndarray): 1-d np array, the control variate.
            y (ndarray): 1-d np.array, the ind variate.
            n_samples (int): how many points to sample.
    """
    rng = np.random.default_rng(seed)

    if x.shape[0] != y.shape[0]:
        raise ValueError("x and y must be the same shape.")
    n = int(x.shape[0])

    i1 = rng.integers(0, n, int(n_samples))
    i2 = rng.integers(0, n, int(n_samples))
    # i1 = np.random.randint(0, n, n_samples)
    # i2 = np.random.randint(0, n, n_samples)

    slopes = _slope(x[i1], x[i2], y[i1], y[i2])

    slope_ = nanmedian(slopes)
    # find the optimal b as the median of y_i - slope*x_i
    intercepts = np.empty(n, dtype="float64")
    for i in range(n):
        intercepts[i] = y[i] - slope_ * x[i]
    intercept_ = nanmedian(intercepts)

    return np.array([slope_, intercept_])


def BIC(data, model, dof, scatter=None):
    """
    Calculate the Bayesian Information Criterion given data, model and degrees of freedom.
    Parameters:
            data (ndarray):
            model (ndarray):
            dof (int): Number of degrees of freedom in the model.
            scatter (ndarray, optional):
                    If not provided, a robust mean scatter is calculated from the data.
    Returns:
            float: Bayesian Information Criterion.
    """
    if scatter is None:
        # : Conversion constant from MAD to Sigma. Constant is 1/norm.ppf(3/4)
        mad_to_sigma = 1.482602218505602
        scatter = nanmedian(np.abs(np.diff(data))) * mad_to_sigma
    return (
        len(data) * np.log(2) + nansum((data - model) ** 2 / scatter**2) + dof * np.log(len(data))
    )


def kasoc_kstitch(t, x, jumps, width=3.0, return_flags=False):
    """
    Remove jumps from timeseries.
    Parameters:
            t (ndarray): Time vector (days). Must be sorted in time.
            x (ndarray): Flux vector. Can contain invalid points (NaN).
            jumps (list): Vector of timestamps where jumps are to be corrected.
            width (float): Width of the region on each side of jumps to compare (default=3 days).
            return_flags (boolean): Return two additional arrays with location of corrected jumps.
    Returns:
            tuple:
            - ndarray: Corrected flux vector.
            - list: List with the same length as `jumps`, indicating if the particular jump was corrected.
            - ndarray: Quality array with same length as `x`, indicating where and which correction was performed.
    """

    # Get the logger to use for printing messages:
    logger = logging.getLogger(__name__)

    # Number of points:
    N = len(t)

    dt = nanmedian(np.diff(t))

    # Convert a simple list of times to a jumps-dictionary:
    jumps = np.atleast_1d(jumps)
    for k, jump in enumerate(jumps):
        if np.isscalar(jump):
            jumps[k] = {"time": jump}
        elif not isinstance(jump, dict):
            raise ValueError("Invalid input in JUMPS")

    # Important that we correct the jumps in the right order:
    jumps = sorted(jumps, key=lambda k: k["time"])

    # Arrays needed for the following:
    correction = np.empty(2, dtype="float64")
    jumps_flag = None
    if return_flags:
        jumps_flag = np.array(
            [],
            dtype={
                "names": ["time", "type", "force", "applied", "method", "value"],
                "formats": ["float64", "a1", "bool", "bool", "a1", "float64"],
            },
        )

    # Correct jumps one after the other:
    kj = 0
    for k, jump in enumerate(jumps):

        jump2 = dict(jump)
        logger.debug(jump)
        # Extract information about jump:
        tjump = jump.get("time")
        jumptype = jump.get("type", "additive")
        jumpforce = jump.get("force", False)

        # Make maps to central region and region after jump:
        kj_pre = kj
        kj = np.searchsorted(t, tjump)
        if kj == 0 or kj == N or kj == kj_pre:
            continue  # Stop if first, last or same point as previous
        central1 = np.searchsorted(t, t[kj - 1] - width)
        central2 = np.searchsorted(t, t[kj] + width)

        gapsize = t[kj] - t[kj - 1]  # The length of the jump

        # Make small timeseries around the gap:
        tcen = t[central1:central2]
        xcen = x[central1:central2]
        xmdl = np.empty_like(xcen)
        indx = np.searchsorted(tcen, tjump)

        # Do simple check to see if all datapoints are NaN:
        if allnan(x[central1:kj]) or allnan(x[kj:central2]):
            continue

        # Run LOWESS filter on two halves to eliminate effects of transit:
        if kj - central1 < 0.5 * int(width / dt):
            w1 = np.hstack(
                (
                    [
                        t[central1:kj],
                    ],
                    [
                        x[central1:kj],
                    ],
                )
            )
        else:
            w1 = lowess(x[central1:kj], t[central1:kj], frac=1.0 / 3, is_sorted=True)

        if central2 - kj < 0.5 * int(width / dt):
            w2 = np.hstack(
                (
                    [
                        t[kj:central2],
                    ],
                    [
                        x[kj:central2],
                    ],
                )
            )
        else:
            w2 = lowess(x[kj:central2], t[kj:central2], frac=1.0 / 3, is_sorted=True)

        # Calculate median levels before and after jump
        # and make these match up:
        level1_const = nanmedian(w1[:, 1])
        level2_const = nanmedian(w2[:, 1])

        # Do not try to use linear relation on very long gaps
        # it will in many cases not work.
        if gapsize < 2 * width:
            # Do robust linear fit of part before and after jump:
            res1 = theil_sen(w1[:, 0], w1[:, 1], n_samples=1e4)
            res2 = theil_sen(w2[:, 0], w2[:, 1], n_samples=1e4)

            # Evaluate fitted lines at midpoint in the gap:
            tmid = (t[kj] + t[kj - 1]) / 2  # Midpoint in gap
            level1_linear = np.polyval(res1, tmid)
            level2_linear = np.polyval(res2, tmid)
        else:
            level1_linear = np.NaN
            level2_linear = np.NaN

        # Calculate Bayesian Information Criterion (BIC) for the different
        # models of the jump to decide which one should be applied to the data:
        if jumptype == "additive":
            # Constant model:
            correction[0] = level1_const - level2_const
            if np.isfinite(correction[0]):
                # Calculate model:
                xmdl[:indx] = level1_const
                xmdl[indx:] = level2_const
                # Calculate BIC:
                s1 = BIC(xcen, xmdl, 2)
            else:
                s1 = np.Inf

            # Linear model:
            correction[1] = level1_linear - level2_linear
            if np.isfinite(correction[1]):
                # Calculate model:
                xmdl[:indx] = np.polyval(res1, tcen[:indx])
                xmdl[indx:] = np.polyval(res2, tcen[indx:])
                # Calculate BIC:
                s2 = BIC(xcen, xmdl, 4)
            else:
                s2 = np.Inf

        elif jumptype == "multiplicative":
            # Constant model:
            correction[0] = level1_const / level2_const
            if np.isfinite(correction[0]) and correction[0] > 0:
                # Correct data:
                # take a deep copy, such that corrections doesn't affect xcen
                xcen2 = deepcopy(xcen)
                xcen2[indx:] *= correction[0]
                # Calculate model:
                xmdl[:] = level1_const
                # Calculate BIC:
                s1 = BIC(xcen2, xmdl, 2)
            else:
                s1 = np.Inf

            # Linear model:
            correction[1] = level1_linear / level2_linear
            if np.isfinite(correction[1]) and correction[1] > 0:
                # Correct data:
                # take a deep copy, such that corrections doesn't affect xcen
                xcen2 = deepcopy(xcen)
                xcen2[indx:] *= correction[1]
                # Calculate model:
                xmdl[:indx] = np.polyval(res1, tcen[:indx])
                xmdl[indx:] = np.polyval(res2, tcen[indx:]) * correction[1]
                # Calculate BIC:
                s2 = BIC(xcen2, xmdl, 4)
            else:
                s2 = np.Inf

        else:
            raise ValueError("Unknown jump type")

        # Apply correction to entire timeseries if the standard deviation improves:
        if jumpforce:
            i = np.argmin([s1, s2]) + 1
        else:
            # Calculate BIC of uncorrected central part:
            s0 = BIC(xcen, nanmedian(xcen), 1)
            i = np.argmin([s0, s1, s2])
        logger.debug(i)

        jump2["applied"] = False
        jump2["method"] = 0
        jump2["value"] = 0.0

        if i != 0:  # Do not correct if unaltered data gives the best
            # Apply the best correction to everything to the right of the jump:
            if jumptype == "additive":
                x[kj:] += correction[i - 1]
            else:
                x[kj:] *= correction[i - 1]

            jump2["value"] = correction[i - 1]
            # Set the flags, if required:
            if return_flags:

                jump2["applied"] = True

                if jumptype == "additive":
                    if i == 1:
                        jump2["method"] |= 2  # constant additive
                    elif i == 2:
                        jump2["method"] |= 4  # linear additive
                elif jumptype == "multiplicative":
                    if i == 1:
                        jump2["method"] |= 256  # constant multiplicative
                    elif i == 2:
                        jump2["method"] |= 512  # linear multiplicative

        jumps_flag = np.append(jumps_flag, jump2)

    if return_flags:
        return x, jumps_flag
    else:
        return x


# ------------------------------------------------------------------------------


def _get_jumps(t, q, flag):
    """
    Get jumps information from quarter and flag
    """
    qs = np.array(list(set(q)))

    jumps = np.array(
        [], dtype={"names": ["time", "type", "force"], "formats": ["float64", "a1", "bool"]}
    )
    jumptimes = t[(flag != 0) & (flag != 24)]

    # The definition of "type" will depend on the quality flags in the un-stitched data
    # In this case I simply use additive corrections for both mask updates and quarter start times
    for qq in qs[1::]:
        jmp = t[q == qq][0]  # Quarter start times (force a correction here)
        jumps = np.append(jumps, {"time": jmp, "type": "additive", "force": True})

    for jmp in jumptimes:  # Mask updates (allow BIC to decide here)
        jumps = np.append(jumps, {"time": jmp, "type": "additive", "force": False})
    return jumps


# %%

stars_to_compute = list(map(lambda x: x[0], tic_list_df.values))
# stars_to_compute = [261136679]

for i, tic in enumerate(stars_to_compute):
    tic = int(tic)
    print(f"star {i+1}/{len(stars_to_compute)}, TIC {tic}")

    dirname = ipad.downloads.qlp.get_star_qlp_directory(tic, data_location)
    lc = Lightcurve()
    lc.load_qlp(dirname)
    lc.jumps = {}

    plt.close("all")
    fig, (ax0, ax1, ax2, ax3) = plt.subplots(4, figsize=(10, 16), dpi=150)

    lc.plot_flux(ax=ax0, title="Original flux", by_sector=True)

    # Pre-stitching pipeline: this is to remove quality, outliers, finding jumps...

    lc.remove_quality()
    lc.remove_invalids()
    lc.remove_outliers(normalize_period_cut=5, separate_fluxes_by="half-sector")
    lc.remove_outliers(normalize_period_cut=5, separate_fluxes_by="half-sector")
    lc.resample(0.5 / 24)

    lc.find_jumps_between_tess_sectors()

    lc.remove_isolated_points()
    lc.remove_invalids()
    # lc.remove_data(lc.time < 2600, removal_mode="delete")

    removed_large_jumps = lc.remove_large_jumps(90, return_removed_jumps=True)
    for jump in removed_large_jumps:
        lc.jumps.pop(jump, None)

    lc.plot_flux(ax=ax1, title="Cleaned flux", by_sector=True)
    lc.plot_jumps(ax=ax1, c="teal")
    lc.plot_jumps(ax=ax2, c="teal")
    lc.plot_jumps(ax=ax3, c="teal")

    # We stitch here: two different lightcurves, one for KASOC and one for Lobster

    # Lobster stitching
    lc_lobster = lc.copy()
    lc_lobster.stitch(
        stitching_methods=[
            {"method": "total-mean"},
            {"method": "tips-mean"},
            {"method": "tips-poly-1", "max-jump": 15},
            {"method": "tips-poly-2"},
            # {
            #    "method": "optimal-estimator",
            #    "length-scale": 130,
            #    "sigma": 0.05,
            #    "nu": 2.3,
            #    "lambda-reg": 1e-12,
            # },
        ],
        verbose=False,
        debug=False,
        show_on_ax=ax2,
    )

    lc_lobster_new = lc.copy()
    lc_lobster_new.better_stitch(
        stitching_methods=[
            {"method": "total-mean"},
            {"method": "tips-mean"},
            {"method": "tips-poly-1", "max-jump": 15},
            {"method": "tips-poly-2"},
            # {
            #    "method": "optimal-estimator",
            #    "length-scale": 130,
            #    "sigma": 0.05,
            #    "nu": 2.3,
            #    "lambda-reg": 1e-12,
            # },
        ],
        verbose=False,
        debug=False,
        # choose_with_BIC="tips-sin",
        show_on_ax=ax2,
    )

    # KASOC stitching, we need to resample before
    lc_kasoc = lc.copy()
    lc_kasoc.resample(0.5 / 24)
    kasoc_jumps = []
    for jump in lc_lobster.jumps:
        kasoc_jumps.append({"time": (lc_lobster.time[jump - 1] + lc_lobster.time[jump]) / 2})
    lc_kasoc.flux = kasoc_kstitch(lc_kasoc.time, lc_kasoc.flux, kasoc_jumps)

    # Post-stitching pipeline: we need to run everything on the two lightcurves

    # we give back the large jumps in order to total-mean stitch them
    lc_lobster.jumps = removed_large_jumps
    lc_lobster.resample(0.5 / 24)
    lc_lobster_new.jumps = removed_large_jumps
    lc_lobster_new.resample(0.5 / 24)
    lc_kasoc.jumps = lc_lobster.jumps

    is_first_lc = True
    for lc, name in zip(
        [lc_kasoc, lc_lobster, lc_lobster_new], ["KASOC", "Lobster old", "Lobster new"]
    ):
        lc.plot_flux(ax=ax2, title="Stitched flux", label=name)

        # normalize to remove long-term trends
        lc.normalize_sin4_3_times_filter(cut_above_period=40, divide=False, section_mode="section")

        # equalize the mean of large sections
        lc.stitch(stitching_methods="total-mean")

        # plot only once
        if is_first_lc:
            lc.plot_jumps(ax=ax1, c="red", lw=1)
            lc.plot_jumps(ax=ax2, c="red", lw=1)
            lc.plot_jumps(ax=ax3, c="red", lw=1)

        # little_filter = lc.normalize_adaptative_sin4_filter(
        #    cut_above_periods=(0.5, 20),
        #    thresholds=(1.5, 2),
        #    return_indicators=True,
        #    section_mode="all",
        # )

        lc.plot_flux(ax=ax3, title="Normalized flux", label=name)
        # lc.plot_correction_zones(ax3, little_filter, label=f"little filter for {name}")

        # lc.flux_to_ppm()
        # lc.remove_invalids(removal_mode="zero")
        # lc.plot_psd(ax=ax4, title="PSD", label=name)

        is_first_lc = False

    fig.suptitle(lc.object_name, fontsize=15)
    fig.tight_layout()
    fig.savefig(f"./output_stitching_comparison/{tic}.png")

# %%
