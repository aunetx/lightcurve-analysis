# Lobster correction

<img src="https://gitlab.com/aunetx/lobster/-/raw/master/docs/logo.png" height="200" align="right">

Lobster is a python module used to correct stellar lightcurves before rotation or asteroseismic analysis.

It provides a `Lightcurve` class to describe the lightcurve of a star.

This class has numerous methods to:

- load lightcurves from FITS file with different input format, or from `numpy` arrays
- remove bad quality points, invalids, outliers, large jumps between sections...
- find jumps in-between sectors and TESS sub-sectors, find gaps in time, jumps in flux...
- stitch the lightcurve to correct the jumps, with automatic method comparison to handle edge cases
- apply low-pass convolutive filters, and high-pass convolutive and adaptative filters
- resample the data, pad it with zeros, ensure its positivity...
- plot the lightcurve, its PSD and ACF in the correct way
- save the lightcurve as a FITS file, along with the summary of operations that have been done on it

## How to use

A precise documentation is available on [Read The Docs](https://lobster-correction.readthedocs.io/en/latest/).
It documents every function and method of this module, and give a lot of examples on how to use them.

Multiple examples on how to use this module are shown in the [notebooks/](https://gitlab.com/aunetx/lobster/-/blob/master/notebooks) subfolder, as Jupyter notebooks. More particularly, a step-by-step tutorial covering some importants parts is present at [notebooks/example_tess.ipynb](https://gitlab.com/aunetx/lobster/-/blob/master/notebooks/example_tess.ipynb).

## Requirements

Lobster uses these libraries as dependencies:

- [numpy](https://numpy.org/)
- [matplotlib](https://matplotlib.org/)
- [scipy](https://scipy.org/)
- [pandas](https://pandas.pydata.org/)
- [astropy](https://www.astropy.org/)
- [ipad](https://gitlab.com/dinilbose/ipad), but not a hard dependency, only used for [Lightcurve.find_jumps_between_tess_sectors](https://lobster-correction.readthedocs.io/en/latest/api/lightcurve.html#lobster.Lightcurve.find_jumps_between_tess_sectors)

## Installation

You can install this module with `pip`:

```sh
git clone https://gitlab.com/aunetx/lobster
cd lobster
pip install .
```

## Authors

- **Aurélien Hamy**, maintainer (CEA Saclay, France)
- **Rafael A. García**, contributor (CEA Saclay, France)
- **Dinil B. Palakkatharappil**, contributor (CEA Saclay, France)
- **Alexis Prin**, contributor (CEA Saclay, France)

## Acknowledgements

If you use this module in your work, please provide a link to
the GitLab repository.

Although they are not dependencies of Lobster, this work is inspired by:

- [Lightkurve](https://github.com/lightkurve/lightkurve), by the [Lightkurve Collaboration, 2018](http://adsabs.harvard.edu/abs/2018ascl.soft12013L) for the structure of the `Lightcurve` class
- [apollinaire](https://gitlab.com/sybreton/apollinaire) by [Breton et al., 2022](https://ui.adsabs.harvard.edu/abs/2022A%26A...663A.118B/abstract) for the acf method
- [Platano](https://gitlab.com/rgarcibus/platano) by Rafael A. García
- the [KASOC filter](https://github.com/tasoc/corrections/blob/devel/corrections/kasoc_filter/kasoc_filter.py), by [Rasmus Handberg et al., 2021](https://doi.org/10.5281/zenodo.5154027) for the stitching methods

The TESS-QLP lightcurves provided as test data were produced by [Chelsea X. Huang et al., 2020](https://arxiv.org/abs/2011.06459).

The *Kepler*-BONUS lightcurves provided as test data were produced by [Jorge Martinez-Palomera et al., 2023](https://arxiv.org/abs/2310.17733).

The K2 lightcurves provided as test data were produced by MAST.

This module's logo was created based on the “lobster” icon by food lover from [Noun Project](https://thenounproject.com/browse/icons/term/lobster/) CC BY 3.0.

## License

This program is distributed under the terms of the GNU General Public License, version 3 or later.