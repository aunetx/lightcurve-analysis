"""
Various utilities to easily plot lightcurves and analysis graphs with the correct visual
without the need to configure it too much, while still being flexible.
"""

import matplotlib.pyplot as plt


def plot(
    x,
    y,
    figsize=(10, 4),
    dpi=100,
    ax=None,
    xlabel=None,
    ylabel=None,
    xscale="linear",
    yscale="linear",
    xlim=None,
    ylim=None,
    title=None,
    grid=True,
    duplicate_axes=True,
    show=False,
    path_save=None,
    alpha=0.7,
    label=None,
    **kwargs,
):
    """
    A general function to plot anything with the correct visual at first glance, but to
    be able to still configure it easily.

    Parameters
    ----------
    x : array-like
        Data for the x-axis.
    y : array-like
        Data for the y-axis.
    figsize : tuple, optional
        Figure size in inches, default is ``(10, 4)``.
    dpi : int, optional
        Resolution of the figure in dots per inch, default is ``100``.
    ax : matplotlib.axes.Axes, optional
        Existing axis to plot on. If ``None``, a new figure and axis are created.
    xlabel : str, optional
        Label for the x-axis.
    ylabel : str, optional
        Label for the y-axis.
    xscale : {"linear", "log", "symlog", "logit"}, optional
        Scale for the x-axis, default is ``"linear"``.
    yscale : {"linear", "log", "symlog", "logit"}, optional
        Scale for the y-axis, default is ``"linear"``.
    xlim : tuple, optional
        Limits for the x-axis, default is ``None`` (auto).
    ylim : tuple, optional
        Limits for the y-axis, default is ``None`` (auto).
    title : str, optional
        Title of the plot.
    grid : bool, optional
        Whether to display a grid, default is ``True``.
    duplicate_axes : bool, optional
        Whether to enable both top and bottom (x-axis) or left and right (y-axis) ticks.
    show : bool, optional
        Whether to display the plot, default is ``False``.
    path_save : str, optional
        File path to save the figure. If ``None``, the figure is not saved.
    alpha : float, optional
        Opacity of the plot line, default is ``0.7``.
    label : str, optional
        Label for the plotted data (used in legends).
    **kwargs : dict
        Additional keyword arguments passed to `matplotlib.pyplot.plot`.

    Returns
    -------
    matplotlib.axes.Axes
        The axis containing the plot.
    """
    if ax is None:
        fig = plt.figure(figsize=figsize, dpi=dpi)
        ax = fig.add_subplot()
    else:
        fig = ax.get_figure()

    ax.plot(x, y, label=label, alpha=alpha, **kwargs)

    # ensure we don't perturb previous plots if the values are unset
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if xscale:
        ax.set_xscale(xscale)
    if yscale:
        ax.set_yscale(yscale)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if title:
        ax.set_title(title)
    if grid and not (hasattr(ax, "grid_is_enabled") and ax.grid_is_enabled):
        # needed or the grid is disabled when plotting two times
        ax.grid_is_enabled = True
        ax.grid()
    if duplicate_axes:
        ax.tick_params(axis="x", which="both", bottom=True, top=True)
        ax.tick_params(axis="y", which="both", left=True, right=True)
    if label:
        ax.legend()

    if path_save:
        fig.savefig(path_save)
    if show:
        fig.tight_layout()
        plt.show()
    return ax


def scatter(
    x,
    y,
    figsize=(10, 4),
    dpi=100,
    ax=None,
    xlabel=None,
    ylabel=None,
    xscale="linear",
    yscale="linear",
    xlim=None,
    ylim=None,
    title=None,
    grid=True,
    duplicate_axes=True,
    show=False,
    path_save=None,
    alpha=0.5,
    s=4,
    label=None,
    **kwargs,
):
    """
    A general function to scatter anything with the correct visual at first glance, but to
    be able to still configure it easily.

    Parameters
    ----------
    x : array-like
        Data for the x-axis.
    y : array-like
        Data for the y-axis.
    figsize : tuple, optional
        Figure size in inches, default is ``(10, 4)``.
    dpi : int, optional
        Resolution of the figure in dots per inch, default is ``100``.
    ax : matplotlib.axes.Axes, optional
        Existing axis to plot on. If ``None``, a new figure and axis are created.
    xlabel : str, optional
        Label for the x-axis.
    ylabel : str, optional
        Label for the y-axis.
    xscale : {"linear", "log", "symlog", "logit"}, optional
        Scale for the x-axis, default is ``"linear"``.
    yscale : {"linear", "log", "symlog", "logit"}, optional
        Scale for the y-axis, default is ``"linear"``.
    xlim : tuple, optional
        Limits for the x-axis, default is ``None`` (auto).
    ylim : tuple, optional
        Limits for the y-axis, default is ``None`` (auto).
    title : str, optional
        Title of the plot.
    grid : bool, optional
        Whether to display a grid, default is ``True``.
    duplicate_axes : bool, optional
        Whether to enable both top and bottom (x-axis) or left and right (y-axis) ticks.
    show : bool, optional
        Whether to display the plot, default is ``False``.
    path_save : str, optional
        File path to save the figure. If ``None``, the figure is not saved.
    alpha : float, optional
        Opacity of the points, default is ``0.5``.
    s : float, optional
        Size of the points, default is ``4``.
    label : str, optional
        Label for the plotted data (used in legends).
    **kwargs : dict
        Additional keyword arguments passed to `matplotlib.pyplot.scatter`.

    Returns
    -------
    matplotlib.axes.Axes
        The axis containing the plot.
    """
    if ax is None:
        fig = plt.figure(figsize=figsize, dpi=dpi)
        ax = fig.add_subplot()
    else:
        fig = ax.get_figure()

    ax.scatter(x, y, label=label, alpha=alpha, s=s, **kwargs)

    # ensure we don't perturb previous plots if the values are unset
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if xscale:
        ax.set_xscale(xscale)
    if yscale:
        ax.set_yscale(yscale)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if title:
        ax.set_title(title)
    if grid and not (hasattr(ax, "grid_is_enabled") and ax.grid_is_enabled):
        # needed or the grid is disabled when plotting two times
        ax.grid_is_enabled = True
        ax.grid()
    if duplicate_axes:
        ax.tick_params(axis="x", which="both", bottom=True, top=True)
        ax.tick_params(axis="y", which="both", left=True, right=True)
    if label:
        ax.legend()

    if path_save:
        fig.savefig(path_save)
    if show:
        fig.tight_layout()
        plt.show()
    return ax
