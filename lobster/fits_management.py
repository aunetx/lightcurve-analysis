"""
Utilities that are used to read FITS files from different sources and write them.
"""

import os
import datetime
import importlib.metadata
import numpy as np
from astropy.io import fits

from lobster.utils import version_is_higher_or_equal


def open_k2_fits(path):
    """
    Opens a FITS file in the format used by K2 and extract relevant data.

    Parameters
    ----------
    path : str
        The path of the K2 FITS file to load.

    Returns
    -------
    times : list of np.array
        A list of arrays containing the time values for each sector.
    fluxes : list of np.array
        A list of arrays containing the flux values for each sector.
    qualities : list of np.array
        A list of arrays containing the quality values for each sector.
    sectors : list of np.array
        A list of arrays containing the sector number for each sector.
    object_name : str
        The name of the object, here an EPIC id.
    unit : str
        The unit used for the flux data, here ``"e-/s"`` for K2.
    """
    times, fluxes, qualities, sectors = [], [], [], []
    with fits.open(path, mode="readonly") as hdulist:
        object_name = hdulist[0].header["OBJECT"]  # pylint: disable=no-member
        sector_num = hdulist[0].header["CAMPAIGN"]  # pylint: disable=no-member

        assert isinstance(hdulist[1], fits.hdu.table.BinTableHDU)
        data = hdulist[1].data  # pylint: disable=no-member

        times.append(data["TIME"])
        fluxes.append(data["FLUX"])
        qualities.append(data["QUALITY"])
        sectors.append(np.full_like(data["TIME"], sector_num))

    return times, fluxes, qualities, sectors, object_name, "e-/s"


def open_qlp_fits(path):
    """
    Opens a directory of FITS files in the format used by TESS-QLP and extract relevant data.

    Parameters
    ----------
    path : str
        The path of the TESS-QLP directory of FITS files to load.

    Returns
    -------
    times : list of np.array
        A list of arrays containing the time values for each sector.
    fluxes : list of np.array
        A list of arrays containing the flux values for each sector.
    qualities : list of np.array
        A list of arrays containing the quality values for each sector.
    sectors : list of np.array
        A list of arrays containing the sector number for each sector.
    object_name : str
        The name of the object, here a TIC id.
    unit : str
        The unit used for the flux data, here ``"ppm"`` for TESS-QLP.
    """
    times, fluxes, qualities, sectors = [], [], [], []
    for _, _, filenames in os.walk(path):
        for filename in filenames:
            fits_path = path + "/" + filename
            with fits.open(fits_path, mode="readonly") as hdulist:
                object_name = hdulist[0].header["OBJECT"]  # pylint: disable=no-member

                assert isinstance(hdulist[1], fits.hdu.table.BinTableHDU)
                data = hdulist[1].data  # pylint: disable=no-member
                sector_num = hdulist[0].header["SECTOR"]  # pylint: disable=no-member

                times.append(data["TIME"])
                fluxes.append(data["SAP_FLUX"])
                qualities.append(data["QUALITY"])
                sectors.append(np.full_like(data["TIME"], sector_num))

    assert len(times) > 0, "the FITS folder is empty"
    # sort the lists to ensure the time is continuous
    times, fluxes, qualities, sectors = zip(
        *sorted(zip(times, fluxes, qualities, sectors), key=lambda x: x[0][0])
    )

    return times, fluxes, qualities, sectors, object_name, "ppm"


def open_kbonus_fits(path):
    """
    Opens a FITS file in the format used by Kepler-BONUS and extract relevant data.

    Parameters
    ----------
    path : str
        The path of the Kepler-BONUS FITS file to load.

    Returns
    -------
    times : list of np.array
        A list of arrays containing the time values for each sector.
    fluxes : list of np.array
        A list of arrays containing the flux values for each sector.
    qualities : list of np.array
        A list of arrays containing the quality values for each sector.
    sectors : list of np.array
        A list of arrays containing the sector number for each sector.
    object_name : str
        The name of the object, here a KIC id.
    unit : str
        The unit used for the flux data, here ``"e-/s"`` for Kepler-BONUS.
    """
    times, fluxes, qualities, sectors = [], [], [], []
    with fits.open(path, mode="readonly") as hdulist:
        object_name = hdulist[0].header["OBJECT"]  # pylint: disable=no-member
        for i in range(2, len(hdulist), 2):
            assert isinstance(hdulist[i], fits.hdu.table.BinTableHDU)
            data = hdulist[i].data
            sector_num = hdulist[i].header["QUARTER"]
            unit = hdulist[i].columns["FLUX"].unit

            times.append(data["TIME"])
            fluxes.append(data["FLUX"])
            qualities.append(data["SAP_QUALITY"])
            sectors.append(np.full_like(data["TIME"], sector_num))

    times, fluxes, qualities, sectors = zip(
        *sorted(zip(times, fluxes, qualities, sectors), key=lambda x: x[0][0])
    )

    return times, fluxes, qualities, sectors, object_name, unit


def open_lobster_fits(path):
    """
    Opens a FITS file in the format used by Lobster and extract relevant data.

    It will automatically load the file the correct way, even if the format changed since the FITS
    file was produced.

    Parameters
    ----------
    path : str
        The path of the Lobster FITS file to load.

    Returns
    -------
    times : list of np.array
        A list of arrays containing the time values for each sector.
    fluxes : list of np.array
        A list of arrays containing the flux values for each sector.
    qualities : list of np.array
        A list of arrays containing the quality values for each sector.
    sectors : list of np.array
        A list of arrays containing the sector number for each sector.
    object_name : str
        The name of the object.
    unit : str
        The unit used for the flux data.
    """
    times, fluxes, qualities, sectors = [], [], [], []
    with fits.open(path, mode="readonly") as hdulist:
        creator = hdulist[0].header["CREATOR"]  # pylint: disable=no-member
        try:
            lobster_version = creator.split(" ")[1]
        except IndexError:
            # if could not find the current version, make sure to use the last version possible
            lobster_version = "99.99.99"

        if "OBJECT" in hdulist[0].header:  # pylint: disable=no-member
            object_name = hdulist[0].header["OBJECT"]  # pylint: disable=no-member
        else:
            object_name = ""

        unit = hdulist[1].columns["FLUX"].unit  # pylint: disable=no-member
        data = hdulist[1].data  # pylint: disable=no-member

        times.append(data["TIME"])
        fluxes.append(data["FLUX"])

        if version_is_higher_or_equal(lobster_version, "1.0.5"):
            qualities.append(data["QUALITY"])
            sectors.append(data["SECTOR"])
        else:
            qualities.append(np.full_like(data["TIME"], -1).astype(int))
            sectors.append(np.full_like(data["TIME"], -1).astype(int))

    return times, fluxes, qualities, sectors, object_name, unit


def write_to_fits(time, flux, quality, sector, operations, unit, fits_path, object_name=None):
    """
    Save time and flux data to a FITS file.

    This function creates a FITS file with two HDUs, similarly to a Kepler-BONUS one:

    - A Primary HDU containing metadata.
    - A Binary Table HDU storing:
      - The time array in column ``"TIME"``,
      - The flux array in column ``"FLUX"``,
      - The quality array in column ``"QUALITY"``,
      - The sector array in column ``"SECTOR"``.

    If an object name is provided, it is added to the Primary HDU header.
    Additionally, the history of operations is recorded in the Binary Table HDU header.

    Parameters
    ----------
    time : array-like
        The time values to be stored in the FITS file.
    flux : array-like
        The flux values corresponding to the `time` values.
    quality : array-like
        The quality values corresponding to the `time` values.
    sector : array-like
        The sector values corresponding to the `time` values.
    operations : list
        A list of operations performed on the data, stored in the FITS header.
    unit : str
        The unit of the flux values.
    fits_path : str
        The file path where the FITS file will be saved.
    object_name : str, optional
        Name of the observed object, added to the FITS header if provided.

    Returns
    -------
    None
        The function writes a FITS file to the specified path but does not return any value.
    """
    # Primary HDU, contains general data about the lightcurve
    primary_hdu = fits.PrimaryHDU()

    try:
        creator = f"Lobster {importlib.metadata.version('lobster')}"
    except:  # pylint: disable=bare-except
        creator = "Lobster"

    default = {
        "DATE": datetime.datetime.now().strftime("%Y-%m-%d"),
        "CREATOR": creator,
    }

    if object_name:
        default["OBJECT"] = object_name

    for kw, value in default.items():
        primary_hdu.header[kw] = value

    # Lightcurve HDU, contains the TIME and FLUX values
    cols = [
        fits.Column(
            name="TIME",
            format="D",
            unit="d",
            array=time,
        ),
        fits.Column(
            name="FLUX",
            format="D",
            unit=unit,
            array=flux,
        ),
        fits.Column(
            name="QUALITY",
            format="J",
            unit=None,
            array=quality,
        ),
        fits.Column(
            name="SECTOR",
            format="J",
            unit=None,
            array=sector,
        ),
    ]

    coldefs = fits.ColDefs(cols)
    lightcurve_hdu = fits.BinTableHDU.from_columns(coldefs)
    lightcurve_hdu.header["EXTNAME"] = "LIGHTCURVE"

    for i, operation in enumerate(operations):
        lightcurve_hdu.header["HISTORY"] = f"{i}: {operation}"

    # write both into the FITS file
    fits.HDUList(
        [
            primary_hdu,
            lightcurve_hdu,
        ]
    ).writeto(fits_path, overwrite=True)
