"""
The module where all the stitching functions and related utilities are defined.

They are meant to be used by the `Lightcurve` class, but could possibly
be used by themselves in some cases; although their usage might be difficult.

If you just want to stitch a lightcurve, please refer to the `Lightcurve.stitch`
method which manages everything automatically.
"""

import matplotlib.pyplot as plt
import scipy.optimize
import numpy as np

from lobster.utils import is_all_invalid
from lobster.maths import calculate_bic, optimal_linear_estimation_irregular

AVAILABLE_METHODS = {
    "total-mean": {
        "min-jump": 0,
        "max-jump": np.inf,
        "max-jump-multiplicator": np.inf,
    },
    "tips-mean": {
        "min-width": 0,
        "max-width": 3,
        "min-jump": 0,
        "max-jump": 30,
        "max-jump-multiplicator": 10,
        "constrain-equal-width": True,
    },
    "tips-poly-1": {
        "min-width": 0,
        "max-width": 3,
        "min-jump": 0,
        "max-jump": 30,
        "max-jump-multiplicator": 10,
        "constrain-equal-width": False,
    },
    "tips-poly-2": {
        "min-width": 0,
        "max-width": 3,
        "min-jump": 0,
        "max-jump": 10,
        "max-jump-multiplicator": 3,
        "constrain-equal-width": False,
    },
    "tips-poly-3": {
        "min-width": 0,
        "max-width": 3,
        "min-jump": 0,
        "max-jump": 10,
        "max-jump-multiplicator": 3,
        "constrain-equal-width": False,
    },
    "tips-sin": {},
}
"""
The stitching methods that are available for the `stitch` function, and their
default parameters.

Most methods can be used either as a stitching method (it will try to calculate the difference
between the flux before and after the flux); or as a choosing method (which is used to compare
the different stitching methods used for a particular jump).

However, the method ``"tips-sin"`` can only be used as a choosing method, because stitching with
a sinusoidal model, although very tempting, is currently quite slow, unstable and under-performing.
"""


def stitch(times, fluxes, jumps, choose_with_bic=None, debug=False, verbose=True):
    """
    Stitches together flux time series by selecting the difference calculated using the model
    that has the lowest BIC.

    Parameters
    ----------
    times : list of ndarray
        A list of time arrays, where each entry corresponds to a section separated by the jumps.
    fluxes : list of ndarray
        A list of flux arrays corresponding to `times`.
    jumps : dict
        Dictionary mapping indices of jumps to their metadata, including:

        - ``"stitching_methods"`` : list of dict
            Each dictionary defines a method and its parameters for stitching.
    choose_with_bic : str, optional
        The method to use for BIC comparison, overriding the default of each stitching method.
    debug : bool, default=False
        If ``True``, generates diagnostic plots of the stitching process.
    verbose : bool, default=True
        If ``True``, prints information about the stitching process, including chosen methods.

    Returns
    -------
    concatenated_fluxes : ndarray
        The fully stitched flux array, where discontinuities have been corrected.
    chosen_methods : list of str
        The list of stitching methods chosen for each jump.

    Examples
    --------
    >>> times = [np.linspace(0, 10, 100), np.linspace(12, 20, 120)]
    >>> fluxes = [np.linspace(10, 15, 100), np.linspace(14, 18, 120)]
    >>>
    >>> jumps = {3: {"stitching_methods": [
    ...     {"method": "tips-mean"}, {"method": "tips-poly-2"}
    ... ]}}
    >>> stitched_flux, chosen_methods = stitch(times, fluxes, jumps, verbose=False)
    >>> stitched_flux
    array([10. , ..., 20. ])
    >>> chosen_methods
    ['tips-poly-2']
    """
    calculate_diffs_for_stitching(
        times, fluxes, jumps, choose_with_bic=choose_with_bic, debug=debug, verbose=verbose
    )
    return concatenate_fluxes_with_diffs(times, fluxes, jumps, debug=debug, verbose=verbose)


def concatenate_fluxes_with_diffs(times, fluxes, jumps, debug=False, verbose=True):
    """
    Stitches together flux time series by selecting the difference calculated using the model
    that has the lowest BIC, as computed by `calculate_diffs_for_stitching` earlier.

    Parameters
    ----------
    times : list of ndarray
        A list of time arrays, where each entry corresponds to a section separated by the jumps.
    fluxes : list of ndarray
        A list of flux arrays corresponding to `times`.
    jumps : dict
        Dictionary mapping indices of jumps to their metadata, including:

        - ``"stitching_methods"`` : list of dict
            Each dictionary defines a method and its parameters for stitching.
        - ``"debug_fig"`` and ``"debug_axes"`` : matplotlib.pyplot.Figure and matplotlib.axes.Axes
            Used for debug plots if `debug` is ``True``.
    debug : bool, default=False
        If ``True``, generates diagnostic plots of the stitching process.
    verbose : bool, default=True
        If ``True``, prints information about chosen diff for each jump.

    Returns
    -------
    concatenated_fluxes : ndarray
        The fully stitched flux array, where discontinuities have been corrected.
    chosen_methods : list of str
        The list of stitching methods chosen for each jump.

    Notes
    -----
    - The function iterates through jumps, selecting the method with the lowest unified BIC.
    - If all methods for a jump are dropped, no adjustment is made for that jump.
    - The chosen flux difference is applied cumulatively to all subsequent flux segments.
    - If `debug=True`, diagnostic plots of the stitched fluxes are generated.

    Examples
    --------
    >>> times = [np.array([1, 2, 3]), np.array([4, 5, 6])]
    >>> fluxes = [np.array([10, 12, 14]), np.array([18, 20, 22])]
    >>> jumps = {0: {"stitching_methods": [
    ...     {"method": "tips-poly-2", "diff": 2, "unified-BIC": 10, "should-drop": False},
    ...     {"method": "tips-mean", "diff": 4, "unified-BIC": 30, "should-drop": False}
    ... ]}}
    >>> stitched_flux, chosen_methods = concatenate_fluxes_with_diffs(
    ...     times, fluxes, jumps, verbose=False
    ... )
    >>> np.testing.assert_array_equal(stitched_flux, np.array([10, 12, 14, 16, 18, 20]))
    >>> chosen_methods
    ['tips-poly-2']
    """
    concatenated_fluxes = [flux.copy() for flux in fluxes]
    chosen_methods = []

    for i, jump in enumerate(jumps):
        t_jump = np.nan if len(times[i + 1]) == 0 else times[i + 1][0]

        stitching_methods = jumps[jump]["stitching_methods"]

        chosen_diff = 0
        chosen_method = "none"
        min_bic = np.inf
        for method in stitching_methods:
            diff = method["diff"]
            unified_bic = method["unified-BIC"]
            if not method["should-drop"] and unified_bic < min_bic:
                min_bic = unified_bic
                chosen_diff = diff
                chosen_method = method["method"]

        chosen_methods.append(chosen_method)
        if verbose:
            if min_bic == np.inf:
                print(f"Warning: every method has been dropped for jump at t={t_jump:.4f} d")
            else:
                print(f"Choosing {chosen_diff} for jump at t={t_jump:.4f} d")

        for flux in concatenated_fluxes[i + 1 :]:
            flux -= chosen_diff

        if debug:
            fig = jumps[jump]["debug_fig"]
            axes = jumps[jump]["debug_axes"]
            axes[0].scatter(
                times[i + 1],
                fluxes[i + 1] - chosen_diff,
                label="stitched",
                s=1,
                c="darkkhaki",
                zorder=-100,
            )
            axes[0].set_title(f"Stitched flux, chosen {chosen_method}")
            for ax in axes:
                ax.legend(fontsize="small", ncols=2)
                ax.set_xlabel("Time (d)")
                ax.set_ylabel("Flux")
            fig.tight_layout()
            fig.show()

    return np.concatenate(concatenated_fluxes), chosen_methods


def generate_tips_for_method(
    method, valid_time_left, valid_time_right, valid_flux_left, valid_flux_right, real_jump_gap
):
    """
    Generates the tips of the left and right data, both for the stitching phase and the choosing
    phase.

    These tips correspond to the end of the left section, and the beginning of the right section.
    The lenght of these tips is choosen based on constraints contained in the `method` dictionnary,
    that are the maximum width of the tips, and whether or not to force ourselves having tips of
    equal width; along with the natural constraints of the sections such as its real lenght.

    Parameters
    ----------
    method : dict
        Dictionary containing at least two parameters:

        - ``"max-width"`` : float
            The maximum width for each tip.
        - ``"constrain-equal-width"`` : bool
            If ``True``, ensures that both left and right tips will have equal width, even if
            one of the sections is too short to reach ``"max-width"``.
    valid_time_left : array-like
        Time values corresponding to the valid part of the left section.
    valid_time_right : array-like
        Time values corresponding to the valid part of the right section.
    valid_flux_left : array-like
        Flux values corresponding to `valid_time_left`.
    valid_flux_right : array-like
        Flux values corresponding to `valid_time_right`.
    real_jump_gap : float
        The time gap inside the jump, including NaNs.

    Returns
    -------
    left_fit_time : ndarray
        Time values for the left tip used in fitting.
    right_fit_time : ndarray
        Time values for the right tip used in fitting.
    left_fit_flux : ndarray
        Flux values for the left tip used in fitting.
    right_fit_flux : ndarray
        Flux values for the right tip used in fitting.
    left_bic_time : ndarray
        Time values for the left tip used in BIC calculation.
    right_bic_time : ndarray
        Time values for the right tip used in BIC calculation.
    left_bic_flux : ndarray
        Flux values for the left tip used in BIC calculation.
    right_bic_flux : ndarray
        Flux values for the right tip used in BIC calculation.
    min_tip_width : float
        The final minimum width between the left and right tips.

    Notes
    -----
    - The function first selects initial tip regions based on ``method["max-width"]``, ensuring
      they fit within the valid time range.
    - If ``method["constrain-equal-width"]`` is ``True``, the tip width is adjusted to be the
      minimum of both segments.
    - BIC evaluation tips are extended beyond the fitting tips by ``real_jump_gap / 2`` on each
      side, if there is enough data. This allows to evaluate the models in data that they have
      not been fitted with, and this particular extension duration is based on the though that if
      a jump is 4 days long and a model behaves well 2 days further than it has been fitted on,
      then it may as well behave well at the midpoint of the jump too.

    Examples
    --------
    >>> valid_time_left = np.linspace(0, 10, 200)
    >>> valid_time_right = np.linspace(12, 16, 150)
    >>> valid_flux_left = np.sin(valid_time_left)
    >>> valid_flux_right = np.sin(valid_time_right)
    >>> real_jump_gap = 2. # jump between 10d and 12d
    >>> left_fit_time, right_fit_time, left_fit_flux, right_fit_flux,\
        left_bic_time, right_bic_time, left_bic_flux, right_bic_flux,\
        min_tip_width = generate_tips_for_method(
    ...     {"max-width": 6.0, "constrain-equal-width": False},
    ...     valid_time_left, valid_time_right,
    ...     valid_flux_left, valid_flux_right, real_jump_gap
    ... )
    >>> length = lambda arr: float(np.round(arr[-1] - arr[0], 1))
    >>> length(left_fit_time)
    6.0
    >>> length(right_fit_time)
    4.0
    >>> length(left_bic_time)
    7.0
    >>> length(right_bic_time)
    4.0
    >>> np.testing.assert_array_equal(right_fit_flux, valid_flux_right)
    >>> np.testing.assert_array_equal(right_bic_flux, valid_flux_right)

    >>> valid_time_left = np.linspace(0, 5, 200)
    >>> valid_time_right = np.linspace(8, 16, 250)
    >>> valid_flux_left = np.sin(valid_time_left)
    >>> valid_flux_right = np.sin(valid_time_right)
    >>> real_jump_gap = 3. # jump between 5d and 8d
    >>> left_fit_time, right_fit_time, left_fit_flux, right_fit_flux,\
        left_bic_time, right_bic_time, left_bic_flux, right_bic_flux,\
        min_tip_width = generate_tips_for_method(
    ...     {"max-width": 6.0, "constrain-equal-width": True},
    ...     valid_time_left, valid_time_right,
    ...     valid_flux_left, valid_flux_right, real_jump_gap
    ... )
    >>> length = lambda arr: float(np.round(arr[-1] - arr[0], 1))
    >>> length(left_fit_time)
    5.0
    >>> length(right_fit_time)
    5.0
    >>> length(left_bic_time)
    5.0
    >>> length(right_bic_time)
    6.5
    >>> np.testing.assert_array_equal(left_fit_flux, valid_flux_left)
    >>> np.testing.assert_array_equal(left_bic_flux, valid_flux_left)
    """
    # * tips used to fit the method

    # the begin (end) time for the left (right) section we would like to see hapenning
    left_tip_begin_time = valid_time_left[-1] - method["max-width"]
    right_tip_end_time = valid_time_right[0] + method["max-width"]

    # the actual bin corresponding to the closest begin (end) time for the left (right)
    # section, which encompasses the entire valid section if it is too small
    left_tip_begin_bin = np.searchsorted(valid_time_left, left_tip_begin_time, side="left")
    right_tip_end_bin = np.searchsorted(valid_time_right, right_tip_end_time, side="right")

    # if we constrain to have equal tip width, we then redo the same operation with
    # the minimum width of the two tips
    min_tip_width = min(
        valid_time_left[-1] - valid_time_left[left_tip_begin_bin],
        valid_time_right[right_tip_end_bin - 1] - valid_time_right[0],
    )

    if method["constrain-equal-width"]:
        left_tip_begin_time = valid_time_left[-1] - min_tip_width
        right_tip_end_time = valid_time_right[0] + min_tip_width

        # same as before, but now we have equal width
        left_tip_begin_bin = np.searchsorted(valid_time_left, left_tip_begin_time, side="left")
        right_tip_end_bin = np.searchsorted(valid_time_right, right_tip_end_time, side="right")

    # the left and right tips times
    left_fit_time = valid_time_left[left_tip_begin_bin:]
    right_fit_time = valid_time_right[:right_tip_end_bin]

    # the left and right tips fluxes
    left_fit_flux = valid_flux_left[left_tip_begin_bin:]
    right_fit_flux = valid_flux_right[:right_tip_end_bin]

    # * tips used to calculate the BIC, overshoots the previous tips up to the encounter point

    # the begin (end) time for the left (right) section we will see hapenning
    left_tip_bic_begin_time = left_tip_begin_time - real_jump_gap / 2
    right_tip_bic_end_time = right_tip_end_time + real_jump_gap / 2

    # the bin corresponding to the previous times
    left_tip_bic_begin_bin = np.searchsorted(valid_time_left, left_tip_bic_begin_time, side="left")
    right_tip_bic_end_bin = np.searchsorted(valid_time_right, right_tip_bic_end_time, side="right")

    # the left and right tips times for the BIC testing
    left_bic_time = valid_time_left[left_tip_bic_begin_bin:]
    right_bic_time = valid_time_right[:right_tip_bic_end_bin]

    # the left and right tips fluxes for the BIC testing
    left_bic_flux = valid_flux_left[left_tip_bic_begin_bin:]
    right_bic_flux = valid_flux_right[:right_tip_bic_end_bin]

    return (
        left_fit_time,
        right_fit_time,
        left_fit_flux,
        right_fit_flux,
        left_bic_time,
        right_bic_time,
        left_bic_flux,
        right_bic_flux,
        min_tip_width,
    )


def calculate_diffs_for_stitching(
    times, fluxes, jumps, choose_with_bic=None, debug=False, verbose=True
):
    """
    Computes the flux differences for stitching time-series data across jumps,
    applying various stitching methods and evaluating their Bayesian Information Criterion (BIC).

    Parameters
    ----------
    times : list of ndarray
        A list of time arrays, where each entry corresponds to a section separated by the jumps.
    fluxes : list of ndarray
        A list of flux arrays corresponding to `times`.
    jumps : dict
        Dictionary mapping indices of jumps to their metadata, including:

        - ``"stitching_methods"`` : list of dict
            Each dictionary defines a method and its parameters for stitching.
        - ``"debug_fig"`` and ``"debug_axes"`` : matplotlib.pyplot.Figure and matplotlib.axes.Axes
            Used for debug plots if `debug` is ``True``.
    choose_with_bic : str, optional
        The method to use for BIC evaluation instead of the default stitching method.
        If ``None``, each method is evaluated with its own model.
    debug : bool, default=False
        If ``True``, generates diagnostic plots of the stitching process.
    verbose : bool, default=False
        If ``True``, prints debug information about invalid flux handling.

    Returns
    -------
    None
        The function modifies the dictionnary `jumps` in place by updating each stitching
        method with:

        - ``"should-drop"`` : bool
            Whether the method should be discarded, in case the left or right flux is
            all invalids or fitting the model failed.
        - ``"diff"`` : float
            The computed flux difference.
        - ``"separate-BIC"`` : float
            BIC for separately fitted models.
        - ``"unified-BIC"`` : float
            BIC for the unified stitched model.

    Notes
    -----
    - Available stitching models include:

      - ``"total-mean"``: Uses the total mean difference.
      - ``"tips-mean"``: Uses the mean difference at selected time tips.
      - ``"tips-poly-n"``, with ``n`` being 1, 2 or 3:
        Fits :math:`n`-order polynomials to both segments.
    - BIC evaluation is performed separately for each method, with an option to override
      using `choose_with_bic`. The ``"tips-sin"`` model can be used here, along with
      any stitching model.

    Examples
    --------
    >>> times = [np.linspace(0, 2, 100), np.linspace(4.5, 6.5, 120)]
    >>> fluxes = [np.linspace(10, 14, 100), np.linspace(16, 20, 120)]
    >>>
    >>> jumps = {3: {"stitching_methods": [{"method": "total-mean"}]}}
    >>> calculate_diffs_for_stitching(times, fluxes, jumps)
    >>> float(np.round(jumps[3]["stitching_methods"][0]["diff"], 2))
    6.0
    >>> bic_mean = jumps[3]["stitching_methods"][0]["unified-BIC"]
    >>>
    >>> jumps = {3: {"stitching_methods": [{"method": "tips-poly-1"}]}}
    >>> calculate_diffs_for_stitching(times, fluxes, jumps)
    >>> float(np.round(jumps[3]["stitching_methods"][0]["diff"], 2))
    -3.0
    >>> bic_poly = jumps[3]["stitching_methods"][0]["unified-BIC"]
    >>>
    >>> assert bic_mean > bic_poly
    """
    for i, jump in enumerate(jumps):
        flux_left, flux_right = fluxes[i], fluxes[i + 1]
        time_left, time_right = times[i], times[i + 1]

        stitching_methods = jumps[jump]["stitching_methods"]

        t_jump = np.nan if len(time_right) == 0 else times[i + 1][0]

        j = i
        while j > 0 and is_all_invalid(flux_left):
            # if the left flux is invalid, then we search fluxes before to try stitching anyway
            if debug and verbose:
                print(
                    f"Jump at t={t_jump:.4f} d, left flux is entirely invalid, taking one before it"
                )
            j -= 1
            flux_left = fluxes[j]
            time_left = times[j]

        jumps[jump]["debug_ax"] = None
        if debug:
            n_axes = len(stitching_methods) + 1
            fig, axes = plt.subplots(n_axes, figsize=(10, 4 * n_axes))
            jumps[jump]["debug_fig"] = fig
            jumps[jump]["debug_axes"] = axes
            axes[0].scatter(time_left, flux_left, label="left flux", s=1.4, c="royalblue")
            axes[0].scatter(time_right, flux_right, label="right flux", s=1.4, c="lightcoral")

        if is_all_invalid(flux_left) or is_all_invalid(flux_right):
            # no need to do anything: we have no data on the left or on the right
            if debug and verbose:
                print(
                    f"Cannot adjust for jump at t={t_jump:.4f} d, left or right flux is"
                    + " entirely invalid"
                )
            for method in stitching_methods:
                method["should-drop"] = True
                method["diff"] = 0
                method["separate-BIC"] = 0
                method["unified-BIC"] = 0

        else:
            # left and right fluxes with only valid values
            valid_flux_left = flux_left[np.isfinite(flux_left)]
            valid_flux_right = flux_right[np.isfinite(flux_right)]

            # left and right times for the previous fluxes
            valid_time_left = time_left[np.isfinite(flux_left)]
            valid_time_right = time_right[np.isfinite(flux_right)]

            # the jump in time actually happening
            real_jump_gap = valid_time_right[0] - valid_time_left[-1]

            # we create there arrays here
            for method_idx, method in enumerate(stitching_methods):
                # using total-mean is just an infinitely long tips-mean, simplifies the rest
                if method["method"] == "total-mean":
                    method["min-width"] = 0
                    method["max-width"] = np.inf
                    method["constrain-equal-width"] = False

                # verify the stitching model described in the stitching method exists
                stitching_model = method["method"]
                if stitching_model not in AVAILABLE_METHODS:
                    raise ValueError(f"could not find stitching method '{stitching_model}'")

                # set default arguments for this stitching method if they are unset
                default_method = AVAILABLE_METHODS[stitching_model]
                for default_method_args in default_method:
                    method[default_method_args] = method.get(
                        default_method_args, default_method[default_method_args]
                    )

                (
                    left_fit_time,
                    right_fit_time,
                    left_fit_flux,
                    right_fit_flux,
                    left_bic_time,
                    right_bic_time,
                    left_bic_flux,
                    right_bic_flux,
                    min_tip_width,
                ) = generate_tips_for_method(
                    method,
                    valid_time_left,
                    valid_time_right,
                    valid_flux_left,
                    valid_flux_right,
                    real_jump_gap,
                )

                ax = None
                if debug:
                    ax = jumps[jump]["debug_axes"][method_idx + 1]

                # * we verify whether or not the method should be dropped
                assert method["min-width"] < method["max-width"]
                if (
                    method["min-width"] > min_tip_width
                    or method["min-jump"] > real_jump_gap
                    or method["max-jump"] < real_jump_gap
                    or method["max-jump-multiplicator"] * min_tip_width < real_jump_gap
                ):
                    method["should-drop"] = True
                else:
                    method["should-drop"] = False

                # * we fit the method for stitching
                if stitching_model in ["total-mean", "tips-mean"]:
                    diff, separate_bic = tips_mean_stitch(
                        left_fit_flux,
                        right_fit_flux,
                        left_bic_flux,
                        right_bic_flux,
                        debug,
                        ax,
                    )

                elif stitching_model[:10] == "tips-poly-":
                    poly_deg = int(stitching_model[10:])
                    diff, separate_bic = tips_poly_stitch(
                        poly_deg,
                        left_fit_time,
                        right_fit_time,
                        left_fit_flux,
                        right_fit_flux,
                        left_bic_time,
                        right_bic_time,
                        left_bic_flux,
                        right_bic_flux,
                        debug,
                        ax,
                    )

                else:
                    raise ValueError(f"unknown stitching model {stitching_model}")

                # we either use the stitching model for the unified BIC, or the provided model
                choosing_model = method["method"]
                if choose_with_bic:
                    choosing_model = choose_with_bic
                if choosing_model not in AVAILABLE_METHODS:
                    raise ValueError(f"could not find choosing method '{choosing_model}'")

                continued_fit_time = np.concatenate([left_fit_time, right_fit_time])
                continued_fit_flux = np.concatenate([left_fit_flux, right_fit_flux - diff])

                continued_bic_time = np.concatenate([left_bic_time, right_bic_time])
                continued_bic_flux = np.concatenate([left_bic_flux, right_bic_flux - diff])

                if choosing_model in ["total-mean", "tips-mean"]:
                    unified_bic = tips_mean_bic(
                        continued_fit_flux,
                        continued_bic_flux,
                        debug,
                        ax,
                    )

                elif choosing_model[:10] == "tips-poly-":
                    poly_deg = int(choosing_model[10:])
                    unified_bic = tips_poly_bic(
                        poly_deg,
                        continued_fit_time,
                        continued_fit_flux,
                        continued_bic_time,
                        continued_bic_flux,
                        debug,
                        ax,
                    )

                elif choosing_model[:10] == "tips-sin":
                    unified_bic = tips_sin_bic(
                        continued_fit_time,
                        continued_fit_flux,
                        continued_bic_time,
                        continued_bic_flux,
                        debug,
                        ax,
                    )

                else:
                    raise ValueError(f"unknown choosing model {choosing_model}")

                if debug:
                    ax.scatter(time_left, flux_left, label="left flux", s=1.4, c="royalblue")
                    ax.scatter(time_right, flux_right, label="right flux", s=1.4, c="lightcoral")
                    ax.scatter(
                        time_right,
                        flux_right - diff,
                        label="stitched right flux",
                        s=1.4,
                        c="darkkhaki",
                    )
                    ax.scatter(
                        left_fit_time, left_fit_flux, label="left fit flux", s=1.4, c="mediumblue"
                    )
                    ax.scatter(
                        right_fit_time, right_fit_flux, label="right fit flux", s=1.4, c="darkred"
                    )
                    ax.scatter(
                        right_fit_time,
                        right_fit_flux - diff,
                        label="stitched right fit flux",
                        s=1.4,
                        c="olive",
                    )
                    ax.axvline(
                        (time_left[-1] + time_right[0]) / 2,
                        ls="--",
                        c="black",
                        lw=0.7,
                        alpha=0.5,
                    )
                    ax.set_title(f"Method {method['method']}")

                # we finally write the infos for the method
                method["diff"] = diff
                method["separate-BIC"] = separate_bic
                method["unified-BIC"] = unified_bic
    return


def tips_mean_stitch(
    left_flux,
    right_flux,
    left_bic_flux,
    right_bic_flux,
    debug,
    ax,
):
    """
    Use the mean as models on the left and right data respectively, and use those models to
    compute the difference in flux between the left and right flux.

    It then computes the BIC of those two models independently and add them together, to form
    an alternative evaluation method of the mean models than what the choosing method
    later computes.

    This method is used for both ``"total-mean"`` where it is fitted on the whole left and right
    fluxes, and for ``"tips-mean"`` where is it fitted only on the end of the left flux and
    beginning of the right flux (the "tips" of the sections).

    Parameters
    ----------
    left_flux : array-like
        The flux values on the left to calculate the mean for the model.
    right_flux : array-like
        The flux values on the right to calculate the mean for the model.
    left_bic_flux : array-like
        The flux values on the left where the model will be evaluated for BIC calculation.
    right_bic_flux : array-like
        The flux values on the right where the model will be evaluated for BIC calculation.
    debug : bool
        If ``True``, the function plots the fitted mean models and BIC calculation
        information on the provided axis.
    ax : matplotlib.axes.Axes
        The Matplotlib axis on which to plot the fitted mean models if `debug` is ``True``.
        If `debug` is ``False``, can be ``None``.

    Returns
    -------
    diff : float
        The difference in flux that corresponds to the jump between the left and right flux,
        according to the mean models.

        If there are not enough valid data points to calculate the means, returns ``np.nan``.
    separate_bic : float
        The Bayesian Information Criterion (BIC) value for the mean models fitted on
        the `left_flux` (`right_flux`) values, and evaluated on `left_bic_flux`
        (`right_bic_flux`). These two independent BIC values are added to for the `separate_bic`.

        If there are not enough valid data points to calculate the means, returns ``np.nan``.

    Examples
    --------
    .. plot::
        :include-source:

        import numpy as np
        from lobster.plotting import plot
        from lobster.stitching import tips_mean_stitch

        time = np.linspace(0, .4, 1000)
        flux = 2.0 * np.sin(2 * np.pi * time) + np.random.normal(0, 0.1, len(time))

        left_time = time[:400]
        left_flux = flux[:400]
        right_time = time[600:]
        right_flux = flux[600:] + .8

        left_bic_flux = left_flux[100:]
        right_bic_flux = right_flux[:900]

        ax = plot(left_time, left_flux, label="left flux", xlabel="time", ylabel="flux")
        plot(right_time, right_flux, label="right flux", ax=ax)
        diff, separate_bic = tips_mean_stitch(
            left_flux, right_flux, left_bic_flux, right_bic_flux,
            debug=True, ax=ax
        )
        ax.legend()
        ax.set_title(f"Found diff = {diff:.3f} with separate_bic = {separate_bic:.2f}")
        ax.get_figure().tight_layout()
    """
    left_mean = np.nanmean(left_flux)
    right_mean = np.nanmean(right_flux)

    separate_data = np.concatenate((left_bic_flux, right_bic_flux))
    separate_model = np.concatenate(
        (
            np.full_like(left_bic_flux, left_mean),
            np.full_like(right_bic_flux, right_mean),
        )
    )

    diff = right_mean - left_mean
    separate_bic = calculate_bic(separate_data, separate_model, 2)

    if debug:
        ax.axhline(left_mean, label="left mean", ls="--", lw=0.9, c="darkorange")
        ax.axhline(right_mean, label="right mean", ls="--", lw=0.9, c="limegreen")

    return diff, separate_bic


def tips_mean_bic(
    flux,
    bic_flux,
    debug,
    ax,
):
    """
    Use the mean as model for the given continuous data and computes the BIC for model evaluation.

    Parameters
    ----------
    flux : array-like
        The flux values to calculate the mean for the model.
    bic_flux : array-like
        The flux values where the model will be evaluated for BIC calculation.
    debug : bool
        If ``True``, the function plots the fitted model and BIC calculation information
        on the provided axis.
    ax : matplotlib.axes.Axes
        The Matplotlib axis on which to plot the fitted model if `debug` is ``True``.
        If `debug` is ``False``, can be ``None``.

    Returns
    -------
    unified_bic : float
        The Bayesian Information Criterion (BIC) value for the mean model fitted on
        the continuous `flux` values, and evaluated on `bic_flux`.

    Examples
    --------
    .. plot::
        :include-source:

        import numpy as np
        from lobster.plotting import plot
        from lobster.stitching import tips_mean_bic

        time = np.linspace(0, .4, 1000)
        flux = 2.0 * np.sin(2 * np.pi * time) + np.random.normal(0, 0.1, len(time))
        bic_flux = flux[500:800]

        ax = plot(time, flux, label="flux", xlabel="time", ylabel="flux")
        bic = tips_mean_bic(flux, bic_flux, debug=True, ax=ax)
        ax.legend()
        ax.set_title(f"Found bic = {bic:.2f}")
        ax.get_figure().tight_layout()
    """
    unified_mean = np.nanmean(flux)
    unified_model_points = np.full_like(bic_flux, unified_mean)
    unified_bic = calculate_bic(bic_flux, unified_model_points, 1)

    if debug:
        ax.axhline(unified_mean, label="unified mean", ls=":", lw=0.9, c="deeppink")

    return unified_bic


def tips_poly_stitch(
    poly_order,
    left_time,
    right_time,
    left_flux,
    right_flux,
    left_bic_time,
    right_bic_time,
    left_bic_flux,
    right_bic_flux,
    debug,
    ax,
):
    """
    Fits polynomial models on the left and right data respectively, and use those models to
    compute the difference in flux between the left and right flux.

    It then computes the BIC of those two models independently and add them together, to form
    an alternative evaluation method of the polynomial models than what the choosing method
    later computes.

    Parameters
    ----------
    poly_order : array-like
        The order of the polynomial to use as model.
    left_time : array-like
        The time values corresponding to the flux measurement for the left data.
    right_time : array-like
        The time values corresponding to the flux measurement for the right data.
    left_flux : array-like
        The flux values corresponding to `left_time` to be modeled with a polynomial trend.
    right_flux : array-like
        The flux values corresponding to `right_time` to be modeled with a polynomial trend.
    left_bic_time : array-like
        The time values at which the BIC is evaluated for the left data.
    right_bic_time : array-like
        The time values at which the BIC is evaluated for the right data.
    left_bic_flux : array-like
        The flux values corresponding to `left_bic_time` for BIC calculation.
    right_bic_flux : array-like
        The flux values corresponding to `right_bic_time` for BIC calculation.
    debug : bool
        If ``True``, the function plots the fitted polynomial models and BIC calculation
        information on the provided axis.
    ax : matplotlib.axes.Axes
        The Matplotlib axis on which to plot the fitted polynomial models if `debug` is ``True``.
        If `debug` is ``False``, can be ``None``.

    Returns
    -------
    diff : float
        The difference in flux that corresponds to the jump between the left and right flux,
        according to the polynomial models.

        If there are not enough valid data points to fit the polynomials, returns ``0``.
    separate_bic : float
        The Bayesian Information Criterion (BIC) value for the polynomial models fitted on
        the `left_time` (`right_time`) and `left_flux` (`right_flux`) values, and evaluated
        on `left_bic_time` (`right_bic_time`) and `left_bic_flux` (`right_bic_flux`).
        These two independent BIC values are added to for the `separate_bic`.

        If there are not enough valid data points to fit the polynomials, returns ``np.inf``.

    Notes
    -----
    - The polynomial model used is:

      .. math::
          P(t) = c_0 + c_1 t + c_2 t^2 + ... + c_n t^n

      Where :math:`n` is the polynomial order and :math:`c_i` are the fitted coefficients.

    - This polynomial is fitted to the left and right data independently using
      `numpy.polynomial.Polynomial.fit`.

    - The difference is computed at the midpoint between the two segments,

      .. math::
          t_{\\text{middle}} = \\frac{t_{\\text{left}}[-1] + t_{\\text{right}}[0]}{2}

    - The difference at the midpoint is:

      .. math::
          \\Delta = P_{\\text{right}}(t_{\\text{middle}}) - P_{\\text{left}}(t_{\\text{middle}})

    Examples
    --------
    .. plot::
        :include-source:

        import numpy as np
        from lobster.plotting import plot
        from lobster.stitching import tips_poly_stitch

        time = np.linspace(0, .4, 1000)
        flux = 2.0 * np.sin(2 * np.pi * time) + np.random.normal(0, 0.1, len(time))

        left_time = time[:400]
        left_flux = flux[:400]
        right_time = time[600:]
        right_flux = flux[600:] + .8

        left_bic_time = left_time[100:]
        left_bic_flux = left_flux[100:]
        right_bic_time = right_time[:900]
        right_bic_flux = right_flux[:900]

        ax = plot(left_time, left_flux, label="left flux", xlabel="time", ylabel="flux")
        plot(right_time, right_flux, label="right flux", ax=ax)
        diff, separate_bic = tips_poly_stitch(
            2,
            left_time, right_time, left_flux, right_flux,
            left_bic_time, right_bic_time, left_bic_flux, right_bic_flux,
            debug=True, ax=ax
        )
        ax.legend()
        ax.set_title(f"Found diff = {diff:.3f} with separate_bic = {separate_bic:.2f}")
        ax.get_figure().tight_layout()
    """
    if sum(np.isfinite(left_flux)) <= poly_order or sum(np.isfinite(right_flux)) <= poly_order:
        return 0, np.infty

    poly_left = np.polynomial.Polynomial.fit(
        left_time[np.isfinite(left_flux)],
        left_flux[np.isfinite(left_flux)],
        poly_order,
    )
    poly_right = np.polynomial.Polynomial.fit(
        right_time[np.isfinite(right_flux)],
        right_flux[np.isfinite(right_flux)],
        poly_order,
    )
    middle_time = (left_time[-1] + right_time[0]) / 2

    separate_data = np.concatenate((left_bic_flux, right_bic_flux))
    separate_model = np.concatenate((poly_left(left_bic_time), poly_right(right_bic_time)))

    diff = poly_right(middle_time) - poly_left(middle_time)
    separate_bic = calculate_bic(separate_data, separate_model, 2 + 2 * poly_order)

    if debug:
        poly_time_left = np.linspace(left_bic_time[0], middle_time, 250)
        poly_time_right = np.linspace(middle_time, right_bic_time[-1], 250)
        ax.plot(
            poly_time_left,
            poly_left(poly_time_left),
            label=f"left poly {poly_order}",
            ls="--",
            c="darkorange",
            lw=1.5,
        )
        ax.plot(
            poly_time_right,
            poly_right(poly_time_right),
            label=f"right poly {poly_order}",
            ls="--",
            c="limegreen",
            lw=1.5,
        )

    return diff, separate_bic


def tips_poly_bic(
    poly_order,
    time,
    flux,
    bic_time,
    bic_flux,
    debug,
    ax,
):
    """
    Fits a polynomial model to the given continuous data and computes the BIC for model evaluation.

    Parameters
    ----------
    poly_order : array-like
        The order of the polynomial to use as model.
    time : array-like
        The time values corresponding to the flux measurement.
    flux : array-like
        The flux values corresponding to `time` to be modeled with a polynomial trend.
    bic_time : array-like
        The time values at which the BIC is evaluated.
    bic_flux : array-like
        The flux values corresponding to `bic_time` for BIC calculation.
    debug : bool
        If ``True``, the function plots the fitted polynomial model and BIC calculation information
        on the provided axis.
    ax : matplotlib.axes.Axes
        The Matplotlib axis on which to plot the fitted polynomial model if `debug` is ``True``.
        If `debug` is ``False``, can be ``None``.

    Returns
    -------
    unified_bic : float
        The Bayesian Information Criterion (BIC) value for the polynomial model fitted on
        the continuous `time` and `flux` values, and evaluated at `bic_time` and `bic_flux`.

        If there are not enough valid data points to fit the polynomial, returns ``np.inf``.

    Notes
    -----
    - The polynomial model used is:

      .. math::
          P(t) = c_0 + c_1 t + c_2 t^2 + ... + c_n t^n

      Where :math:`n` is the polynomial order and :math:`c_i` are the fitted coefficients.

    - This polynomial is fitted to the data using `numpy.polynomial.Polynomial.fit`.

    Examples
    --------
    .. plot::
        :include-source:

        import numpy as np
        from lobster.plotting import plot
        from lobster.stitching import tips_poly_bic

        time = np.linspace(0, .4, 1000)
        flux = 2.0 * np.sin(2 * np.pi * time) + np.random.normal(0, 0.1, len(time))
        bic_time = time[200:800]
        bic_flux = flux[200:800]

        ax = plot(time, flux, label="flux", xlabel="time", ylabel="flux")
        bic = tips_poly_bic(2, time, flux, bic_time, bic_flux, debug=True, ax=ax)
        ax.legend()
        ax.set_title(f"Found bic = {bic:.2f}")
        ax.get_figure().tight_layout()
    """
    if sum(np.isfinite(flux)) <= poly_order:
        return np.infty

    poly_unified = np.polynomial.Polynomial.fit(
        time[np.isfinite(flux)],
        flux[np.isfinite(flux)],
        poly_order,
    )

    unified_model_points = poly_unified(bic_time)
    unified_bic = calculate_bic(bic_flux, unified_model_points, 1 + poly_order)

    if debug:
        poly_time = np.linspace(bic_time[0], bic_time[-1], 500)
        ax.plot(
            poly_time,
            poly_unified(poly_time),
            label=f"unified poly {poly_order}",
            ls=":",
            c="deeppink",
            lw=1.5,
        )

    return unified_bic


def tips_sin_bic(
    time,
    flux,
    bic_time,
    bic_flux,
    debug,
    ax,
):
    """
    Fits a sinusoidal model to the given continuous data and computes the BIC for model evaluation.

    Parameters
    ----------
    time : array-like
        The time values corresponding to the flux measurement.
    flux : array-like
        The flux values corresponding to `time` to be modeled with a sinusoidal function.
    bic_time : array-like
        The time values at which the BIC is evaluated.
    bic_flux : array-like
        The flux values corresponding to `bic_time` for BIC calculation.
    debug : bool
        If ``True``, the function plots the fitted sinusoidal model and BIC calculation information
        on the provided axis.
    ax : matplotlib.axes.Axes
        The Matplotlib axis on which to plot the fitted sinusoidal model if `debug` is ``True``.
        If `debug` is ``False``, can be ``None``.

    Returns
    -------
    unified_bic : float
        The Bayesian Information Criterion (BIC) value for the sinusoidal model fitted on
        the continuous `time` and `flux` values, and evaluated at `bic_time` and `bic_flux`.

        If the fitting fails or there are not enough valid data points, returns ``np.inf``.

    Notes
    -----
    - The sinusoidal model used is:

      .. math::
          f(t) = a \\sin(\\omega t + \\phi) + c

      Where:

      - :math:`a` is the amplitude,
      - :math:`c` is the mean offset,
      - :math:`\\omega` is the angular frequency,
      - :math:`\\phi` is the phase shift.
    - The function first estimates an initial guess for the sinusoidal parameters using
      the Fast Fourier Transform (FFT) of the flux.
    - It then optimizes the sinusoidal fit using `scipy.optimize.curve_fit`.

    Examples
    --------
    .. plot::
        :include-source:

        import numpy as np
        from lobster.plotting import plot
        from lobster.stitching import tips_sin_bic

        time = np.linspace(0, 10, 1000)
        flux = 2.0 * np.sin(2 * np.pi * time) + np.random.normal(0, 0.1, len(time))
        bic_time = time[200:800]
        bic_flux = flux[200:800]

        ax = plot(time, flux, label="flux", xlabel="time", ylabel="flux")
        bic = tips_sin_bic(time, flux, bic_time, bic_flux, debug=True, ax=ax)
        ax.legend()
        ax.set_title(f"Found bic = {bic:.2f}")
        ax.get_figure().tight_layout()
    """
    if sum(np.isfinite(flux)) <= 10:
        return np.inf

    freq = np.fft.fftfreq(len(flux), (time[1] - time[0]))
    fft = abs(np.fft.fft(flux))

    guess_a = np.std(flux)
    guess_c = np.mean(flux)
    guess_w = abs(freq[np.argmax(fft[1:]) + 1]) * 2 * np.pi
    guess_p = 0

    guess = np.array([guess_a, guess_c, guess_w, guess_p])
    bounds = ([-np.inf, -np.inf, -np.inf, -np.pi], [np.inf, np.inf, np.inf, np.pi])

    def sinfunc(t, a, c, w, p):
        return a * np.sin(w * t + p) + c

    try:
        # pylint: disable=unbalanced-tuple-unpacking
        popt, _ = scipy.optimize.curve_fit(sinfunc, time, flux, p0=guess, bounds=bounds)
        opt_a, opt_c, opt_w, opt_p = popt
    except RuntimeError:
        return np.inf

    def fitted_sinfunc(t):
        return sinfunc(t, opt_a, opt_c, opt_w, opt_p)

    unified_model_points = fitted_sinfunc(bic_time)
    unified_bic = calculate_bic(bic_flux, unified_model_points, 4)

    if debug:
        model_time = np.linspace(bic_time[0], bic_time[-1], 500)
        ax.plot(
            model_time,
            fitted_sinfunc(model_time),
            label="unified sin",
            ls=":",
            c="deeppink",
            lw=1.5,
        )

    return unified_bic


def optimal_estimator(
    left_time,
    right_time,
    left_flux,
    right_flux,
    left_bic_time,
    right_bic_time,
    left_bic_flux,
    right_bic_flux,
    length_scale,
    sigma,
    nu,
    lambda_reg,
    debug,
    ax,
    estimator=lambda x, y: None,
):
    """
    Not ready yet.
    """
    optimal_estimator_left = optimal_linear_estimation_irregular(
        left_time[np.isfinite(left_flux)][::8],
        left_flux[np.isfinite(left_flux)][::8],
        length_scale,
        sigma,
        nu,
        lambda_reg,
    )
    optimal_estimator_right = optimal_linear_estimation_irregular(
        right_time[np.isfinite(right_flux)][::8],
        right_flux[np.isfinite(right_flux)][::8],
        length_scale,
        sigma,
        nu,
        lambda_reg,
    )
    middle_time = (left_time[-1] + right_time[0]) / 2

    if debug:
        ax.scatter(
            left_time,
            left_flux,
            label="tip optimal estimator left",
            s=1.5,
            c="green",
        )
        ax.scatter(
            right_time,
            right_flux,
            label="tip optimal estimator right",
            s=1.5,
            c="firebrick",
        )
        estimator_time_left = np.linspace(left_bic_time[0], middle_time, 100)
        estimator_time_right = np.linspace(middle_time, right_bic_time[-1], 100)
        ax.plot(
            estimator_time_left,
            optimal_estimator_left(estimator_time_left),
            label="optimal estimator left",
            ls="--",
            c="darkorange",
            lw=1.5,
        )
        ax.plot(
            estimator_time_right,
            optimal_estimator_right(estimator_time_right),
            label="optimal estimator right",
            ls="--",
            c="limegreen",
            lw=1.5,
        )
        ax.axvline(
            middle_time,
            label="optimal estimator interception",
            ls="--",
            c="black",
            lw=0.7,
            alpha=0.5,
        )

    data = np.concatenate((left_bic_flux, right_bic_flux))
    model = np.concatenate(
        (optimal_estimator_left(left_bic_time), optimal_estimator_right(right_bic_time))
    )
    return optimal_estimator_right([middle_time])[0] - optimal_estimator_left([middle_time])[
        0
    ], estimator(data, model)
