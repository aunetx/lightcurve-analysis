"""
The module where all the filtering functions and related utilities are defined.

They are meant to be used by the `Lightcurve` class, but can
be used by themselves in some cases.
"""

import numpy as np
import matplotlib.pyplot as plt
from astropy.convolution import convolve, convolve_fft

from lobster.utils import is_all_invalid
from lobster.maths import optimal_linear_estimation_irregular


def polynomial_filter(time, flux, order):
    """
    Applies a polynomial filtering to the data.

    Parameters
    ----------
    time : array-like
        The time values corresponding to the flux measurements.
    flux : array-like
        The flux measurements, which may contain invalid values.
    order : int
        The order of the polynomial used for filtering.

    Returns
    -------
    array-like
        The filtered flux values, with NaNs preserved where the input flux was invalid.

    Examples
    --------
    >>> import numpy as np
    >>> time = np.array([1, 2, 3, 4, 5])
    >>> flux = np.array([-1., np.nan, 3., 5., 7.])
    >>> polynomial_filter(time, flux, order=2)
    array([-1.,  1.,  3.,  5.,  7.])
    """
    if is_all_invalid(flux):
        return flux

    valid_time = time[np.isfinite(flux)]
    valid_flux = flux[np.isfinite(flux)]

    if len(valid_flux) < 2:
        return np.full_like(valid_flux, np.nanmean(valid_flux))

    poly = np.polynomial.Polynomial.fit(valid_time, valid_flux, order)

    return poly(time)


def sin4_filter(data, window_size, *args, **kwargs):
    """
    Applies a convolutive :math:`sin^4` filter to the input data. The padding and convolution
    settings can be customized with the same arguments as `filter_with_window`.

    Parameters
    ----------
    data : array-like
        The input data array to filter.
    window_size : int
        The size of the filter window; must be an odd integer.
    *args : tuple
        Additional positional arguments passed to `filter_with_window`.
    **kwargs : dict
        Additional keyword arguments passed to `filter_with_window`.

    Returns
    -------
    array-like
        The filtered data array, of the same size as the input.

    Notes
    ------
    A :math:`sin^4` filter is defined by the window function:

    .. math::
        w[n] = w[n] = \\left[ \\sin \\left( \\frac{n \\pi}{N - 1} \\right) \\right]^4,
        \\quad \\text{for } 0 \\leq n < N

    where :math:`N` is the window size.

    It is closer to a Gaussian convolutive window, and has better properties than both
    the boxcar and the triangular windows. Check `notebooks/cuttings_periods.ipynb`
    for more information.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([1., 2., 9., 4., 5., np.nan, 7., 8., 9.])
    >>> sin4_filter(data, window_size=5, padding_modes=("mirror", "mirror"))
    array([1.333..., 3. , 7. , 5. , 4.8 , nan, 7.2 , 8. , 8.666...])
    >>> sin4_filter(data, window_size=5)
    array([1.166..., 3. , 7. , 5. , 4.8 , nan, 7.2 , 8. , 9.166...])
    """
    if window_size > 1:
        lin = np.linspace(0, np.pi, window_size)
        window = np.sin(lin) ** 4
    else:
        window = np.ones(window_size)
    return filter_with_window(data, window / window.sum(), *args, **kwargs)


def triangular_filter(data, window_size, *args, **kwargs):
    """
    Applies a convolutive triangular filter to the input data. The padding and convolution settings
    can be customized with the same arguments as `filter_with_window`.

    Parameters
    ----------
    data : array-like
        The input data array to filter.
    window_size : int
        The size of the filter window; must be an odd integer.
    *args : tuple
        Additional positional arguments passed to `filter_with_window`.
    **kwargs : dict
        Additional keyword arguments passed to `filter_with_window`.

    Returns
    -------
    array-like
        The filtered data array, of the same size as the input.

    Notes
    ------
    A triangular filter is defined by the window function:

    .. math::
        w[n] = 1 - \\left| \\frac{2n}{N-1} - 1 \\right|, \\quad \\text{for } 0 \\leq n < N

    where :math:`N` is the window size.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([1., 2., 9., 4., 5., np.nan, 7., 8., 9.])
    >>> triangular_filter(data, window_size=3, padding_modes=("mirror", "mirror"))
    array([1.5 , 3.5 , 6. , 5.5 , 4.666...,  nan, 7.333..., 8. , 8.5 ])
    >>> triangular_filter(data, window_size=3)
    array([1.25..., 3.5 , 6. , 5.5 , 4.666..., nan, 7.333..., 8. , 9.25 ])
    """
    window = np.concatenate(
        (
            np.arange(1, window_size // 2 + 1),
            [window_size // 2 + 1],
            np.arange(window_size // 2, 0, -1),
        )
    )
    return filter_with_window(data, window / window.sum(), *args, **kwargs)


def boxcar_filter(data, window_size, *args, **kwargs):
    """
    Applies a convolutive boxcar filter to the input data. The padding and convolution settings
    can be customized with the same arguments as `filter_with_window`.

    Parameters
    ----------
    data : array-like
        The input data array to filter.
    window_size : int
        The size of the filter window; must be an odd integer.
    *args : tuple
        Additional positional arguments passed to `filter_with_window`.
    **kwargs : dict
        Additional keyword arguments passed to `filter_with_window`.

    Returns
    -------
    array-like
        The filtered data array, of the same size as the input.

    Notes
    ------
    A boxcar (moving average) filter is defined by the window function:

    .. math::
        w[n] = \\frac{1}{N}, \\quad \\text{for } 0 \\leq n < N

    where :math:`N` is the window size.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([1., 2., 9., 4., 5., np.nan, 7., 8., 9.])
    >>> boxcar_filter(data, window_size=3, padding_modes=("mirror", "mirror"))
    array([1.666..., 4. , 5. , 6. , 4.5 ,  nan, 7.5 , 8. , 8.333...])
    >>> boxcar_filter(data, window_size=3)
    array([1.333..., 4. , 5. , 6. , 4.5 ,  nan, 7.5 , 8. , 9.333...])
    """
    window = np.ones(window_size)
    return filter_with_window(data, window / window.sum(), *args, **kwargs)


def pad_poly(data, n_points_to_pad, order):
    """
    Pads the data using a polynomial fit.
    Assumes the data is regularly spaced, but may contain invalid (NaN) values.

    Parameters
    ----------
    data : array-like
        The input data to be padded.
    n_points_to_pad : tuple of int
        The number of points to pad on the left and right sides, respectively.
    order : int
        The order of the polynomial used for fitting and padding.

    Returns
    -------
    array-like
        The padded data array.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([4., 5., np.nan, 7., 8., 9.])
    >>> pad_poly(data, (3, 2), order=2)
    array([ 1.,  2.,  3.,  4.,  5., nan,  7.,  8.,  9., 10., 11.])

    >>> data = np.linspace(5, 10, 21)
    >>> data[4:7] = np.nan
    >>> padded_data = pad_poly(data, (30, 5), 2)
    >>> assert len(padded_data) == len(data) + 30 + 5
    >>> np.testing.assert_array_equal(padded_data[30:51], data)
    >>> np.round(padded_data, 4)
    array([-2.5 , -2.25, -2.  , -1.75, -1.5 , ..., 11.  , 11.25])
    """
    time = np.arange(0, len(data), 1)
    valid_time = time[np.isfinite(data)]
    valid_flux = data[np.isfinite(data)]

    if len(valid_flux) < 2:
        mean = np.nanmean(valid_flux)
        return np.full(len(data) + sum(n_points_to_pad), mean)

    poly = np.polynomial.Polynomial.fit(valid_time, valid_flux, order)

    return np.concatenate(
        [
            poly(np.arange(-n_points_to_pad[0], 0, 1)),
            data,
            poly(np.arange(len(data), len(data) + n_points_to_pad[1], 1)),
        ]
    )


def pad_optimal_estimator(data, n_points_to_pad):
    """
    Pads the data using optimal linear estimation computed data.

    Parameters
    ----------
    data : array-like
        The input data to be padded.
    n_points_to_pad : tuple of int
        The number of points to pad on the left and right sides, respectively.

    Returns
    -------
    array-like
        The padded data array.
    """
    time = np.arange(0, len(data), 1)
    valid_time = time[np.isfinite(data)][::16]
    valid_flux = data[np.isfinite(data)][::16]

    optimal_estimator_func = optimal_linear_estimation_irregular(
        valid_time,
        valid_flux,
        # length_scale,
        # sigma,
        # nu,
        # lambda_reg,
    )

    print("one optimal estimator done")
    plt.figure()
    plt.scatter(valid_time, valid_flux, c="black", s=2)
    plt.scatter(
        np.arange(-n_points_to_pad[0], 0, 1),
        optimal_estimator_func(np.arange(-n_points_to_pad[0], 0, 1)),
        c="green",
        s=2,
    )
    plt.scatter(
        np.arange(len(data), len(data) + n_points_to_pad[1], 1),
        optimal_estimator_func(np.arange(len(data), len(data) + n_points_to_pad[1], 1)),
        c="red",
        s=2,
    )
    plt.show()

    return np.concatenate(
        [
            optimal_estimator_func(np.arange(-n_points_to_pad[0], 0, 1)),
            data,
            optimal_estimator_func(np.arange(len(data), len(data) + n_points_to_pad[1], 1)),
        ]
    )


def pad_mirror_both(data, n_points_to_pad, n_points_median=9):
    """
    Pads an array on both sides by mirroring its values on the x-axis around the first
    and last valid time bins, and on the y-axis around the local median of valid values.

    Parameters
    ----------
    data : array-like
        The input data to be padded.
    n_points_to_pad : tuple of int
        The number of points to pad on the left and right, respectively.
    n_points_median : int, optional
        The number of points used to compute the median for mirroring, by default 30.

    Returns
    -------
    array-like
        The padded data array with mirrored values.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([1., 2., 3., 4., 5.])
    >>> padded_data = pad_mirror_both(data, (2, 3), 1)
    >>> padded_data
    array([-1.,  0.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.])
    >>> np.testing.assert_array_equal(
    ...     padded_data, np.pad(data, (2, 3), 'reflect', reflect_type='odd')
    ... )

    >>> data = np.linspace(5, 10, 21)
    >>> data[4:7] = np.nan
    >>> padded_data = pad_mirror_both(data, (30, 5), 3)
    >>> assert len(padded_data) == len(data) + 30 + 5
    >>> np.testing.assert_array_equal(padded_data[30:51], data)
    >>> padded_data
    array([  nan, -2.25, -2.  , -1.75, -1.5 , ..., 11.  , 11.25])
    """
    n_points_orig = len(data)

    # make sure we always pad at least 1 point
    if n_points_median >= len(data):
        n_points_median = len(data) - 1

    median_left = (
        np.nan if is_all_invalid(data[:n_points_median]) else np.nanmedian(data[:n_points_median])
    )
    median_right = (
        np.nan if is_all_invalid(data[-n_points_median:]) else np.nanmedian(data[-n_points_median:])
    )

    n_points_padded_left = min(n_points_to_pad[0], len(data) - n_points_median)
    n_points_padded_right = min(n_points_to_pad[1], len(data) - n_points_median)

    if n_points_padded_left > 0:
        data_left = (
            2 * median_left - data[::-1][-n_points_padded_left - n_points_median : -n_points_median]
        )
        assert len(data_left) == n_points_padded_left
        data = np.concatenate((data_left, data))
    if n_points_padded_right > 0:
        data_right = (
            2 * median_right - data[::-1][n_points_median : n_points_padded_right + n_points_median]
        )
        assert len(data_right) == n_points_padded_right
        data = np.concatenate((data, data_right))

    if len(data) < n_points_orig + sum(n_points_to_pad):
        return pad_mirror_both(
            data,
            (
                n_points_to_pad[0] - n_points_padded_left,
                n_points_to_pad[1] - n_points_padded_right,
            ),
            n_points_median,
        )
    else:
        return data


def filter_with_window(
    data,
    window,
    padding_modes=("mirror_both", "mirror_both"),
    convolve_mode="fft",
    preserve_nan=True,
):
    """
    Applies a filter with a given convolution window to the input data. Padding is applied
    on the left and right of the data before filtering, according to `padding_modes`.

    Parameters
    ----------
    data : array-like
        The input data array to be filtered.
    window : array-like
        A filter window of odd size to be convolved with the data.
    padding_modes : tuple of str, optional
        A tuple of two strings specifying padding modes for left and right sides respectively.

        Each mode can be:

        - ``"mirror"`` : Mirror the data along the vertical axis.
        - ``"mirror_both"`` : Mirror the data along both vertical and horizontal axes.
          See `pad_mirror_both` for more details.
        - ``"zero"`` : Pad with zeros.
        - ``"poly-n"``, with ``n`` an integer : Pad using a polynomial of order :math:`n`.

        Default is ``("mirror_both", "mirror_both")``.
    convolve_mode : {"direct", "fft"}, optional
        The convolution mode, either ``"direct"`` for standard convolution or ``"fft"`` for
        fast Fourier transform-based convolution.
        Default is ``"fft"``.
    preserve_nan : bool, optional
        If ``True``, preserves NaN values during convolution.
        Default is ``True``.

    Returns
    -------
    array-like
        The filtered data array, of the same size as the input.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([1., 2., 9., 4., 5., np.nan, 7., 8., 9.])
    >>> window = np.array([1/3, 1/3, 1/3])
    >>> filter_with_window(data, window=window, padding_modes=("mirror", "mirror"))
    array([1.666..., 4. , 5. , 6. , 4.5 ,  nan, 7.5 , 8. , 8.333...])
    >>> filter_with_window(data, window=window, padding_modes=("mirror_both", "mirror_both"))
    array([1.333..., 4. , 5. , 6. , 4.5 ,  nan, 7.5 , 8. , 9.333...])
    """
    window_size = len(window)
    assert window_size % 2 == 1
    window /= np.sum(window)
    if is_all_invalid(data):
        return data

    # first an last nonnan values
    nonnan_indexes = np.nonzero(np.isfinite(data))[0]
    first_nonnan = nonnan_indexes[0]
    last_nonnan = nonnan_indexes[-1]

    # data that is valid at least on its tips (to do the padding...)
    valid_data = data[first_nonnan : last_nonnan + 1]

    # TODO be more efficient by doing only one padding calculation
    padded_data = np.copy(valid_data)
    for padding_mode, pad in zip(padding_modes, [(window_size // 2, 0), (0, window_size // 2)]):
        if padding_mode == "mirror":
            padded_data = np.pad(padded_data, pad, "reflect")
        elif padding_mode == "mirror_both":
            padded_data = pad_mirror_both(padded_data, pad)
        elif padding_mode.startswith("mirror_both-"):
            padded_data = pad_mirror_both(padded_data, pad, n_points_median=int(padding_mode[12:]))
        elif padding_mode == "zero":
            padded_data = np.pad(padded_data, pad, "constant", constant_values=(0, 0))
        elif padding_mode.startswith("poly-"):
            padded_data = pad_poly(padded_data, pad, int(padding_mode[5:]))
        elif padding_mode == "optimal_estimator":
            padded_data = pad_optimal_estimator(padded_data, pad)
        else:
            raise ValueError(f"incorrect padding mode: '{padding_mode}'")

    if convolve_mode == "direct":
        convolve_function = convolve
    elif convolve_mode == "fft":
        convolve_function = convolve_fft
    else:
        raise ValueError(f"could not find convolution mode '{convolve_mode}'")

    assert len(padded_data) == len(valid_data) + 2 * (window_size // 2)
    convolved = convolve_function(padded_data, window, preserve_nan=preserve_nan)

    if window_size == 1:
        return convolved
    else:
        return np.concatenate(
            (
                np.full(first_nonnan, np.nan),
                convolved[window_size // 2 : -window_size // 2 + 1],
                np.full(len(data) - last_nonnan - 1, np.nan),
            )
        )


def adaptive_filter(
    data,
    window_sizes,
    thresholds,
    filter_function=sin4_filter,
    padding_modes=("mirror", "mirror"),
    convolve_mode="fft",
    debug=False,
    time=None,
):
    """
    Applies an adaptive filter that switches between different window sizes based on the difference
    between a smaller and larger filter applied to the data. The filter adjusts when the difference
    falls outside of the standard deviation, indicating that the larger filter cannot adequately
    follow the data. Between the two threshold values, a weighted average of both filters is used.

    Parameters
    ----------
    data : array-like
        The input data array to filter.
    window_sizes : tuple of two int
        A tuple containing two odd integers: the size of the small filter window and the size of
        the large filter window.
    thresholds : tuple of two float
        A tuple containing two floats:

        - the minimum standard deviation of the difference to its average to start using the\
          small filter,
        - the maximum standard deviation of the difference to its average to stop using the\
          large filter.
    filter_function : function, optional
        A function that applies a filter. Default is ``sin4_filter``.
    padding_modes : tuple of str, optional
        A tuple of two strings specifying padding modes for left and right sides respectively.

        Each mode can be:

        - ``"mirror"`` : Mirror the data along the vertical axis.
        - ``"mirror_both"`` : Mirror the data along both vertical and horizontal axes.
          See `pad_mirror_both` for more details.
        - ``"zero"`` : Pad with zeros.
        - ``"poly-n"``, with ``n`` an integer : Pad using a polynomial of order :math:`n`.

        Default is ``("mirror_both", "mirror_both")``.
    convolve_mode : str, optional
        The convolution mode, either ``"direct"`` for standard convolution or ``"fft"`` for
        fast Fourier transform-based convolution.
        Default is ``"fft"``.
    debug : bool, optional
        If True, a plot with adaptive filter information is generated. Default is ``False``.
    time : array-like, optional
        A numpy array representing the timestamps of the data, used for visualizing the filter
        process during debugging.

    Returns
    -------
    filtered : array-like
        The filtered data array, of the same size as the input data.
    indicator_outlier : array-like
        A binary array indicating the points where no filter was applied (values being 0),
        and where the small filter was used (values closer to 1 as the filter was used more).

    Examples
    --------
    See related notebook `notebooks/testing_everything.ipynb` for examples of how to use.
    """
    little_filter = filter_function(data, window_sizes[0], padding_modes, convolve_mode)
    big_filter = filter_function(data, window_sizes[1], padding_modes, convolve_mode)
    diff = little_filter - big_filter

    mean = np.nanmean(diff)
    sigma = np.nanstd(diff)
    centered_diff = abs(diff - mean) / sigma
    indicator_outlier = (
        np.minimum(thresholds[1], np.maximum(thresholds[0], centered_diff)) - thresholds[0]
    ) / (thresholds[1] - thresholds[0])

    filtered = little_filter * indicator_outlier + 1 - indicator_outlier

    if debug:
        percentage_little_filter = sum(indicator_outlier > 0) / len(indicator_outlier) * 100
        if time is None:
            time = np.arange(len(filtered))

        fig = plt.figure(figsize=(10, 6))
        ax = fig.add_subplot()
        ax.set_title(
            f"Debug graph for adaptative filter, {percentage_little_filter:.2f}% little filter used"
        )
        ax.set_xlabel(r"Time (d)")
        ax.set_ylabel(r"Flux")
        ax.plot(time, data, label="original flux")
        ax.plot(time, little_filter, alpha=0.4, label="little filter")
        ax.plot(time, big_filter, alpha=0.4, label="big filter")
        ax.scatter(
            time,
            filtered,
            s=0.6,
            alpha=0.6,
            c=indicator_outlier,
            cmap="viridis",
            label="adaptative filter",
            zorder=3,
        )
        ax.fill_between(
            time,
            0,
            1,
            where=indicator_outlier > 0.0,
            alpha=0.3,
            label="little filter used",
            transform=ax.get_xaxis_transform(),
        )
        ax.legend()
        ax.grid()

    return filtered, indicator_outlier
