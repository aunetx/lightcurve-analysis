"""
Various utilities that are reused in various modules, or that depend on external
data for `find_tess_down_segments`.
"""

import numpy as np
from astropy.time import Time


def version_is_higher_or_equal(version, comparison_version):
    """
    Checks if a given version string is higher or equal than another one.

    Parameters
    ----------
    version : str
        The version that is being compared.
    comparison_version : str
        The version to compare it to.

    Returns
    -------
    bool :
        ``True`` if and only if `version` is more recent than `comparison_version`.
    """
    a_major, a_minor, a_release = version.split(".")
    b_major, b_minor, b_release = comparison_version.split(".")

    if int(a_major) > int(b_major):
        return True
    if int(a_minor) > int(b_minor):
        return True
    if int(a_release) >= int(b_release):
        return True
    return False


def searchsorted_with_nan_handling(arr, values):
    """
    Perform a binary search to find the insertion indices of values into a sorted array,
    while correctly handling NaN values.

    Parameters
    ----------
    arr : array-like
        A sorted array (ascending order) that may contain NaN values.
    values : scalar or array-like
        The value(s) to search for insertion positions.

    Returns
    -------
    int or ndarray
        The insertion index/indices where values should be inserted to maintain order.

    Examples
    --------
    >>> import numpy as np
    >>> arr = np.array([1, 2, np.nan, 4, 5])
    >>> int(searchsorted_with_nan_handling(arr, 3))
    3
    >>> searchsorted_with_nan_handling(arr, np.array([0, 3, 6]))
    array([0, 3, 5])
    """
    # Identify positions of NaN values
    nan_mask = np.isnan(arr)

    # Create an array of indices where values are not NaN
    valid_indices = np.where(~nan_mask)[0]

    # Create a view of the array with only valid (non-NaN) values
    valid_values = arr[valid_indices]

    # If only NaNs, we treat if like np.searchsorted([], 4) == 0
    if len(valid_values) == 0:
        return np.zeros_like(values)

    # Use searchsorted on the valid values
    insertion_points = np.searchsorted(valid_values, values)

    # If is last indice, we want to treat the values separately
    is_last_indice = insertion_points == len(valid_values)

    # Clip the insertion points to the maximum valid index
    insertion_points = np.clip(insertion_points, 0, len(valid_indices) - 1)

    # Adjust the insertion points to account for the ignored NaN values
    adjusted_insertion_points = np.where(is_last_indice, len(arr), valid_indices[insertion_points])

    return adjusted_insertion_points


def find_tess_down_segments():
    """
    Identifies and categorizes the down segments in the TESS orbit times.

    The function processes TESS orbit data and extracts three types of down segments:

    1. **Inter-sector down segments**: Time intervals between the end of an orbit in one sector\
       and the start of an orbit in the next sector.
    2. **Middle-sector down segments**: Time intervals between the end of an orbit and the\
       start of the next orbit within the same sector.
    3. **Intra-sector down segments**: Time intervals for the gap at the middle of each half-sector\
       when there is one, i.e for last sectors.

    This function processes the orbit times provided by the
    `ipad.downloads.tess.load_tess_orbit_times()` method and organizes the segments accordingly.

    Returns
    -------
    inter_sector_down_segments : list
        List of down segments between sectors.
    middle_sector_down_segments : list
        List of down segments in the middle of a sector.
    intra_sector_down_segments : list
        List of down segments at the end of the first and last quarter of a sector.

    Notes
    -----
    - The function imports and uses the `ipad` library to download and load the TESS orbit data.
    - The end and start times are processed as ISO formatted times using the `Time` class.
    """
    # importing ipad only here so it is not a library-wide requirement
    import ipad  # pylint: disable=import-outside-toplevel

    # TODO ne pas trouver les 4 jumps à la fin
    df = ipad.downloads.tess.load_tess_orbit_times()

    inter_sector_down_segments = []
    middle_sector_down_segments = []
    intra_sector_down_segments = []

    for sector in range(1, df.Sector.max()):
        sector_df = df[df.Sector == sector]

        if sector > 1:
            prev_sector_df = df[df.Sector == sector - 1]
            down_start = Time(prev_sector_df["End of Orbit"].values[-1], format="iso")
            down_end = Time(sector_df["Start of Orbit"].values[0], format="iso")
            inter_sector_down_segments.append([sector, down_start, down_end])

        if len(sector_df) == 2:
            down_start = Time(sector_df["End of Orbit"].values[0], format="iso")
            down_end = Time(sector_df["Start of Orbit"].values[1], format="iso")
            middle_sector_down_segments.append([sector, down_start, down_end])

        elif len(sector_df) == 4:
            down_start = Time(sector_df["End of Orbit"].values[0], format="iso")
            down_end = Time(sector_df["Start of Orbit"].values[1], format="iso")
            intra_sector_down_segments.append([sector, down_start, down_end])

            down_start = Time(sector_df["End of Orbit"].values[1], format="iso")
            down_end = Time(sector_df["Start of Orbit"].values[2], format="iso")
            middle_sector_down_segments.append([sector, down_start, down_end])

            down_start = Time(sector_df["End of Orbit"].values[2], format="iso")
            down_end = Time(sector_df["Start of Orbit"].values[3], format="iso")
            intra_sector_down_segments.append([sector, down_start, down_end])

        else:
            raise ValueError("it seems there are more than 4 subsectors... Please contact Aurélien")

    return inter_sector_down_segments, middle_sector_down_segments, intra_sector_down_segments


def get_deviation(deviation_mode):
    """
    Returns a function that computes the specified deviation measure.

    Parameters
    ----------
    deviation_mode : str
        The type of deviation to compute. Options are:

        - ``"std"``: Standard deviation (`np.nanstd`).
        - ``"iqr"``: Interquartile range (IQR), computed as the difference between
          the 75th and 25th percentiles.

    Returns
    -------
    function
        A function that calculates the specified deviation measure on an array-like input.
    """
    if deviation_mode == "iqr":
        return lambda x: np.subtract(*np.percentile(x, [75, 25]))
    elif deviation_mode == "std":
        return np.nanstd
    else:
        raise ValueError(f"could not find appropriate deviation mode for '{deviation_mode}'")


def is_all_invalid(array):
    """
    Checks if all elements in the array are either NaN, infinite, or if the array is empty.

    Parameters
    ----------
    array : array-like
        Input array to check.

    Returns
    -------
    bool
        True if all elements are NaN or infinite, or if the array is empty. False otherwise.
    """
    return np.all(np.logical_not(np.isfinite(array)))


def sort_by_time(time, flux, quality, sector, jumps):
    """
    Sorts all the data used to create a lightcurve by time.

    Parameters
    ----------
    time : np.array
        The time array to sort by.
    flux, quality, sector : np.array
        The flux, quality and sector arrays to sort according to the `time` array.
    jumps : dict
        The jumps dictionnary, to update its keys accordingly.

    Returns
    -------
    time, flux, quality, sector : np.array
        The sorted time, flux, quality and sector arrays.
    jumps : dict
        The jumps dictionnary, with keys corresponding to the new time array.

    Examples
    --------
    >>> time = np.array([1, 3, 2, 4])
    >>> flux = np.array([6, 7, 8, 9])
    >>> quality, sector = flux.copy(), flux.copy()
    >>> jumps = { 1: "t3", 2: "t2", 3: "t4" } # wrong jumps format to show how they move
    >>> t, f, q, s, j = sort_by_time(time, flux, quality, sector, jumps)
    >>> t
    array([1, 2, 3, 4])
    >>> f
    array([6, 8, 7, 9])
    >>> np.testing.assert_array_equal(f, q)
    >>> np.testing.assert_array_equal(f, s)
    >>> assert j == { 1: "t2", 2: "t3", 3: "t4" }
    """
    if not np.all(time[:-1] < time[1:]):
        sorted_indices = np.argsort(time)

        time = time[sorted_indices]
        flux = flux[sorted_indices]
        quality = quality[sorted_indices]
        sector = sector[sorted_indices]

        new_jumps = {}
        new_indices = np.empty_like(sorted_indices)
        new_indices[sorted_indices] = np.arange(len(time))
        for jump_bin in jumps:
            new_jump_bin = new_indices[jump_bin]
            new_jumps[new_jump_bin] = jumps[jump_bin]
        jumps = dict(sorted(new_jumps.items()))

    assert np.all(time[:-1] < time[1:])

    return time, flux, quality, sector, jumps
