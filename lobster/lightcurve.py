"""
The main module of Lobster. It contains `Lightcurve`, which is the class used
to describe a given star's lightcurve, and then correct, modify, analyze and
plot it at will.
"""

import copy
import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms

import lobster.maths
from lobster.stitching import stitch
from lobster.fits_management import (
    open_k2_fits,
    open_qlp_fits,
    open_kbonus_fits,
    open_lobster_fits,
    write_to_fits,
)
from lobster.filtering import (
    sin4_filter,
    boxcar_filter,
    triangular_filter,
    adaptive_filter,
    polynomial_filter,
)
from lobster.utils import (
    get_deviation,
    is_all_invalid,
    find_tess_down_segments,
    searchsorted_with_nan_handling,
    sort_by_time,
)
from lobster.plotting import plot, scatter

#: Link between window size and cutting period for a 0.9 gain, for a boxcar diviser
SMOOTH_BOXCAR_FILTER_DIVISER = 4
#: Link between window size and cutting period for a 0.9 gain, for a triangular filter
SMOOTH_TRIANGULAR_FILTER_DIVISER = 3
#: Link between window size and cutting period for a 0.9 gain, for a sin4 filter
SMOOTH_SIN4_FILTER_DIVISER = 2
#: Link between window size and cutting period for a 1.0 gain, for a boxcar normalization filter
NORMALIZE_BOXCAR_FILTER_DIVISER = 1.00
#: Link between window size and cutting period for a 1.0 gain, for a triangular normalization filter
NORMALIZE_TRIANGULAR_FILTER_DIVISER = 0.532
#: Link between window size and cutting period for a 1.0 gain, for a sin4 normalization filter
NORMALIZE_SIN4_FILTER_DIVISER = 0.354
#: Link between window size and cutting period for a 1.0 gain, for a sin4 normalization filter done
#: 3 times
NORMALIZE_SIN4_3_TIMES_FILTER_DIVISER = 0.340


class Lightcurve:
    """
    A class for handling lightcurve data, including loading from FITS files,
    applying filters, and plotting.

    Parameters
    ----------
    path : str
        When initializing, you can give it a `path`, and Lobster will try to load
        data from it with one of the `Lightcurve.load_...` methods.

        It is still better to give no path and specifically call on of these methods
        directly, so you are sure data is loaded correctly.
    """

    # * -------------------- LIGHTCURVE CREATION -------------------- * #

    def __init__(self, path=None):
        """
        Initializes the lightcurve object. If a path is given, the time and flux is loaded from the
        associated FITS file.
        """

        self.object_name = ""
        """
        str : The name identifier for the lightcurve object. It is often loaded from the FITS file.
        """
        self.unit = "unit"
        """
        str : The unit used for the flux, ``"unit"`` when not specified. It is often loaded from
        the FITS file.
        """
        self.figsize = (10, 6)
        """
        tuple : The default figure size for plotting.
        """
        self.dpi = 100
        """
        int : The default dots per inch for plotting.
        """
        self.jumps = {}
        """
        dict : A dictionary of jumps in the lightcurve.

        A jump corresponds to a point where the lightcurve is considered not continuous anymore,
        whether it originated from a time gap, flux discontinuity, new sector... Methods exist to
        find such jumps automatically. They can then be used to stitch the flux together or to
        perform some operations in some sections separately.

        The structure of the jumps dictionnary is:

        .. code-block:: python

          lc.jumps = {
            # for a discontinuity occuring just before lc.time[2521]
            2521: {
              # how to pad the end of the left section and the beginning of the right section,
              # when the need arises
              'padding_modes': ['mirror_both', 'mirror_both'],
              # the different methods that can be used to stitch this particular jump
              'stitching_methods': [
                {'method': 'total-mean', 'min-jump': 10},
                {'method': 'tips-mean', 'max-width': 30, 'max-jump': 5},
                {'method': 'tips-poly-1', 'min-width': 5, 'max-width': 15, 'max-jump': 10},
                {'method': 'tips-poly-1', 'min-width': 20, 'max-width': 30, 'max-jump': 30}
              ]
            },
            # a jump can also be empty, default values will be added
            5947: {}
          }

        Most of the time, default ``{}`` values for jumps can be used, and the metadata of the jump
        can be changed through arguments of the different methods. Still, if a given jump has
        metadata (for example for the padding modes), it will override the ones given in argument.
        """
        self.time = np.array([])
        """
        array-like : The time array for the lightcurve.

        It needs to be updated through standard builtin methods in order to always keep it in sync
        with `_corresponding_time`.
        """
        self.flux = np.array([])
        """
        array-like : The flux array for the lightcurve.

        It can be updated as will, as long as its shape is the same as `time`.
        """
        self._original_time = np.array([])
        """
        array-like : The original time array, which never changes.

        When searching for the original time of a given point, you need to search in
        `_corresponding_time` the indice to where the point came from, and take the corresponding
        value in this array.
        """
        self._original_quality = np.array([])
        """
        array-like : The original quality array, which never changes.

        When searching for the quality of a given point, you need to search in
        `_corresponding_time` the indice to where the point came from, and take the corresponding
        value in this array.
        """
        self._original_sector = np.array([])
        """
        array-like : The original sector array, which never changes.

        When searching for the sector of a given point, you need to search in
        `_corresponding_time` the indice to where the point came from, and take the corresponding
        value in this array.
        """
        self._corresponding_time = np.array([])
        """
        array-like : The corresponding time in the new time array relative to its original version.

        When searching in this array a particular time bin that has been possibly affected and
        moved around by correction in this array, it will be found at the indice corresponding to
        where it originated when the lightcurve was created.

        It is updated whenever the `time` array is changed through standard buildin method to
        always keep them in sync.
        """
        self._operations = []
        """
        list : A list of operations performed on the lightcurve.
        """
        self._debug = False
        """
        bool : If True, debug graphs of the lightcurve and PSD will be saved in `self._operations`.
        To be changed with `self.debug`.
        """

        if path is not None:
            if "kbonus" in path:
                self.load_kbonus(path)
            elif "qlp" in path:
                self.load_qlp(path)
            elif "k2" in path:
                self.load_k2(path)
            else:
                raise ValueError(
                    f"could not find the correct lightcurve type for path {path}, "
                    + "please use the dedicated `load_...` method."
                )

    def copy(self):
        """
        Copy the lightcurve object, making a deepcopy of its arrays.

        Returns
        -------
        Lightcurve
            A deep copy of the current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve().init_from_data(np.array([1, 2, 3]), np.array([2, 3, 1]))
        >>> lc_copy = lc.copy()
        >>> lc_copy.remove_data(lc_copy.time > 2).flux
        array([2, 3])
        >>> lc.flux
        array([2, 3, 1])
        """
        return copy.deepcopy(self)

    def init_from_data(
        self, time, flux, quality=None, sector=None, jumps={}, object_name=None, unit=None
    ):  # pylint: disable=dangerous-default-value
        """
        Initializes a lightcurve given a time array, a flux array and optional
        quality array, sector array and jumps dictionary.

        Parameters
        ----------
        time : array-like
            The time array for the lightcurve.
        flux : array-like
            The flux array for the lightcurve.
        quality : array-like, optional
            The quality array for the lightcurve.
        sector : array-like, optional
            The sector array for the lightcurve.
        jumps : dict, optional
            A dictionary of jumps in the lightcurve.
        object_name : str
            The name of the object, default ``None`` to keep the same.
        unit : str, optional
            The unit used for the flux data, default ``None`` to keep the same.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        quality = (np.full_like(time, -1) if quality is None else quality).astype(int)
        sector = (np.full_like(time, -1) if sector is None else sector).astype(int)

        time, flux, quality, sector, jumps = sort_by_time(time, flux, quality, sector, jumps)

        self.time = np.copy(time)
        self.flux = np.copy(flux)
        self.jumps = copy.deepcopy(jumps)

        # the original time, it NEVER changes
        self._original_time = np.copy(time)
        # same length as original time, but with the corresponding time in new time array
        self._corresponding_time = np.copy(time)
        # same length as original time
        self._original_quality = np.copy(quality)
        self._original_sector = np.copy(sector)

        if object_name:
            self.object_name = object_name
        if unit:
            self.unit = unit

        return self

    def _init_from_quarters_data(
        self,
        times,
        fluxes,
        qualities,
        sectors,
        object_name=None,
        unit=None,
    ):
        """
        Generates a lightcurve from data for each sector, along with some metadata.

        Parameters
        ----------
        times : list of np.array
            A list of arrays containing the time values for each sector.
        fluxes : list of np.array
            A list of arrays containing the flux values for each sector.
        qualities : list of np.array
            A list of arrays containing the quality values for each sector.
        sectors : list of np.array
            A list of arrays containing the sector number for each sector.
        object_name : str
            The name of the object, default ``None`` to keep the same.
        unit : str, optional
            The unit used for the flux data, default ``None`` to keep the same.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Notes
        -----
        This method is meant to be used internally and should not be called
        directly by users.
        """
        time = np.concatenate(times)
        flux = np.concatenate(fluxes)
        quality = np.concatenate(qualities)
        sector = np.concatenate(sectors)

        jumps = {}
        for i in range(1, len(times)):
            jumps[sum(map(len, times[:i]))] = {}
        self.jumps = dict(sorted(jumps.items()))

        return self.init_from_data(time, flux, quality, sector, jumps, object_name, unit)

    def load_kbonus(self, fits_path):
        """
        Initializes a lightcurve from a single FITS file, reading every even HDU table beginning at
        the third one. It is the format KBONUS lightcurves use.

        Parameters
        ----------
        fits_path : str
            The path to the FITS file containing the lightcurve data.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_kbonus("tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.object_name
        'KIC 2998253'
        """
        self._init_from_quarters_data(*open_kbonus_fits(fits_path))

        return self._add_operation("loaded as KBONUS from FITS file")

    def load_qlp(self, base_path):
        """
        Initializes a lightcurve from a folder containing multiple FITS files, one for each quarter.
        It is the format QLP lightcurves use.

        Parameters
        ----------
        base_path : str
            The path to the folder containing the FITS files.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_qlp("tests/data/tess/000000001874/0000000018749338/qlp/")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.object_name
        'TIC 18749338'
        """
        self._init_from_quarters_data(*open_qlp_fits(base_path))

        return self._add_operation("loaded as QLP from FITS directory")

    def load_k2(self, fits_path):
        """
        Initializes a lightcurve from a single FITS file, reading the only quarter there is.
        It is the format K2 lightcurves use.

        Parameters
        ----------
        fits_path : str
            The path to the FITS file containing the lightcurve data.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_k2("tests/data/k2/212175472/hlsp_everest_k2_llc_212175472-c18_kepler_v2.0_lc.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.object_name
        'EPIC 212175472'
        """
        self._init_from_quarters_data(*open_k2_fits(fits_path))

        return self._add_operation("loaded as K2 from FITS file")

    def load_lobster_fits(self, fits_path):
        """
        Initializes a lightcurve from a single FITS file that was saved with
        `Lightcurve.write_fits_file`.

        Parameters
        ----------
        fits_path : str
            The path to the FITS file containing the lightcurve data.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_lobster_fits("tests/data/lobster/test_fits_read_1.0.5.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.object_name
        'test data 1.0.5'
        """
        self._init_from_quarters_data(*open_lobster_fits(fits_path))

        return self._add_operation("loaded as Lobster data from FITS file")

    def write_to_fits(self, filename):
        """
        Writes the lightcurve object as a FITS file. You can open it again with
        `load_lobster_fits`.

        Parameters
        ----------
        filename : str
            The name of the FITS file to write the lightcurve data to.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> time, flux = np.array([1., 2., 3.]), np.array([10., 11., 9.])
        >>> lc.init_from_data(time, flux, object_name="test data")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.write_to_fits("tests/data/lobster/test_fits_write.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        """
        write_to_fits(
            self.time,
            self.flux,
            self.quality,
            self.sector,
            self.get_operations_list(),
            self.unit,
            filename,
            self.object_name,
        )
        return self

    @property
    def quality(self):
        """
        Gets the quality array of the lightcurve.

        Returns
        -------
        numpy.ndarray
            The quality array of the lightcurve.
        """
        # TODO do as Rafa told, with quality things
        time_to_original_indexes = searchsorted_with_nan_handling(
            self._corresponding_time, self.time
        )
        return np.where(
            np.logical_and(
                time_to_original_indexes >= 0,
                time_to_original_indexes < len(self._original_quality),
            ),
            self._original_quality[
                np.clip(time_to_original_indexes, 0, len(self._original_quality) - 1)
            ],
            -1,
        )

    @property
    def sector(self):
        """
        Gets the sector array of the lightcurve.

        Returns
        -------
        numpy.ndarray
            The sector array of the lightcurve.
        """
        time_to_original_indexes = searchsorted_with_nan_handling(
            self._corresponding_time, self.time
        )
        return np.where(
            np.logical_and(
                time_to_original_indexes >= 0,
                time_to_original_indexes < len(self._original_sector),
            ),
            self._original_sector[
                np.clip(time_to_original_indexes, 0, len(self._original_sector) - 1)
            ],
            -1,
        )

    # * -------------------- LIGHTCURVE PROPERTIES -------------------- * #

    @property
    def dt_median(self):
        """
        Gets the median time difference between consecutive data points.

        Returns
        -------
        float
            The median time difference between consecutive data points.
        """
        return np.median(np.diff(self.time))

    @property
    def dt_mean(self):
        """
        Gets the mean time difference between consecutive data points.

        Returns
        -------
        float
            The mean time difference between consecutive data points.
        """
        return np.mean(np.diff(self.time))

    def _duration_to_odd_size(self, duration, dt="median"):
        """
        Converts a duration to the nearest odd number of data points. This is meant to be used as a
        conversion function to have filter window lenghts.

        Parameters
        ----------
        duration : float
            The duration in days.
        dt : str or float, optional
            The method to use for calculating the time difference between data points.

            This can be one of the following:

            - ``"mean"``: Use the mean time difference between consecutive data points.
            - ``"median"``: Use the median time difference between consecutive data points.
            - A number: Use a fixed time difference specified by this number.

            Default is ``"median"``.

        Returns
        -------
        int
            The nearest odd number of data points corresponding to the given duration.

        Notes
        -----
        This method is meant to be used internally and should not be called
        directly by users.
        """
        if dt == "median":
            dt = self.dt_median
        elif dt == "mean":
            dt = self.dt_mean

        if np.isfinite(duration / dt):
            return int(duration / dt) // 2 * 2 + 1
        else:
            return 1

    def _divide_flux_between_jumps(
        self,
        default_padding=("mirror_both", "mirror_both"),
        stitching_methods=[
            {"method": "total-mean"},
            {"method": "tips-mean"},
            {"method": "tips-poly-1"},
            {"method": "tips-poly-2"},
        ],
    ):  # pylint: disable=dangerous-default-value
        """
        Divides the flux data between jumps, useful for stitching and filtering.

        Parameters
        ----------
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.
        stitching_methods : list of dict, optional
            A list of dictionaries specifying the stitching methods to register.
            Default is a list of methods including ``"total-mean"``, ``"tips-mean"``,
            ``"tips-poly-1"``, and ``"tips-poly-2"``.

        Returns
        -------
        times : list
            A list of the time arrays for each section.
        fluxes : list
            A list of the flux arrays for each section.
        jumps : dict
            The jumps dictionnary of the lightcurve.
        padding_modes_list : list
            A list of padding mode instructions (left_mode, right_mode) for each section.

        Notes
        -----
        This method is meant to be used internally and should not be called
        directly by users.
        """
        if len(self.jumps) > 0:
            padding_modes_list = (
                [[default_padding[0], None]]
                + [[None, None]] * (len(self.jumps) - 1)
                + [[None, default_padding[1]]]
            )
        else:
            padding_modes_list = [default_padding]

        fluxes = []
        times = []
        jumps = copy.deepcopy(self.jumps)
        start_index = 0
        for i, jump_index in enumerate(jumps):
            times.append(self.time[start_index:jump_index])
            fluxes.append(self.flux[start_index:jump_index])
            start_index = jump_index

            jump = jumps[jump_index]

            # set default values
            jump["padding_modes"] = jump.get("padding_modes", list(reversed(default_padding)))
            jump["stitching_methods"] = jump.get(
                "stitching_methods", copy.deepcopy(stitching_methods)
            )

            padding_modes_list[i][1] = jump["padding_modes"][0]
            padding_modes_list[i + 1][0] = jump["padding_modes"][1]

        times.append(self.time[start_index:])
        fluxes.append(self.flux[start_index:])
        return times, fluxes, jumps, padding_modes_list

    # * -------------------- STITCHING -------------------- * #

    def stitch(
        self,
        stitching_methods=[
            {"method": "total-mean"},
            {"method": "tips-mean"},
            {"method": "tips-poly-1"},
            {"method": "tips-poly-2"},
        ],
        choose_with_bic=None,
        verbose=False,
        debug=False,
        show_on_ax=None,
    ):  # pylint: disable=dangerous-default-value
        """
        Stitches the fluxes together using the specified stitching methods, at the position
        given by the jumps. The stitching methods can be customized using the `stitching_methods`
        parameter. It correctly ignores invalids inside the flux.

        Parameters
        ----------
        stitching_methods : list of dict or str, optional
            A list of dictionaries specifying the stitching methods that will be compared to each
            other using the BIC (Bayesian Information Criterion).

            Each dictionary can include the following keys:

            - ``"method"``: str
                The name of the method (e.g., ``"total-mean"``, ``"tips-mean"``, ``"tips-poly-1"``,
                ``"tips-poly-2"``).
            - ``"min-jump"``: float
                The minimum number of days in the jump to use this method.
            - ``"max-width"``: float
                The maximum length of the tip.
            - ``"max-jump"``: float
                The maximum number of days in the jump to use this method.
            - ``"max-jump-multiplicator"``: float
                The maximum "jump length / tip length" ratio before ditching the method.
            - ``"constrain-equal-width"``: bool
                Thether or not to always use an equal duration of both tips.

            If a string is given instead of a list of dicts, it is equivalent to:
            ``[{"method": string}]``.

            Default is a list of methods including ``"total-mean"``, ``"tips-mean"``,
            ``"tips-poly-1"``, and ``"tips-poly-2"``.
        choose_with_bic : str or None, optional
            The model to use for comparing the other stitching methods. If None, the models will
            be compared by fitting themselves on the stitched fluxes. If one is given, this model
            will instead be fitted on the stitched fluxes.

            The choosing model can be either ``"total-mean"``, ``"tips-mean"``, ``"tips-poly-1"``,
            ``"tips-poly-2"`` or ``"tips-sin"`` (which can only be using for choosing,
            not stitching).

            The BIC of this model given the stitched data will then be computed, and the stitching
            model with the lowest BIC will be choosen. Default is ``None``.
        verbose : bool, optional
            Whether to enable verbose output. Default is ``False``.
        debug : bool, optional
            Whether to enable debug mode. Default is ``False``.
        show_on_ax : matplotlib.axes.Axes, optional
            The matplotlib axes to plot the stitching process on. Default is ``None``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_kbonus("tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.stitch(stitching_methods=[
        ...     {"method": "total-mean", "min-jump": 10},
        ...     {"method": "tips-mean", "max-width": 30, "max-jump": 5},
        ...     {"method": "tips-poly-1", "min-width": 5, "max-width": 15, "max-jump": 10},
        ...     {"method": "tips-poly-1", "min-width": 20, "max-width": 30, "max-jump": 30}
        ... ], choose_with_bic="tips-sin")
        <lobster.lightcurve.Lightcurve object at ...>
        """
        if isinstance(stitching_methods, str):
            stitching_methods = [{"method": stitching_methods}]
        else:
            stitching_methods = copy.deepcopy(stitching_methods)

        times, fluxes, jumps, _ = self._divide_flux_between_jumps(
            stitching_methods=stitching_methods
        )

        self.flux, chosen_methods = stitch(
            times,
            fluxes,
            jumps,
            choose_with_bic=choose_with_bic,
            debug=self.debug or debug,
            verbose=verbose,
        )

        if show_on_ax:
            for jump, method in zip(jumps, chosen_methods):
                show_on_ax.text(
                    self.time[jump],
                    0.05,
                    method,
                    fontsize=6,
                    transform=transforms.blended_transform_factory(
                        show_on_ax.transData, show_on_ax.transAxes
                    ),
                    rotation="vertical",
                    verticalalignment="bottom",
                )

        return self._add_operation("stitched fluxes")

    def remove_isolated_points(
        self, isolated_of_more_than_days=5 / 24, min_n_points=10, removal_mode="NaN"
    ):
        """
        Removes isolated points in the lightcurve data, i.e groups of points that are
        separated by more than a specified number of days from the rest of the data.

        Parameters
        ----------
        isolated_of_more_than_days : float, optional
            The minimum number of days that isolated points must be separated by
            to be removed. Default is ``5/24`` (5 hours).
        min_n_points : int, optional
            The minimum number of points required to consider a section as
            isolated. Default is ``10``.
        removal_mode : str, optional
            The mode for removing isolated points. Options are ``"NaN"`` to replace
            isolated points with ``NaN``, ``"zero"`` to replace with 0, and ``"delete"``
            to delete the points. Default is ``"NaN"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        lc_copy = self.copy()
        lc_copy.jumps = {}
        lc_copy.find_jumps_in_time(min_jump_days=isolated_of_more_than_days)
        time_blocks = lc_copy._divide_flux_between_jumps()[0]  # pylint: disable=protected-access
        to_remove = []
        for time_block in time_blocks:
            if len(time_block[np.isfinite(time_block)]) <= min_n_points:
                to_remove += [True] * len(time_block)
            else:
                to_remove += [False] * len(time_block)
        self.remove_data(to_remove, removal_mode)
        return self

    # * -------------------- FILTERING -------------------- * #

    def _apply_filter(self, filter_func, window_duration, section_mode, default_padding):
        """
        Applies a filter to the lightcurve data, using a given window duration and padding mode.
        The filtering can be applied to the entire lightcurve or to individual sections.

        Parameters
        ----------
        filter_func : callable
            The filter function to apply to the lightcurve data. It needs to take as argument
            the flux (np.ndarray), window size (float) and the padding mode to use to each side
            of the flux (2-tuple of str); and return a modified flux.
        window_duration : float
            The duration of the filter window in days.
        section_mode : str
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections.
        default_padding : tuple
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Notes
        -----
        This method is meant to be used internally and should not be called
        directly by users.
        """
        window_size = self._duration_to_odd_size(window_duration)

        if section_mode == "all":
            self.flux = filter_func(self.flux, window_size, default_padding)

        elif section_mode == "section":
            _, fluxes, _, padding_modes_list = self._divide_flux_between_jumps(
                default_padding=default_padding
            )
            modified_lightcurves = []
            for l, padding_modes in zip(fluxes, padding_modes_list):
                if not is_all_invalid(l):
                    modified_lightcurves.append(filter_func(l, window_size, padding_modes))
                else:
                    modified_lightcurves.append(l)
            self.flux = np.concatenate(modified_lightcurves)

        else:
            raise ValueError(f"unknown section mode '{section_mode}'")

        return self._add_operation(
            f"applied filter {filter_func.__name__}, window duration of {window_duration:.2f} d, "
            + f"window size of {window_size}"
            + " section-by-section" * (section_mode == "section")
        )

    def smooth_boxcar_filter(
        self,
        cut_below_period=1,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies a boxcar smoothing filter to the lightcurve data, using a specified cutoff
        period and padding modes. The filtering can be applied to the entire lightcurve or
        to individual sections.

        Parameters
        ----------
        cut_below_period : float, optional
            The cutoff period below which the filter is applied, in days.

            This is currently a cutoff period at 90%, which means the periods under this cutoff
            should be attenuated of more than 90%.
            See the cutting_periods.ipynb notebook and SMOOTH_BOXCAR_FILTER_DIVISER for
            informations.

            Default is ``1``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        return self._apply_filter(
            boxcar_filter,
            cut_below_period / SMOOTH_BOXCAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def smooth_triangular_filter(
        self,
        cut_below_period=1,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies a triangular smoothing filter to the lightcurve data, using a specified cutoff
        period and padding modes. The filtering can be applied to the entire lightcurve or
        to individual sections.

        Parameters
        ----------
        cut_below_period : float, optional
            The cutoff period below which the filter is applied, in days.

            This is currently a cutoff period at 90%, which means the periods under this cutoff
            should be attenuated of more than 90%.
            See the cutting_periods.ipynb notebook and SMOOTH_TRIANGULAR_FILTER_DIVISER for
            more informations.

            Default is ``1``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        return self._apply_filter(
            triangular_filter,
            cut_below_period / SMOOTH_TRIANGULAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def smooth_sin4_filter(
        self,
        cut_below_period=1,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies a sin4 smoothing filter to the lightcurve data, using a specified cutoff
        period and padding modes. The filtering can be applied to the entire lightcurve or
        to individual sections.

        Parameters
        ----------
        cut_below_period : float, optional
            The cutoff period below which the filter is applied, in days.

            This is currently a cutoff period at 90%, which means the periods under this cutoff
            should be attenuated of more than 90%.
            See the cutting_periods.ipynb notebook and SMOOTH_SIN4_FILTER_DIVISER for
            more informations.

            Default is ``1``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        return self._apply_filter(
            sin4_filter,
            cut_below_period / SMOOTH_SIN4_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_boxcar_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies a boxcar normalization filter to the lightcurve data, using a specified cutoff
        period and padding modes. The filtering can be applied to the entire lightcurve or
        to individual sections.

        Parameters
        ----------
        cut_above_period : float, optional
            The cutoff period above which the filter is applied, in days.

            This is currently a cutoff period at 100%, which means the periods below
            this cutoff will not be touched, but the periods over this cutoff are not
            necessarily very much attenuated.
            See the cutting_periods.ipynb notebook and NORMALIZE_BOXCAR_FILTER_DIVISER for
            more informations.

            Default is ``30``.
        divide : bool, optional
            If ``True``, the original flux will be divided by the filtered flux in order to
            normalize it. This means the result will be centered around 1, and fluctuations
            around the local mean will be scaled accordingly; but there must not be near-zero
            flux.

            If ``False``, the filtered flux will instead be substracted from the original flux,
            which means that the result will be centered around 0 and fluctuations are locally
            the same as the original flux.

            Default is ``False``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if divide:

            def boxcar_normalisation(flux, window_size, padding_modes):
                return flux / boxcar_filter(flux, window_size, padding_modes)

        else:

            def boxcar_normalisation(flux, window_size, padding_modes):
                return flux - boxcar_filter(flux, window_size, padding_modes)

        return self._apply_filter(
            boxcar_normalisation,
            cut_above_period / NORMALIZE_BOXCAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_triangular_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies a triangular normalization filter to the lightcurve data, using a specified cutoff
        period and padding modes. The filtering can be applied to the entire lightcurve or
        to individual sections.

        Parameters
        ----------
        cut_above_period : float, optional
            The cutoff period above which the filter is applied, in days.

            This is currently a cutoff period at 100%, which means the periods below
            this cutoff will not be touched, but the periods over this cutoff are not
            necessarily very much attenuated.
            See the cutting_periods.ipynb notebook and NORMALIZE_TRIANGULAR_FILTER_DIVISER for
            more informations.

            Default is ``30``.
        divide : bool, optional
            If ``True``, the original flux will be divided by the filtered flux in order to
            normalize it. This means the result will be centered around 1, and fluctuations
            around the local mean will be scaled accordingly; but there must not be near-zero
            flux.

            If ``False``, the filtered flux will instead be substracted from the original flux,
            which means that the result will be centered around 0 and fluctuations are locally
            the same as the original flux.

            Default is ``False``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if divide:

            def triangular_normalisation(flux, window_size, padding_modes):
                return flux / triangular_filter(flux, window_size, padding_modes)

        else:

            def triangular_normalisation(flux, window_size, padding_modes):
                return flux - triangular_filter(flux, window_size, padding_modes)

        return self._apply_filter(
            triangular_normalisation,
            cut_above_period / NORMALIZE_TRIANGULAR_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_sin4_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies a sin4 normalization filter to the lightcurve data, using a specified cutoff
        period and padding modes. The filtering can be applied to the entire lightcurve or
        to individual sections.

        Parameters
        ----------
        cut_above_period : float, optional
            The cutoff period above which the filter is applied, in days.

            This is currently a cutoff period at 100%, which means the periods below
            this cutoff will not be touched, but the periods over this cutoff are not
            necessarily very much attenuated.
            See the cutting_periods.ipynb notebook and NORMALIZE_SIN4_FILTER_DIVISER for
            more informations.

            Default is ``30``.
        divide : bool, optional
            If ``True``, the original flux will be divided by the filtered flux in order to
            normalize it. This means the result will be centered around 1, and fluctuations
            around the local mean will be scaled accordingly; but there must not be near-zero
            flux.

            If ``False``, the filtered flux will instead be substracted from the original flux,
            which means that the result will be centered around 0 and fluctuations are locally
            the same as the original flux.

            Default is ``False``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if divide:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux / sin4_filter(flux, window_size, padding_modes)

        else:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux - sin4_filter(flux, window_size, padding_modes)

        return self._apply_filter(
            sin4_normalisation,
            cut_above_period / NORMALIZE_SIN4_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_sin4_3_times_filter(
        self,
        cut_above_period=30,
        divide=False,
        section_mode="all",
        default_padding=("mirror_both", "mirror_both"),
    ):
        """
        Applies three times in a row the sin4 normalization filter to the lightcurve data,
        using a specified cutoff period and padding modes. The filtering can be applied to
        the entire lightcurve or to individual sections.

        Parameters
        ----------
        cut_above_period : float, optional
            The cutoff period above which the filter is applied, in days.

            This is currently a cutoff period at 100%, which means the periods below
            this cutoff will not be touched, but the periods over this cutoff are not
            necessarily very much attenuated.
            See the cutting_periods.ipynb notebook and NORMALIZE_SIN4_3_TIMES_FILTER_DIVISER for
            more informations.

            Default is ``30``.
        divide : bool, optional
            If ``True``, the original flux will be divided by the filtered flux in order to
            normalize it. This means the result will be centered around 1, and fluctuations
            around the local mean will be scaled accordingly; but there must not be near-zero
            flux.

            If ``False``, the filtered flux will instead be substracted from the original flux,
            which means that the result will be centered around 0 and fluctuations are locally
            the same as the original flux.

            Default is ``False``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if divide:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux / sin4_filter(flux, window_size, padding_modes)

        else:

            def sin4_normalisation(flux, window_size, padding_modes):
                return flux - sin4_filter(flux, window_size, padding_modes)

        def sin4_normalisation_3_times(flux, window_size, padding_modes):
            sin4_1 = sin4_normalisation(flux, window_size, padding_modes)
            sin4_2 = sin4_normalisation(sin4_1, window_size, padding_modes)
            return sin4_normalisation(sin4_2, window_size, padding_modes)

        return self._apply_filter(
            sin4_normalisation_3_times,
            cut_above_period / NORMALIZE_SIN4_3_TIMES_FILTER_DIVISER,
            section_mode,
            default_padding,
        )

    def normalize_adaptative_filter(
        self,
        cutoff_periods,
        thresholds,
        filter_func,
        period_window_diviser,
        return_indicators,
        default_padding,
        section_mode,
    ):
        """
        Applies an adaptative filter which automatically switches from a little window size
        to a big one. See the documentation of `filtering.adaptive_filter` for more theorical
        information.

        Parameters
        ----------
        cutoff_periods : tuple
            A tuple of two cutoff periods (little_filter_cutoff, big_filter_cutoff) for
            the adaptive filter.
        thresholds : tuple
            A tuple of two thresholds (min_threshold, max_threshold) in between which the
            filter will alternate between big and little filter.
        filter_func : callable
            The filter function to apply to the lightcurve data.
        period_window_diviser : float
            The number by which divide the cutoff period to obtain the window duration.
        return_indicators : bool
            Whether to return indicators of the adaptive filter.
            If ``True``, the function will return an array whose non-zero values correspond to
            where the little filter was used and with which intensity, to be used with
            the `plot_corrections` method.
        default_padding : tuple
            The default padding to apply to the flux data.
        section_mode : str
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections.

        Returns
        -------
        Lightcurve or array-like
            If `return_indicators` is ``False``, returns the current lightcurve object.

            If `return_indicators` is ``True``, returns an array-like object containing
            the indicators.

        Notes
        -----
        This method is meant to be used internally and should not be called
        directly by users.
        """
        if section_mode == "section":
            times, fluxes, _, _ = self._divide_flux_between_jumps()
            new_fluxes = []
            new_indicators = []
            for time, flux in zip(times, fluxes):
                lc = Lightcurve()
                lc.debug = self.debug
                lc.init_from_data(time, flux)
                indicator = lc.normalize_adaptative_filter(
                    cutoff_periods=cutoff_periods,
                    thresholds=thresholds,
                    filter_func=filter_func,
                    period_window_diviser=period_window_diviser,
                    return_indicators=True,
                    default_padding=default_padding,
                    section_mode="all",
                )
                new_fluxes.append(lc.flux)
                new_indicators.append(indicator)
            new_flux = np.concatenate(new_fluxes)

            indicator_outlier = np.concatenate(new_indicators)
            self.flux = new_flux

        elif section_mode == "all":
            window_duration_1 = self._duration_to_odd_size(
                cutoff_periods[0] / period_window_diviser
            )
            window_duration_2 = self._duration_to_odd_size(
                cutoff_periods[1] / period_window_diviser
            )
            filtered, indicator_outlier = adaptive_filter(
                self.flux,
                (window_duration_1, window_duration_2),
                thresholds,
                padding_modes=default_padding,
                filter_function=filter_func,
                debug=self.debug,
                time=self.time,
            )
            self.flux = self.flux / filtered
            percentage_little_filter = sum(indicator_outlier > 0) / len(indicator_outlier) * 100

            # TODO make this for section mode section too
            self._add_operation(
                f"normalized with adaptative filter {filter_func.__name__}, cutoff periods of "
                + f"{cutoff_periods[0]:.2f} d and {cutoff_periods[1]:.2f} d, from threshold "
                + f"{thresholds[0]:.4f} to {thresholds[1]:.4f}, little filter used for "
                + f"{percentage_little_filter:.2f}% of data"
            )

        if return_indicators:
            return indicator_outlier
        else:
            return self

    def normalize_adaptative_boxcar_filter(
        self,
        cut_above_periods=(0.5, 30),
        thresholds=(0.8, 1.3),
        return_indicators=False,
        default_padding=("mirror_both", "mirror_both"),
        section_mode="all",
    ):
        """
        Applies an adaptive boxcar normalization filter to the lightcurve data, using specified
        cutoff periods and thresholds. The filtering can be applied to the entire lightcurve or
        to individual sections.

        See the documentation of `filtering.adaptive_filter` and
        `Lightcurve.normalize_adaptative_filter` for more informations.

        Parameters
        ----------
        cut_above_periods : tuple, optional
            A tuple of two cutoff periods (in days) for the adaptive filter.
            Default is ``(0.5, 30)``.
        thresholds : tuple, optional
            A tuple of two thresholds for the adaptive filter. Default is ``(0.8, 1.3)``.
        return_indicators : bool, optional
            Whether to return indicators for the adaptive filter. Default is ``False``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.

        Returns
        -------
        Lightcurve or array-like
            If `return_indicators` is ``False``, returns the current lightcurve object.

            If `return_indicators` is ``True``, returns an array-like object containing
            the indicators.
        """
        return self.normalize_adaptative_filter(
            cut_above_periods,
            thresholds,
            boxcar_filter,
            NORMALIZE_BOXCAR_FILTER_DIVISER,
            return_indicators,
            default_padding,
            section_mode,
        )

    def normalize_adaptative_triangular_filter(
        self,
        cut_above_periods=(0.5, 30),
        thresholds=(0.8, 1.3),
        return_indicators=False,
        default_padding=("mirror_both", "mirror_both"),
        section_mode="all",
    ):
        """
        Applies an adaptive triangular normalization filter to the lightcurve data, using specified
        cutoff periods and thresholds. The filtering can be applied to the entire lightcurve or
        to individual sections.

        See the documentation of `filtering.adaptive_filter` and
        `Lightcurve.normalize_adaptative_filter` for more informations.

        Parameters
        ----------
        cut_above_periods : tuple, optional
            A tuple of two cutoff periods (in days) for the adaptive filter.
            Default is ``(0.5, 30)``.
        thresholds : tuple, optional
            A tuple of two thresholds for the adaptive filter. Default is ``(0.8, 1.3)``.
        return_indicators : bool, optional
            Whether to return indicators for the adaptive filter. Default is ``False``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.

        Returns
        -------
        Lightcurve or array-like
            If `return_indicators` is ``False``, returns the current lightcurve object.

            If `return_indicators` is ``True``, returns an array-like object containing
            the indicators.
        """
        return self.normalize_adaptative_filter(
            cut_above_periods,
            thresholds,
            triangular_filter,
            NORMALIZE_TRIANGULAR_FILTER_DIVISER,
            return_indicators,
            default_padding,
            section_mode,
        )

    def normalize_adaptative_sin4_filter(
        self,
        cut_above_periods=(0.5, 30),
        thresholds=(0.8, 1.3),
        return_indicators=False,
        default_padding=("mirror_both", "mirror_both"),
        section_mode="all",
    ):
        """
        Applies an adaptive sin4 normalization filter to the lightcurve data, using specified
        cutoff periods and thresholds. The filtering can be applied to the entire lightcurve or
        to individual sections.

        See the documentation of `filtering.adaptive_filter` and
        `Lightcurve.normalize_adaptative_filter` for more informations.

        Parameters
        ----------
        cut_above_periods : tuple, optional
            A tuple of two cutoff periods (in days) for the adaptive filter.
            Default is ``(0.5, 30)``.
        thresholds : tuple, optional
            A tuple of two thresholds for the adaptive filter. Default is ``(0.8, 1.3)``.
        return_indicators : bool, optional
            Whether to return indicators for the adaptive filter. Default is ``False``.
        default_padding : tuple, optional
            The default padding to apply to the flux data. Default is
            ``("mirror_both", "mirror_both")``.
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.

        Returns
        -------
        Lightcurve or array-like
            If `return_indicators` is ``False``, returns the current lightcurve object.

            If `return_indicators` is ``True``, returns an array-like object containing
            the indicators.
        """
        return self.normalize_adaptative_filter(
            cut_above_periods,
            thresholds,
            sin4_filter,
            NORMALIZE_SIN4_FILTER_DIVISER,
            return_indicators,
            default_padding,
            section_mode,
        )

    def normalize_polynomial_filter(self, section_mode="all", order=2, divide=False):
        """
        Applies a polynomial normalization filter to the lightcurve data, using a specified
        polynomial order. The filtering can be applied to the entire lightcurve or to
        individual sections.

        Parameters
        ----------
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        order : int, optional
            The order of the polynomial filter. Default is ``2``.
        divide : bool, optional
            Whether to divide the flux by the polynomial filter. If ``False``, the
            polynomial filter is subtracted from the flux. Default is ``False``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if section_mode == "section":
            times, fluxes, _, _ = self._divide_flux_between_jumps()
            new_fluxes = []
            for time, flux in zip(times, fluxes):
                lc = Lightcurve()
                lc.debug = self.debug
                lc.init_from_data(time, flux)
                lc.normalize_polynomial_filter(section_mode="all", order=order, divide=divide)
                new_fluxes.append(lc.flux)
            new_flux = np.concatenate(new_fluxes)

            self.flux = new_flux

        elif section_mode == "all":
            filtered = polynomial_filter(self.time, self.flux, order)
            if divide:
                self.flux = self.flux / filtered
            else:
                self.flux = self.flux - filtered

        return self._add_operation(f"normalized with polynomial filter of order {order}")

    def smooth_polynomial_filter(self, section_mode="all", order=2):
        """
        Applies a polynomial smoothing filter to the lightcurve data, using a specified
        polynomial order. The filtering can be applied to the entire lightcurve or to
        individual sections. This is mostly a polynomial fitting with sugar coating.

        Parameters
        ----------
        section_mode : str, optional
            The mode for applying the filter. Options are ``"all"`` to apply the
            filter to the entire lightcurve, and ``"section"`` to apply the filter
            to individual sections. Default is ``"all"``.
        order : int, optional
            The order of the polynomial filter. Default is ``2``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if section_mode == "section":
            times, fluxes, _, _ = self._divide_flux_between_jumps()
            new_fluxes = []
            for time, flux in zip(times, fluxes):
                lc = Lightcurve()
                lc.debug = self.debug
                lc.init_from_data(time, flux)
                lc.smooth_polynomial_filter(section_mode="all", order=order)
                new_fluxes.append(lc.flux)
            new_flux = np.concatenate(new_fluxes)

            self.flux = new_flux

        elif section_mode == "all":
            filtered = polynomial_filter(self.time, self.flux, order)
            self.flux = filtered

        return self._add_operation(f"smoothed with polynomial filter of order {order}")

    # * -------------------- SECTIONS CORRECTIONS -------------------- * #

    def find_jumps_in_time(self, min_jump_days=3, max_jump_days=np.inf):
        """
        Identifies jumps in the lightcurve data based on time gaps. It will only detect gaps
        that are between the given min and max jump days.

        These jumps are added to the lightcurve's jumps dictionary.

        Parameters
        ----------
        min_jump_days : float, optional
            The minimum number of days in the gap to be considered a jump.
            Default is ``3``.
        max_jump_days : float, optional
            The maximum number of days in the gap to be considered a jump.
            Default is infinity (``np.inf``).

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        lc_copy = self.copy()
        lc_copy.remove_invalids(removal_mode="delete")
        diff = np.diff(lc_copy.time)
        holes = 1 + np.flatnonzero(np.logical_and(diff >= min_jump_days, diff < max_jump_days))
        n_jumps = 0
        for hole_index_without_nan in holes:
            hole_time = lc_copy.time[hole_index_without_nan]
            hole_index = np.searchsorted(self.time, hole_time, side="left")
            if not hole_index in self.jumps:
                self.jumps[hole_index] = {}
                n_jumps += 1
        self.jumps = dict(sorted(self.jumps.items()))
        return self._add_operation(f"found {n_jumps} new jumps in time")

    def find_jumps_in_flux(self, threshold=10, deviation_mode="std"):
        """
        Identifies jumps in the lightcurve data based on sudden changes in flux, which are
        considered outliers in the derivative of the flux.

        These jumps are added to the lightcurve's jumps dictionary.

        Parameters
        ----------
        threshold : float, optional
            The threshold for identifying outliers in the derivative of the flux.
            Default is ``10``.
        deviation_mode : str, optional
            The mode for calculating the deviation. Options are ``"std"`` for standard
            deviation and ``"iqr"`` for interquartile range. Default is ``"std"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        lc_copy = self.copy()
        lc_copy.remove_invalids(removal_mode="delete")
        deriv = np.diff(lc_copy.flux) / np.diff(lc_copy.time)
        mean = np.nanmean(deriv)
        deviation_func = get_deviation(deviation_mode)
        sigma = deviation_func(deriv)
        jumps = 1 + np.flatnonzero(abs(deriv - mean) > threshold * sigma)
        n_jumps = 0
        for hole_index_without_nan in jumps:
            hole_time = lc_copy.time[hole_index_without_nan]
            hole_index = np.searchsorted(self.time, hole_time, side="left")
            if not hole_index in self.jumps:
                self.jumps[hole_index] = {}
                n_jumps += 1
        self.jumps = dict(sorted(self.jumps.items()))
        return self._add_operation(f"found {n_jumps} new jumps in flux")

    def find_jumps_between_sectors(self):
        """
        Add a jump at the start of each new sector.

        These jumps are added to the lightcurve's jumps dictionary.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        n_jumps = 0
        _, sector_indexes = np.unique(self.sector, return_index=True)
        for sector_index in sector_indexes:
            if sector_index > 0 and not sector_index in self.jumps:
                self.jumps[sector_index] = {}
                n_jumps += 1

        self.jumps = dict(sorted(self.jumps.items()))
        return self._add_operation(f"found {n_jumps} new jumps between sectors")

    def find_jumps_between_tess_sectors(
        self,
        find_inter_sector_jumps=True,
        find_middle_sector_jumps=True,
        find_intra_sector_jumps=False,
    ):
        """
        Identifies jumps between TESS sectors in the lightcurve data based on specified
        criteria. It can find jumps between sectors, at the middle of the sectors, and
        and between each quarter of sector.

        These jumps are added to the lightcurve's jumps dictionary.

        Parameters
        ----------
        find_inter_sector_jumps : bool, optional
            Whether to add jumps between sectors. Default is ``True``.
        find_middle_sector_jumps : bool, optional
            Whether to add jumps that are in the middle of each sector. Default is ``True``.
        find_intra_sector_jumps : bool, optional
            Whether to add jumps that are in the end of the first and third quarter of each
            sector, for the latest sectors. Default is ``False``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        zero_epoch = 2457000.0
        inter_sector_down_segments, middle_sector_down_segments, intra_sector_down_segments = (
            find_tess_down_segments()
        )

        # inter sector jumps
        inter_sector_jumps = []
        for _, _, down_end in inter_sector_down_segments:
            end_day = down_end.jd - zero_epoch

            jump = np.searchsorted(self.time, end_day, side="left")
            if jump > 0 and jump < len(self.time):
                inter_sector_jumps.append(jump)
        inter_sector_jumps = list(set(inter_sector_jumps))

        # middle sector jumps
        middle_sector_jumps = []
        for _, _, down_end in middle_sector_down_segments:
            end_day = down_end.jd - zero_epoch

            jump = np.searchsorted(self.time, end_day, side="left")
            if jump > 0 and jump < len(self.time) and jump not in inter_sector_jumps:
                middle_sector_jumps.append(jump)
        middle_sector_jumps = list(set(middle_sector_jumps))

        # intra sector jumps
        intra_sector_jumps = []
        for _, _, down_end in intra_sector_down_segments:
            end_day = down_end.jd - zero_epoch

            jump = np.searchsorted(self.time, end_day, side="left")
            if (
                jump > 0
                and jump < len(self.time)
                and jump not in inter_sector_jumps
                and jump not in middle_sector_jumps
            ):
                intra_sector_jumps.append(jump)
        intra_sector_jumps = list(set(intra_sector_jumps))

        # add jumps
        jumps = []
        if find_inter_sector_jumps:
            jumps += inter_sector_jumps
        if find_middle_sector_jumps:
            jumps += middle_sector_jumps
        if find_intra_sector_jumps:
            jumps += intra_sector_jumps
        n_jumps = 0
        for jump in jumps:
            if not jump in self.jumps:
                self.jumps[jump] = {}
                n_jumps += 1

        self.jumps = dict(sorted(self.jumps.items()))
        return self._add_operation(
            f"found {n_jumps} new jumps between TESS sectors and inter-sectors"
        )

    def remove_large_jumps(
        self, min_jump_days=90, dt="median", remove_jumps_metadata=False, return_removed_jumps=False
    ):
        """
        Removes large jumps in the lightcurve data, by identifying gaps
        in the time array that are larger than a specified minimum number of days.
        These jumps are replaced by jumps of a given lenght inside the data, and
        removed from the lightcurve's jumps dictionary if `remove_jumps_metadata` is ``True``.

        It also:

        - removes the NaNs that are placed inside this jump, and the NaNs on both sides\
        of the lightcurve itself
        - set the first time bin to 0

        Parameters
        ----------
        min_jump_days : float, optional
            The minimum number of days in the gap to be considered a large jump.
            Default is ``90``.
        dt : str or float, optional
            The lenght of the gap to put in the data instad of the large jump. This is meant
            to be non-zero, else two flux points will be at the same time.

            This can be one of the following:

            - ``"mean"``: Use the mean time difference between consecutive data points.
            - ``"median"``: Use the median time difference between consecutive data points.
            - A number: Use a fixed time difference specified by this number.

            Default is ``"median"``.
        remove_jumps_metadata : bool, optional
            Whether to remove the jumps that have been removed from the time array from the
            lightcurve metadata. You want it to ``True`` only it you consider the lightcurve
            continuous in this point after removing the time gap. Default is ``False``.
        return_removed_jumps : bool, optional
            Whether to return the removed jumps instad of the Lightcurve. Default is ``False``.

        Returns
        -------
        Lightcurve or dict
            If `return_removed_jumps` is ``False``, returns the current lightcurve object.

            If `return_removed_jumps` is ``True``, returns a dictionary of the removed jumps.
        """
        # remove invalid data in-between jumps
        lc_copy = self.copy()
        lc_copy.jumps = {}
        lc_copy.remove_invalids(removal_mode="delete")
        if len(lc_copy.time) == 0:
            if return_removed_jumps:
                return {}
            return self
        lc_copy.find_jumps_in_time(min_jump_days=min_jump_days)

        for jump in list(lc_copy.jumps):
            valid_time_before = lc_copy.time[jump - 1]
            valid_time_after = lc_copy.time[jump]
            time_bins_to_remove = np.logical_and(
                self.time > valid_time_before, self.time < valid_time_after
            )
            self.remove_data(time_bins_to_remove, removal_mode="delete")

        # remove invalid data on both sides of the lightcurve
        first_valid_time = lc_copy.time[0]
        last_valid_time = lc_copy.time[-1]
        time_bins_to_remove = np.logical_or(
            self.time < first_valid_time, self.time > last_valid_time
        )
        self.remove_data(time_bins_to_remove, removal_mode="delete")

        # move around the time array to close the gap
        lc_copy = self.copy()
        lc_copy.jumps = {}
        lc_copy.find_jumps_in_time(min_jump_days=min_jump_days)
        num_jumps_to_remove = len(list(lc_copy.jumps))

        removed_jumps = {}

        first_day = self.time[0]
        self.time -= first_day
        self._corresponding_time -= first_day
        removed_jumps[0] = {"large-jump-removed": first_day}

        for i_jump in range(num_jumps_to_remove):
            jump = list(lc_copy.jumps)[i_jump]
            jump_before = 0
            if i_jump > 0:
                jump_before = list(lc_copy.jumps)[i_jump - 1]

            cadence_calc_time = self.time[jump_before:jump]
            if dt == "median":
                dt = np.median(np.diff(cadence_calc_time))
            elif dt == "mean":
                dt = np.mean(np.diff(cadence_calc_time))

            move_by = -self.time[jump] + self.time[jump - 1] + dt
            corresponding_time_jump_index = searchsorted_with_nan_handling(
                self._corresponding_time, self.time[jump]
            )

            removed_jumps[jump] = {"large-jump-removed": -move_by}

            self.time[jump:] += move_by
            self._corresponding_time[corresponding_time_jump_index:] += move_by

            if remove_jumps_metadata and jump in self.jumps:
                del self.jumps[jump]

        self._add_operation(f"removed {num_jumps_to_remove} jumps larger than {min_jump_days}")
        if return_removed_jumps:
            return removed_jumps
        else:
            return self

    def rescale_sections(self, deviation_mode="iqr"):
        """
        Rescales the sections of the lightcurve to equalize their deviation for a given deviation
        mode.

        Parameters
        ----------
        deviation_mode : str, optional
            The mode for calculating the deviation. Options are ``"std"`` for
            standard deviation and ``"iqr"`` for interquartile range.
            Default is ``"iqr"`` because it permits not to take into account the outliers
            which can be very significant for noisy sections.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        fluxes = self._divide_flux_between_jumps()[1]
        if len(fluxes) <= 1:
            return self

        deviation_func = get_deviation(deviation_mode)

        previous_deviation = np.nan
        for i, _ in enumerate(fluxes):
            if np.isnan(previous_deviation):
                previous_deviation = deviation_func(fluxes[i])
                continue
            fluxes[i] = fluxes[i] / np.nanmean(fluxes[i])
            current_deviation = deviation_func(fluxes[i])
            if np.isfinite(deviation_func(fluxes[i - 1])):
                previous_deviation = deviation_func(fluxes[i - 1])
            fluxes[i] = 1 + (fluxes[i] - 1) * previous_deviation / current_deviation

        self.flux = np.concatenate(fluxes)

        return self._add_operation(f"rescaled sections with {deviation_mode} deviation mode")

    # * -------------------- POINTS CORRECTIONS -------------------- * #

    def remove_data(self, indexes, removal_mode="delete"):
        """
        Removes the given data from the time, flux, and quality arrays based on the given indexes
        and removal mode.

        The removal mode can be:

        - ``"delete"`` to entirely remove the values from the arrays
        - ``"NaN"`` to set the corresponding flux indexes to ``NaN``
        - ``"zero"`` to zero out the flux.

        Parameters
        ----------
        indexes : array-like
            An array of booleans of the same size as Lightcurve.time, with ``False`` to keep the
            flux bin and ``True`` to remove it.
        removal_mode : str, optional
            The removal mode, which can be ``"delete"``, ``"NaN"``, or ``"zero"``.
            Default is ``"delete"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_kbonus("tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> lc.remove_data(lc.sector == 5).remove_data(lc.time > 300)
        <lobster.lightcurve.Lightcurve object at ...>
        >>> len(lc.time)
        8011
        """
        if removal_mode == "NaN":
            self.flux[indexes] = np.nan
        elif removal_mode == "zero":
            self.flux[indexes] = 0.0
        elif removal_mode == "delete":
            new_time = np.delete(self.time, indexes)
            new_jumps = {}
            for jump_index in self.jumps:
                new_index = np.searchsorted(new_time, self.time[jump_index])
                # do not add jump at the beginning and at the end
                if 0 < new_index < len(new_time) - 1:
                    new_jumps[new_index] = self.jumps[jump_index]

            self._corresponding_time = np.where(
                np.isin(self._corresponding_time, self.time[indexes]),
                np.nan,
                self._corresponding_time,
            )
            self.time = new_time
            self.flux = np.delete(self.flux, indexes)
            self.jumps = dict(sorted(new_jumps.items()))
        else:
            raise ValueError(
                f"could not find right data removal mode, unknown mode '{removal_mode}'"
            )
        return self

    def remove_outliers(
        self,
        threshold=5,
        normalize_period_cut=3,
        separate_fluxes_by="sector",
        removal_mode="NaN",
        deviation_mode="std",
    ):
        """
        Removes outliers from the lightcurve data, that are flux values outside of the
        `threshold*deviation` difference with the mean flux. It can normalize the flux before
        identifying outliers and perform outliers identification for each sector or for the
        global lightcurve.

        Parameters
        ----------
        threshold : float, optional
            The threshold for identifying outliers. Default is ``5``.
        normalize_period_cut : float, optional
            The period cut for normalization. If set to 0, no normalization will be done.
            Default is ``3``.
        separate_fluxes_by : str, optional
            The mode for separating fluxes. Options are ``None``, ``"sector"`` and
            ``"half-sector"``.

            - if ``None``, the identification will be done globally.
            - if ``"sector"``, the flux is separated between each sector, which means the\
              statistics of a very bad sector will not influence the whole lightcurve for example.
            - if ``"half-sector"``, the flux is separated between each TESS half sector.

            Default is ``"sector"``.
        removal_mode : str, optional
            The mode for removing outliers. Options are ``"zero"``, ``"NaN"`` and ``"delete"``.
            Default is ``"NaN"``.
        deviation_mode : str, optional
            The way to compute deviation. Options are ``"std"`` and ``"iqr"``.
            Default is ``"std"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Examples
        --------
        >>> lc = Lightcurve()
        >>> lc.load_kbonus("tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
        <lobster.lightcurve.Lightcurve object at ...>
        >>> len(lc.time)
        65680
        >>> lc.flux[500:700] = np.nan
        >>> lc.flux[800:900] = np.inf
        >>> lc.remove_invalids()
        <lobster.lightcurve.Lightcurve object at ...>
        >>> len(lc.time)
        65380
        """
        original_length = len(self.flux)
        if original_length == 0:
            return self

        deviation_func = get_deviation(deviation_mode)

        lc_copy = self.copy()
        if separate_fluxes_by:
            lc_copy.jumps = {}
            if separate_fluxes_by == "sector":
                lc_copy.find_jumps_between_sectors()
            elif separate_fluxes_by == "half-sector":
                lc_copy.find_jumps_between_tess_sectors()
            else:
                raise ValueError(
                    f"could not find flux separation mode, unknown mode '{separate_fluxes_by}'"
                )
            # pylint: disable=protected-access
            times, fluxes, _, _ = lc_copy._divide_flux_between_jumps()
        else:
            times = [lc_copy.time]
            fluxes = [lc_copy.flux]

        if normalize_period_cut > 0:
            for i, (time, flux) in enumerate(zip(times, fluxes)):
                little_lc = Lightcurve().init_from_data(time, flux)
                little_lc.normalize_sin4_3_times_filter(normalize_period_cut)
                fluxes[i] = little_lc.flux

        i = 0
        to_remove = np.full_like(self.time, False, dtype=bool)
        for time, flux in zip(times, fluxes):
            if is_all_invalid(flux):
                continue
            mean = np.nanmean(flux)
            sigma = deviation_func(flux)
            to_remove[i : i + len(time)] = abs(flux - mean) > threshold * sigma
            i += len(time)
        self.remove_data(to_remove, removal_mode=removal_mode)
        n_removed = sum(to_remove)

        return self._add_operation(
            f"removed {n_removed} ({100*n_removed/original_length:.2f}%) outliers with "
            + f"{deviation_mode} deviation, {normalize_period_cut} "
            + f"period cut, separated by {separate_fluxes_by}, "
            + f"threshold = {threshold}"
        )

    def resample(self, dt="median", begin_at_dt_ratio=-0.5):
        """
        Resamples the lightcurve data to a regular time grid. This is useful to perform fix a
        constant time step to perform some analysis that require it, or as a binning method to
        both reduce the size of the data and remove high-frequency noise.

        The lightcurve's jump dictionnary will be updated accordingly.

        Parameters
        ----------
        dt : float or str, optional
            The time step for resampling.

            This can be one of the following:

            - ``"mean"``: Use the mean time difference between consecutive data points.
            - ``"median"``: Use the median time difference between consecutive data points.
            - A number: Use a fixed time difference specified by this number.

            Default is ``"median"``.
        begin_at_dt_ratio : float
            Where, as `dt` ratio unit, the resampling should occur; for example:

            - a ratio of :math:`0` means the first bin is :math:`[t_{min}, t_{min}+dt]`
            - a ratio of :math:`-0.5` means the first bin is :math:`[t_{min} - dt/2, t_{min}+dt/2]`
            - a ratio of :math:`-1` means the first bin is :math:`[t_{min} - dt, t_{min}]`

            Every next bin will be translated by :math:`dt`. Default is ``-0.5`` to have the
            resampled time array centered, but it means the first bin will have only half as much
            data than the rest of the bins.

            Should be between :math:`-1` and :math:`0`, so the first bin always contains the first
            time step.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        assert (
            begin_at_dt_ratio >= -1 and begin_at_dt_ratio <= 0
        ), "`begin_at_dt_ratio` should be between -1 and 0"

        if len(self.time) == 0:
            return self

        t_min = self.time[0]
        t_max = self.time[-1]

        if dt == "median":
            dt = self.dt_median
        elif dt == "mean":
            dt = self.dt_mean

        new_time = np.arange(t_min, t_max + dt / 2, dt)
        n = len(new_time)

        # Initialize new_flux with NaNs to ensure it has the correct length
        new_flux = np.full(n, np.nan)

        # Edges of the time bins
        bin_edges = np.concatenate(
            ([t_min + dt * begin_at_dt_ratio], new_time + dt * (1 + begin_at_dt_ratio))
        )

        # Indices of the bins to which each time value belongs
        bin_indices = np.digitize(self.time, bin_edges) - 1

        # Ensure bin indices are within valid range
        bin_indices = np.clip(bin_indices, 0, n - 1)

        bin_indices_original = np.digitize(self._corresponding_time, bin_edges) - 1

        # Sum of flux values for each bin
        keep = np.isfinite(self.flux)
        flux_sum = np.bincount(bin_indices[keep], weights=self.flux[keep], minlength=n)

        # Count of flux values for each bin
        flux_count = np.bincount(bin_indices[keep], minlength=n)

        # Mean flux values for each bin, handling cases where count is zero
        with np.errstate(divide="ignore", invalid="ignore"):
            new_flux = np.true_divide(flux_sum, flux_count)

        # Update jumps
        new_jumps = {}
        for jump_index, jump_value in self.jumps.items():
            # Locate the bin the original jump time belongs to
            original_time = self.time[jump_index]
            bin_index = np.digitize(original_time, bin_edges) - 1

            # Snap bin_index to valid range
            bin_index = np.clip(bin_index, 0, n - 1)

            # Map the jump to the new index
            if bin_index not in new_jumps:
                new_jumps[bin_index] = jump_value

        self.jumps = dict(sorted(new_jumps.items()))

        self._corresponding_time = np.where(
            np.logical_and(bin_indices_original >= 0, bin_indices_original < n),
            new_time[np.clip(bin_indices_original, 0, n - 1)],
            np.nan,
        )
        self.time = new_time
        self.flux = new_flux

        return self._add_operation(f"resampled data to new dt of {dt:.6f} d")

    def zero_pad(self, n=10, dt="median"):
        """
        Zero-pad the lightcurve by concatenating n zeros arrays of the same size of the
        original flux to the lightcurve.
        The new flux length will be (n+1) times the old one's length, but may not be exactly
        regularly sampled.

        Parameters
        ----------
        n : int, optional
            The number of zero arrays to concatenate. Default is ``10``.
        dt : float or str, optional
            The time step for the zero-padded arrays.

            This can be one of the following:

            - ``"mean"``: Use the mean time difference between consecutive data points.
            - ``"median"``: Use the median time difference between consecutive data points.
            - A number: Use a fixed time difference specified by this number.

            Default is ``"median"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if dt == "median":
            dt = self.dt_median
        elif dt == "mean":
            dt = self.dt_mean

        l = len(self.flux)
        zero_pad = np.zeros(n * l)
        t_pad = np.linspace(self.time[-1] + dt, self.time[-1] + n * l * dt, n * l)

        self.flux = np.concatenate((self.flux, zero_pad))
        self.time = np.concatenate((self.time, t_pad))
        return self._add_operation(f"zero padded {n} times with dt {dt}")

    def flux_to_ppm(self):
        """
        Centers the flux around 0 and multiplies by 10^6 to convert to PPM units.

        This is typically done to prepare the data for frequency analysis.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        # centering twice prevents floating point errors to accumulate
        if not is_all_invalid(self.flux[self.flux != 0]):
            self.flux[self.flux != 0] -= np.nanmean(self.flux[self.flux != 0])
            self.flux *= 1e6
            self.flux[self.flux != 0] -= np.nanmean(self.flux[self.flux != 0])
        self.unit = "ppm"
        return self._add_operation("centered and multiplied by 10^6 the flux")

    def force_positive(self):
        """
        Forces the flux to be positive and to have values not too close to 0 in comparison to
        its mean value.

        If the mean value is less than or equal to 0 or if the minimum flux value is less
        than or equal to a quarter of the mean value, the flux is adjusted to ensure positivity.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        if is_all_invalid(self.flux):
            return self
        mean = np.nanmean(self.flux)
        if mean <= 0 or np.nanmin(self.flux) <= abs(mean) / 4:
            self.flux = self.flux + abs(np.nanmin(self.flux)) + abs(mean) / 4
            self._add_operation("forced the flux to be positive")
        return self

    def remove_exponential_trends(self, exp_duration=2, poly_duration=10, poly_order=2):
        """
        Removes the exponential trends in the first days of every section, by:

        - fitting a polynomial :math:`P(t)` of order :math:`n` from :math:`t=d_{exp}` to
          :math:`t=d_{exp}+d_{poly}`
        - fitting a polynomial of order :math:`1`, :math:`T(t)` on
          :math:`\\ln[\\text{flux}(t) - P(t)]` from :math:`t=0` to :math:`t=d_{exp}`
        - removing the exponential trend: :math:`\\text{new flux} = \\text{flux} - \\exp[T(t)]`

        Parameters
        ----------
        exp_duration : float
            The expected maximum duration of the exponential trend.
        poly_duration : float
            The duration of the polynomial to fit on the flux after the exponential trend, used
            to not fit the exponential on the normal flux trend.
        poly_order : int
            The order of the polynomial to fit on the flux.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        valid_time = self.time[np.isfinite(self.flux)]
        valid_flux = self.flux[np.isfinite(self.flux)]

        def exp_model(t, a, tau):
            return a * np.exp(-(t - first_time_of_section) / tau)

        jump_indexes = list(self.jumps.keys())
        for i_begin, i_end in zip([0] + jump_indexes, jump_indexes + [len(self.time)]):
            first_time_of_section = self.time[i_begin]
            last_time_of_section = self.time[i_end - 1]
            section_mask = (valid_time >= first_time_of_section) & (
                valid_time <= last_time_of_section
            )
            exp_decay_mask = section_mask & (valid_time < first_time_of_section + exp_duration)
            poly_mask = (
                section_mask
                & (valid_time >= first_time_of_section + exp_duration)
                & (valid_time < first_time_of_section + exp_duration + poly_duration)
            )
            if sum(poly_mask) <= poly_order:
                continue

            poly = np.polynomial.Polynomial.fit(
                valid_time[poly_mask], valid_flux[poly_mask], poly_order
            )

            guess = np.array(
                [
                    valid_flux[exp_decay_mask][0] - np.mean(valid_flux[exp_decay_mask]),
                    exp_duration,
                ]
            )
            bounds = ([-np.inf, 0], [np.inf, np.inf])

            exp_data = valid_flux[exp_decay_mask] - poly(valid_time[exp_decay_mask])

            try:
                # pylint: disable=unbalanced-tuple-unpacking
                popt, _ = scipy.optimize.curve_fit(
                    exp_model,
                    valid_time[exp_decay_mask],
                    exp_data,
                    p0=guess,
                    bounds=bounds,
                )
                opt_a, opt_tau = popt
            except RuntimeError:
                continue

            self.flux[self.time >= first_time_of_section] -= exp_model(
                self.time[self.time >= first_time_of_section], opt_a, opt_tau
            )
            valid_flux = self.flux[np.isfinite(self.flux)]

        return self._add_operation(f"removed exponential trends of the first {exp_duration} days")

    def remove_first_days_of_sections(self, duration_to_remove, removal_mode="NaN"):
        """
        Removes the first days of each section, useful if there are exponential decays due to
        temperature fluctuations.

        Parameters
        ----------
        duration_to_remove : float
            The duration in days to remove of the beginning of each section.
        removal_mode : str, optional
            The mode for removing invalid data. Options are ``"delete"``, ``"NaN"``, and ``"zero"``.
            Default is ``"delete"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        original_len = len(self.time)
        to_remove = np.full_like(self.time, False, dtype=bool)
        for jump in [0] + list(self.jumps.keys()):
            first_time_of_section = self.time[jump]
            to_remove_section = (self.time >= first_time_of_section) & (
                self.time < first_time_of_section + duration_to_remove
            )
            to_remove = to_remove | to_remove_section
        self.remove_data(to_remove, removal_mode=removal_mode)

        return self._add_operation(
            f"removed first {duration_to_remove:.3f} days of each sections, "
            + f"{sum(to_remove)} points removed ({sum(to_remove)/original_len*100:.2f}%)"
        )

    def remove_sections_with_highest_deviation(
        self, threshold=4, removal_mode="NaN", deviation_mode="std"
    ):
        """
        Entirely removes sections that deviate too much from the baseline set by the rest of the
        lightcurve. This permits to not get a wrong result all because of one bad section.

        Parameters
        ----------
        threshold : float
            How much more than the rest of the sections the deviation must be for the section to
            be removed.
        removal_mode : str, optional
            The mode for removing invalid data. Options are ``"delete"``, ``"NaN"``, and ``"zero"``.
            Default is ``"delete"``.
        deviation_mode : str, optional
            The mode for calculating the deviation. Options are ``"std"`` for standard
            deviation and ``"iqr"`` for interquartile range. Default is ``"std"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        deviation_func = get_deviation(deviation_mode)
        to_remove = np.full_like(self.time, False, dtype=bool)
        jump_indexes = list(self.jumps.keys())
        n_sections_removed = 0
        for i_begin, i_end in zip([0] + jump_indexes, jump_indexes + [len(self.time)]):
            section_flux = self.flux[i_begin:i_end]
            other_flux = np.concatenate((self.flux[:i_begin], self.flux[i_end:]))
            if deviation_func(section_flux) > threshold * deviation_func(other_flux):
                to_remove[i_begin:i_end] = True
                n_sections_removed += 1
        self.remove_data(to_remove, removal_mode=removal_mode)

        return self._add_operation(f"removed {n_sections_removed} sections with highest deviation")

    def remove_invalids(self, removal_mode="delete"):
        """
        Removes invalid fluxes from the data, which corresponds to NaNs and infinite values.

        Parameters
        ----------
        removal_mode : str, optional
            The mode for removing invalid data. Options are ``"delete"``, ``"NaN"``, and ``"zero"``.
            Default is ``"delete"``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        to_remove = np.logical_not(np.isfinite(self.flux))
        original_len = len(self.flux)
        if original_len == 0:
            return self
        self.remove_data(to_remove, removal_mode)
        return self._add_operation(
            f"removed {sum(to_remove)} invalid data points ({sum(to_remove)/original_len*100:.2f}%)"
        )

    def remove_quality(
        self, removal_mode="NaN", quality_to_keep=[-1, 0]
    ):  # pylint: disable=dangerous-default-value
        """
        Removes flux where the quality flags are not in the list `quality_to_keep`.
        By default, it keeps quality flags 0 and -1, which typically mean no whatsoever
        bad quality flag.

        Parameters
        ----------
        removal_mode : str, optional
            The mode for removing data. Options are ``"NaN"``, ``"zero"``, and ``"delete"``.
            Default is ``"NaN"``.
        quality_to_keep : list of int, optional
            The list of quality flags to keep. Default is ``[-1, 0]``.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        to_remove = ~np.isin(self.quality, quality_to_keep)
        original_len = len(self.flux)
        if original_len == 0:
            return self
        self.remove_data(to_remove, removal_mode)
        return self._add_operation(
            f"removed {sum(to_remove)} bad quality data points"
            + f" ({sum(to_remove)/original_len*100:.2f}%)"
        )

    # * -------------------- OPERATIONS TRACKING -------------------- * #

    @property
    def debug(self):
        """
        Gets the debug mode status of the lightcurve object.

        With debug mode activated, the operations that are saved inside the lightcurve will have
        their flux plot and PSD plot save at the same time. This is particularly useful to have
        a detailled view of the analysis done step-by-step; but will be way slower.

        Returns
        -------
        bool
            The debug mode status of the lightcurve object.
        """
        return self._debug

    @debug.setter
    def debug(self, v):
        """
        Parameters
        ----------
        v : bool
            The debug mode status to set.
        """
        self._debug = v
        if v:
            self._add_operation("beggining the debug mode")

    def _add_operation(self, comment):
        """
        Adds a linear summary of operations that happened to the lightcurve.

        If the debug mode is enabled (`self.debug` is ``True``), it also saves the figure
        of the flux and PSD associated with the operation, right after it happened.

        Parameters
        ----------
        comment : str
            A comment describing the operation.

        Returns
        -------
        Lightcurve
            The current lightcurve object.

        Notes
        -----
        This method is meant to be used internally and should not be called
        directly by users.
        """
        if self.debug:
            self._operations.append((comment, self.time.copy(), self.flux.copy()))
        else:
            self._operations.append(comment)
        return self

    def get_operations_list(self):
        """
        Returns the list of operations that happened in the lightcurve.

        Returns
        -------
        operations_list : list of str
            The list of operations, with each string corresponding to a step in the correction.
        """
        operations_list = []
        for operation in self._operations:
            if isinstance(operation, str):
                operations_list.append(operation)
            else:
                operations_list.append(operation[0])
        return operations_list

    def print_operations(self):
        """
        Prints to stdout the linear summary of operations that happened to the lightcurve,
        including implicit operations (that have been called by other operations).

        If the debug mode was enabled when the operation happened (`self.debug` is ``True``),
        it also displays the figure of the flux and PSD associated with this operation.

        Returns
        -------
        Lightcurve
            The current lightcurve object.
        """
        for i, operation in enumerate(self._operations):
            if isinstance(operation, str):
                print(f"{i}: {operation}")
            else:
                comment, time, flux = operation
                print(f"{i}: {comment}")
                fig, (ax1, ax2) = plt.subplots(1, 2, figsize=self.figsize, dpi=self.dpi)
                debug_lc = Lightcurve().init_from_data(time, flux)
                debug_lc.plot_flux(ax=ax1)
                debug_lc.resample().flux_to_ppm()
                debug_lc.plot_psd(ax=ax2, filter_psd=True)
                fig.suptitle(f"{i}: {comment}", fontsize=12)
                fig.tight_layout()
        return self

    # * -------------------- ANALYSIS -------------------- * #

    def psd(self, zero_padding=0, correct=False, as_hertz=False):
        """
        Computes the Power Spectrum Density (PSD) of the lightcurve.

        Equally-spaced data is primordial to have correct results, hence the function will
        warn the user if either the data was not correctly resampled (the time points must be
        equally spaced), or if the flux is not centered around 0 (because the mean value would
        hinder the PSD).

        Invalid data (NaNs, infs) will be sent to 0 before the PSD analysis.

        Parameters
        ----------
        zero_padding : int, optional
            The number of zero arrays of the same size as the flux to pad the flux array
            with before computing the PSD.
            Default is ``0``.
        correct : bool, optional
            Whether to apply automatically correct the flux before computing the PSD if needed.
            If ``True``, the data may be resampled and centered around 0 automatically.
            Default is ``False``.
        as_hertz : bool, optional
            Whether to return the frequencies in Hertz instead of days. Default is ``False``.

        Returns
        -------
        freq : array-like
            The frequencies to which the PSD was computed.
        psd : array-like
            The intensity of the PSD at each given frequency.
        """
        lc_cleaned = self.copy()

        max_deviation_from_time_mean = abs(
            np.nanmax(np.diff(lc_cleaned.time) - np.nanmean(np.diff(lc_cleaned.time)))
        )
        if max_deviation_from_time_mean > 1e-11:
            if correct:
                lc_cleaned.resample()
            else:
                print("lc was not correctly resampled before the PSD, result will be wrong")
                print(f"max deviation from time mean is {max_deviation_from_time_mean} > 1e-11")

        flux_mean = abs(np.nanmean(lc_cleaned.flux))
        if flux_mean > 1e-11:
            if correct:
                lc_cleaned.flux_to_ppm()
            else:
                print("flux mean is not 0 before the PSD, result will be wrong")
                print(f"flux mean is {flux_mean} > 1e-11")

        lc_cleaned.remove_invalids(removal_mode="zero")
        lc_cleaned.zero_pad(n=zero_padding)

        return lobster.maths.psd(lc_cleaned.flux, lc_cleaned.dt_median, as_hertz=as_hertz)

    def acf(self):
        """
        Computes the autocorrelation function of the signal.

        Returns
        -------
        lag : array-like
            The array of lags.
        acf :
            The Autocorrelation Function calculated at each lag.
        """
        return lobster.maths.acf(self.flux, self.dt_median)

    # * -------------------- PLOTTING -------------------- * #

    def plot_jumps(self, ax, **kwargs):
        """
        Plots the jumps of the lightcurve on the given axis as vertical bars.

        Parameters
        ----------
        ax : matplotlib.axes.Axes
            The axis on which to plot the jumps.
        **kwargs
            Additional keyword arguments to pass to `matplotlib.axes.Axes.axvline`.

        Returns
        -------
        matplotlib.axes.Axes
            The axis on which the jumps were plotted.
        """
        kwargs.setdefault("c", "black")
        kwargs.setdefault("ls", "--")
        kwargs.setdefault("lw", 0.4)
        kwargs.setdefault("alpha", 0.7)
        kwargs.setdefault("zorder", 1)

        for i, jump_index in enumerate(self.jumps):
            ax.axvline(x=self.time[jump_index], **kwargs)

            if "large-jump-removed" in self.jumps[jump_index]:
                jump_gap = self.jumps[jump_index]["large-jump-removed"]
                ax.text(
                    self.time[jump_index],
                    0.95,
                    f"+{jump_gap:.1f} d",
                    fontsize="x-small",
                    transform=transforms.blended_transform_factory(ax.transData, ax.transAxes),
                )

            if i == 0 and "label" in kwargs:
                ax.legend()
        return ax

    def plot_correction_zones(self, ax, correction, **kwargs):
        """
        Plots the zones which have had corrections on the given axis as filled vertical bars.

        Parameters
        ----------
        ax : matplotlib.axes.Axes
            The axis on which to plot the jumps.
        correction : array-like
            An array of the same size as the time, with 0 for bins where there was no correction
            and a positive value for bins where a correction happened.
        **kwargs
            Additional keyword arguments to pass to `matplotlib.axes.Axes.fill_between`.

        Returns
        -------
        matplotlib.axes.Axes
            The axis on which the correction zones were plotted.
        """
        kwargs.setdefault("alpha", 0.3)
        ax.fill_between(
            self.time,
            0,
            1,
            where=correction > 0.0,
            transform=ax.get_xaxis_transform(),
            **kwargs,
        )
        if "label" in kwargs:
            ax.legend()
        return ax

    def plot_flux(
        self,
        scatter_flux=True,
        by_sector=False,
        sector_text=True,
        sector_text_position=0.05,
        sector_color_list=[s["color"] for s in plt.rcParams["axes.prop_cycle"]],
        **kwargs,
    ):
        """
        Plots the flux of the lightcurve, and possibly the informations about the sectors.

        Parameters
        ----------
        scatter_flux : bool, optional
            Whether to show the flux as scatter plot or continuous line. Scatter is better if you
            care about seeing points isolated in-between NaNs.
            Default is ``True`` for scatter.
        by_sector : bool, optional
            Whether to plot the flux with a different color for each sector. Default is ``False``.
        sector_text : bool, optional
            Whether to add the sector number as text to the plot, only done if `by_sector` is
            ``True``. Default is ``True``.
        sector_text_position : float, optional
            The position of the sector number on the y-axis. Default is ``0.05``.
        sector_color_list : list of colors, optional
            The color cycle to use for each sector. Sector :math:`i` will have the color
            contained in the tuple number :math:`i \\mod len(\\text{sector_color_list})`.
            Default is the user's matplotlib color cycle, from ``plt.rcParams["axes.prop_cycle"]``.
        **kwargs
            Additional keyword arguments to pass to `plotting.scatter` or `plotting.plt`, and then
            to `matplotlib.axes.Axes.scatter` or `matplotlib.axes.Axes.scatter` depending on
            `scatter_flux`.

        Returns
        -------
        matplotlib.axes.Axes
            The axis on which the flux was plotted.

        Examples
        --------
        .. plot::
           :include-source:

            from lobster import Lightcurve
            lc = Lightcurve()
            lc.load_kbonus("../../tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
            lc.remove_quality().stitch()
            ax = lc.plot_flux(by_sector=True, title="Stitched flux")
            ax.get_figure().tight_layout()
        """
        time = self.time
        flux = self.flux

        kwargs.setdefault("title", "Flux")
        kwargs.setdefault("xlabel", r"Time (d)")
        kwargs.setdefault("ylabel", rf"Flux ({self.unit})")

        if scatter_flux:
            plot_function = scatter
            kwargs.setdefault("s", 3)
            if "markersize" in kwargs:
                kwargs["s"] = kwargs.pop("markersize")
        else:
            plot_function = plot
            kwargs.setdefault("lw", 1.8)
            if "linewidth" in kwargs:
                kwargs["lw"] = kwargs.pop("linewidth")

        if by_sector:
            sector_list = self.sector
            unique_sec = np.unique(self.sector)
            ax = kwargs.pop("ax", None)
            label = kwargs.pop("label", None)
            for i, sector in enumerate(unique_sec):
                sector_mask = sector_list == sector
                color = sector_color_list[sector % len(sector_color_list)]
                ax = plot_function(
                    time[sector_mask],
                    flux[sector_mask],
                    ax=ax,
                    figsize=self.figsize,
                    dpi=self.dpi,
                    color=color,
                    label=label if i == 0 else None,
                    **kwargs,
                )
                if sector_text:
                    if is_all_invalid(flux[sector_mask]):
                        continue

                    ax.text(
                        time[sector_mask][np.isfinite(flux[sector_mask])][0],
                        sector_text_position,
                        f"{int(sector)}",
                        fontsize="x-small",
                        transform=transforms.blended_transform_factory(ax.transData, ax.transAxes),
                    )
            return ax
        else:
            return plot_function(time, flux, figsize=self.figsize, dpi=self.dpi, **kwargs)

    def plot_flux_nans(self, **kwargs):
        """
        Plots the NaNs of the flux, in red by default.

        Parameters
        ----------
        **kwargs
            Additional keyword arguments to pass to `plotting.scatter`, and then to
            `matplotlib.axes.Axes.scatter`.

        Returns
        -------
        matplotlib.axes.Axes
            The axis on which the NaNs were plotted.
        """
        to_plot = np.logical_not(np.isfinite(self.flux))

        kwargs.setdefault("title", "Flux")
        kwargs.setdefault("xlabel", r"Time (d)")
        kwargs.setdefault("ylabel", rf"Flux ({self.unit})")
        kwargs.setdefault("s", 1)
        kwargs.setdefault("c", "red")
        kwargs.setdefault("marker", "o")
        kwargs.setdefault("alpha", 0.3)
        return scatter(
            self.time[to_plot],
            to_plot[to_plot],
            figsize=self.figsize,
            dpi=self.dpi,
            **kwargs,
        )

    def plot_psd(
        self,
        visualisation="Hz",
        zero_padding=0,
        filter_psd=False,
        correct=False,
        nu_max=None,
        **kwargs,
    ):
        """
        Plots the Power Spectrum Density (PSD) of the lightcurve, with different computation and
        visualisation options.

        Parameters
        ----------
        visualisation : str, optional
            The unit of visualization for the x-axis. Options are ``"Hz"``, ``"1/d"``, and ``"d"``.
            Default is ``"Hz"``.
        zero_padding : int, optional
            The number of zero arrays of the same size as the flux to pad the flux array
            with before computing the PSD.
            Default is ``0``.
        filter_psd : bool or int, optional
            Whether or not to show a filtered version of the PSD superimposed on the original one,
            may be useful reading it.

            You can also give an odd integer as the window size of the filter to use, default is
            ``41`` when `filter_psd` is just ``True``. Default is ``False``.
        correct : bool, optional
            Whether to apply automatically correct the flux before computing the PSD if needed.
            If ``True``, the data may be resampled and centered around 0 automatically.
            Default is ``False``.
        nu_max : float, optional
            The maximum frequency to consider for the PSD. Default is ``None``.
        **kwargs
            Additional keyword arguments to pass to `plotting.plot`, and then to
            `matplotlib.axes.Axes.plot`.

        Returns
        -------
        matplotlib.axes.Axes
            The axis on which the PSD was plotted.

        Examples
        --------
        .. plot::
           :include-source:

            from lobster import Lightcurve
            lc = Lightcurve()
            lc.load_kbonus("../../tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
            lc.remove_quality().remove_outliers()
            lc.normalize_sin4_3_times_filter(section_mode="section").remove_outliers(3)

            fig, (ax1, ax2) = plt.subplots(2, figsize=(10, 8))
            lc.plot_flux(ax=ax1, by_sector=True, title="Normalized flux")
            lc.plot_psd(ax=ax2, filter_psd=True, correct=True)
            fig.tight_layout()
        """
        freq, psd = self.psd(zero_padding, correct)

        if visualisation == "1/d":
            freq = freq[1:]
            psd = psd[1:]
            kwargs.setdefault("xlabel", r"Frequency (d$^{-1}$)")
            kwargs.setdefault("ylabel", rf"PSD ({self.unit}$^2 \cdot$ d)")
        elif visualisation == "Hz":
            freq = freq[1:] * 1e6 / (24 * 3600)
            psd = psd[1:] * 1e-6 * 24 * 3600
            kwargs.setdefault("xlabel", r"Frequency ($\mu$Hz)")
            kwargs.setdefault("ylabel", rf"PSD ({self.unit}$^2$ / $\mu$Hz)")
        elif visualisation == "d":
            freq = 1 / freq[1:]
            psd = psd[1:]
            kwargs.setdefault("xlabel", r"Period (d)")
            kwargs.setdefault("ylabel", rf"PSD ({self.unit}$^2$ / d)")
        else:
            raise ValueError(f"could not find the visualisation mode '{visualisation}'")

        kwargs.setdefault("title", "PSD")
        kwargs.setdefault("xscale", "log")
        kwargs.setdefault("yscale", "log")
        kwargs.setdefault("lw", 0.8)

        ax = plot(freq, psd, figsize=self.figsize, dpi=self.dpi, **kwargs)
        if filter_psd:
            # pylint: disable=unidiomatic-typecheck
            filtered_psd = sin4_filter(psd, filter_psd if type(filter_psd) == int else 41)
            kwargs.setdefault("ax", ax)
            plot(freq, filtered_psd, **kwargs)

        if nu_max is not None:
            ax.axvline(x=nu_max, c="red", ls="--", lw=1.5, alpha=0.8, zorder=1)

        return ax

    def plot_acf(self, filter_acf=False, **kwargs):
        """
        Plots the Autocorrelation Function (ACF) of the lightcurve.

        Parameters
        ----------
        filter_acf : bool or int, optional
            Whether or not to show a filtered version of the ACF superimposed on the original one,
            may be useful reading it.

            You can also give an odd integer as the window size of the filter to use, default is
            ``41`` when `filter_acf` is just ``True``. Default is ``False``.
        **kwargs
            Additional keyword arguments to pass to `plotting.plot`, and then to
            `matplotlib.axes.Axes.plot`.

        Returns
        -------
        matplotlib.axes.Axes
            The axis on which the ACF was plotted.

        Examples
        --------
        .. plot::
           :include-source:

            from lobster import Lightcurve
            lc = Lightcurve()
            lc.load_kbonus("../../tests/data/kbonus/002998253/hlsp_kbonus-bkg_kepler_kepler_kic-002998253_kepler_v1.0_lc.fits")
            lc.remove_quality().remove_outliers()
            lc.normalize_sin4_3_times_filter(section_mode="section").remove_outliers(3)

            fig, (ax1, ax2) = plt.subplots(2, figsize=(10, 8))
            lc.plot_flux(ax=ax1, by_sector=True, title="Normalized flux")
            lc.plot_acf(ax=ax2, filter_acf=True)
            fig.tight_layout()
        """
        lag, acf = self.acf()
        kwargs.setdefault("title", "ACF")
        kwargs.setdefault("xlabel", r"Lag (d)")
        kwargs.setdefault("ylabel", r"ACF")
        kwargs.setdefault("alpha", 1)
        kwargs.setdefault("lw", 0.7)

        ax = plot(lag, acf, figsize=self.figsize, dpi=self.dpi, **kwargs)
        if filter_acf:
            # pylint: disable=unidiomatic-typecheck
            filtered_acf = sin4_filter(acf, filter_acf if type(filter_acf) == int else 41)
            kwargs.setdefault("ax", ax)
            plot(lag, filtered_acf, **kwargs)

        return ax
