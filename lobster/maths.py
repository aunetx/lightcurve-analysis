"""
A module that contains various mathematical functions and analysis methods, meant to be independent
from the rest of the code.
"""

import numpy as np
import scipy as sp


def acf(signal, dt):
    """
    Computes the autocorrelation of the signal.

    It accepts invalids, and will ignore them.

    Parameters
    ----------
    signal : array-like
        The signal to compute the ACF of, may contain NaNs and infinity.
    dt : float
        The timestep between two points in the signal.

    Returns
    -------
    lag : array-like
        The array of lags.
    acf :
        The Autocorrelation Function calculated at each lag.
    """
    signal = signal[np.isfinite(signal)] - np.nanmean(signal[signal != 0])

    n = len(signal)
    lag = np.arange(0, (n + 1 / 10) * dt / 2, dt)
    corr = np.correlate(signal, signal, "full")[n - 1 :] / (np.var(signal) * n)
    return lag, corr[: len(lag)]


def psd(signal, dt, as_hertz=False):
    """
    Computes the Power Spectrum Density (PSD) of the lightcurve.

    It assumes the data contains no invalids, and that the signal is homogeneously-sampled.

    Parameters
    ----------
    signal : array-like
        The signal to compute the PSD of.
    dt : float
        The timestep between two points in the signal.
    as_hertz : bool, optional
        Whether to return the frequencies in Hertz instead of days. Default is ``False``.

    Returns
    -------
    freq : array-like
        The frequencies to which the PSD was computed.
    psd : array-like
        The intensity of the PSD at each given frequency.

    Notes
    -----
    Code adapted from Sylvain Breton's apollinaire's ``series_to_psd``, see
    https://gitlab.com/sybreton/apollinaire/-/blob/master/src/apollinaire/psd/psd.py
    """
    freq = np.fft.rfftfreq(signal.size, d=dt)
    tf = np.fft.rfft(signal) / (signal.size / 2.0)
    t = signal.size * dt
    dc = np.count_nonzero(signal) / signal.size
    spectrum = np.abs(tf) * np.abs(tf) * t / 2.0 / dc

    if as_hertz:
        freq = freq / (24 * 3600)
        spectrum = spectrum * 24 * 3600

    return freq, spectrum


def calculate_bic(data, model, dof, scatter=None):
    """
    Compute the Bayesian Information Criterion (BIC) for a given dataset and model.

    The BIC is used to evaluate the goodness of fit of a model while penalizing complexity.
    A lower BIC value indicates a better balance between model accuracy and complexity.

    Parameters
    ----------
    data : ndarray
        Observed data points.
    model : ndarray
        Model predictions corresponding to the data points.
    dof : int
        Degrees of freedom in the model (number of free parameters).
    scatter : ndarray, optional
        The scatter (variance) of the data. If not provided, a robust estimate is computed
        using the median absolute deviation (MAD).

    Returns
    -------
    float
        The Bayesian Information Criterion (BIC) value.

    Examples
    --------
    >>> import numpy as np
    >>> data = np.array([1.0, 2.1, 3.0, 4.2])
    >>> model = np.array([1.1, 2.0, 3.1, 4.0])
    >>> float(calculate_bic(data, model, dof=2))
    5.5714...
    """
    if scatter is None:
        # : Conversion constant from MAD to Sigma. Constant is 1/norm.ppf(3/4)
        mad_to_sigma = 1.482602218505602
        scatter = np.nanmedian(np.abs(np.diff(data))) * mad_to_sigma
    return (
        len(data) * np.log(2)
        + np.nansum((data - model) ** 2 / scatter**2)
        + dof * np.log(len(data))
    )


def matern_covariance(t1, t2, length_scale, sigma, nu):
    """
    Compute the Matérn covariance between two points.

    Parameters
    ----------
    t1 : float
        First input point.
    t2 : float
        Second input point.
    length_scale : float
        Length scale parameter of the Matérn kernel.
    sigma : float
        Standard deviation (amplitude) of the process.
    nu : float
        Smoothness parameter of the Matérn kernel.

    Returns
    -------
    float
        The computed Matérn covariance between `t1` and `t2`.

    Notes
    -----
    The Matérn covariance function is defined as:

    .. math::
        k(d) = \\sigma^2 \\frac{2^{1-\\nu}}{\\Gamma(\\nu)}
        \\left(\\frac{\\sqrt{2\\nu} d}{l}\\right)^\\nu
        K_\\nu \\left(\\frac{\\sqrt{2\\nu} d}{l}\\right),

    where :math:`d = |t1 - t2|` is the Euclidean distance,
    :math:`K_\\nu` is the modified Bessel function of the second kind,
    and :math:`\\Gamma(\\nu)` is the Gamma function.
    """
    distance = np.abs(t1 - t2)

    if distance == 0:
        return sigma**2

    factor = np.sqrt(2 * nu) * distance / length_scale
    coeff = sigma**2 * (2 ** (1 - nu)) / sp.special.gamma(nu)

    return coeff * factor**nu * sp.special.kv(nu, factor)


def optimal_linear_estimation_irregular(
    time, signal, length_scale=130, sigma=0.05, nu=4, lambda_reg=1e-9
):
    """
    Perform optimal linear estimation using a Matérn covariance model for irregularly spaced data.

    Parameters
    ----------
    time : array-like
        Time points at which the signal is observed.
    signal : array-like
        Observed signal values corresponding to the given time points.
    length_scale : float, optional
        Length scale parameter of the Matérn kernel, by default 130.
    sigma : float, optional
        Standard deviation (amplitude) of the process, by default 0.05.
    nu : float, optional
        Smoothness parameter of the Matérn kernel, by default 4.
    lambda_reg : float, optional
        Regularization parameter added to the covariance matrix diagonal for numerical stability,
        by default 1e-9.

    Returns
    -------
    function
        A function that takes an array of time points as input and returns the estimated values
        at those points.

    Notes
    -----
    This function constructs an optimal linear estimator by solving the system:

    .. math::
        W = K^{-1} y,

    where :math:`K` is the covariance matrix computed using the Matérn kernel,
    and :math:`y` is the observed signal. The estimated values at new time points
    :math:`t^*` are computed as:

    .. math::
        \\hat{y}^* = k^* W,

    where :math:`k^*` is the vector of covariances between the new time points and
    the observed time points.
    """
    n = len(signal)

    # covariance matrix cov_mat
    cov_mat = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            cov_mat[i, j] = matern_covariance(time[i], time[j], length_scale, sigma, nu)

    # regularize K
    cov_mat += np.identity(len(cov_mat)) * lambda_reg

    # solve for optimal weights
    weights = np.linalg.solve(cov_mat, signal)

    def optimal_estimator_func(times_to_predict):
        # predict future values
        predicted_values = []
        for t_predict in times_to_predict:
            k_star = np.array(
                [matern_covariance(t_predict, t, length_scale, sigma, nu) for t in time]
            )
            predicted_values.append(np.dot(k_star, weights))
        return np.array(predicted_values)

    return optimal_estimator_func
